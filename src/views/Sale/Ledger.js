import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Select from 'react-select';
import axios from 'axios';
import { Grid } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import BlockUI from "../../BlockUI/BlockUI";
// import ReactJson from 'react-json-view';

class Ledger extends React.Component {
    constructor() {
        super()
        var today = new Date(),
        date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() ;
        this.state = { selectOptions : [], selectGSTOptions : [],ramt:'',customerid:'',customeridgst:'', date:date,repaydata: [],repayamt: 0,rmark:'',
        ramtold:'',repaytotal:'',totaldata:'',cusdata: [],due:'',cusdatagst: [],duegst:'',repaydata1: []};  
    }
    state = {
        blocking: false,
        user: {}
      };
PrintPdf(){
    window.print()
}
// togglePrint(){
//     let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/repayment/";
// let data = this.state;
// fetch(url, {
// method: 'POST',
// headers: {
// "Content-Type": "application/json",
// "Accept": "application/json"
// },
// body: JSON.stringify(data)
// }).then((result) => {
// result.json().then((resp) => {
// this.componentDidMount();
// this.setState({ramt:'',rmark:''})
// })})
// fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getrepayment/' + this.state.customerid).then((resp) => {
//         resp.json().then((result) => {
//             this.setState({ repaydata: result })
//         })
//     })
// }

togglePrint1(){
    this.setState({blocking:true})
    if(this.selectedGstType =="Yes"){
let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/repayment1";
let data = this.state;
fetch(url, {
method: 'POST',
headers: {
"Content-Type": "application/json",
"Accept": "application/json"
},
body: JSON.stringify(data)
}).then((result) => {
result.json().then((resp) => {
this.componentDidMount();
this.setState({ramt:'',rmark:'',blocking:false})
})})
} else
{ 
    let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/repayment";
    let data = this.state;
    fetch(url, {
    method: 'POST',
    headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
    },
    body: JSON.stringify(data)
    }).then((result) => {
    result.json().then((resp) => {
    this.componentDidMount();
    this.setState({ramt:'',rmark:'',blocking:false})
    })})
}
const gstOptions = [{"label" : 'With GST', 'value' : 'Yes'}, {"label" : 'Without GST', 'value' : 'No'}]
this.setState({ selectGSTOptions : gstOptions })
}
async getdata() {this.setState({blocking:true})
const res = await axios.get('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getcus')
const data = res.data

const options = data.map(d => ({
"value": d.id,
"label": d.customername,
"remark": d.remark,
"address": d.address,
"citys": d.c_state }))

const gstOptions = [{"label" : 'Both', 'value' : 'both'},{"label" : 'With GST', 'value' : 'Yes'}, {"label" : 'Without GST', 'value' : 'No'}]
this.setState({ selectOptions: options, selectGSTOptions : gstOptions ,blocking:false})
}

componentDidMount(){
    this.getdata();
    // fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getrepayment/' + this.state.customerid).then((resp) => {
    //     resp.json().then((result) => {
    //          this.setState({ repaydata: result })
    //      })
    //  })
    //  fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getrepaymentgst/' + this.state.customeridgst).then((resp) => {
    //     resp.json().then((result) => {
    //         this.setState({ repaydata1: result })
    //     })
    // })
    // fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getrepayment/' + this.state.customeridgst).then((resp) => {
    //     resp.json().then((result) => {
    //         this.setState({ repaydata: result })
    //     })
    // })
}

handleChangeGst(e){
    this.setState({blocking:true})
    this.selectedGstType = e.value;
    this.setState({customeridgst: this.selectedUser.value, name: this.selectedUser.label, 
                    remark: this.selectedUser.remark,address: this.selectedUser.address,
                    citys: this.selectedUser.citys,customerid:''})
  
    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/gettotal/' + this.selectedUser.value).then((resp) => {
        resp.json().then((result) => {
            this.setState({ totaldata: result })
        })
    })
            if(this.selectedGstType == 'both'){
                fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getcusdata/' + this.selectedUser.value).then((resp) => {
                    resp.json().then((result) => {
                        // console.log(result + 'gfgfff')
                        this.setState({ cusdatagst: result })
                    })
                })
                fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getrepaymentgst/' + this.selectedUser.value).then((resp) => {
                    resp.json().then((result) => {
                        this.setState({ repaydata1: result })
                    })
                })
                fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getrepayment/' + this.selectedUser.value).then((resp) => {
                    resp.json().then((result) => {
                        this.setState({ bothRepay: result,blocking:false })
                        // console.log(this.state.repaydata);
    })
})
            } else if(this.selectedGstType == 'Yes'){
                fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getcusdatagst/' + this.selectedUser.value).then((resp) => {
                    resp.json().then((result) => {
                        console.log(result + 'gfgfff')
                        this.setState({ cusdatagst: result })
                    })
                })
                fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getrepaymentgst/' + this.selectedUser.value).then((resp) => {
                    resp.json().then((result) => {
                        this.setState({ repaydata1: result })
                        this.setState({ repaydata: [],blocking:false })
                    })
                })
            } else {
                fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getcusdatawithout/' + this.selectedUser.value).then((resp) => {
                    resp.json().then((result) => {
                        this.setState({ cusdatagst: result })
                        // console.log(this.state.cusdatagst);
                    })
                })

                fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getrepayment/' + this.selectedUser.value).then((resp) => {
                    resp.json().then((result) => {
                        this.setState({ repaydata: result })
                        this.setState({ repaydata1: [] ,blocking:false})
                    })
                })
            }
    }

handleChange(e) {
    this.setState({blocking:true})
    this.selectedUser = e;
this.setState({ customerid: e.value, name: e.label, remark: e.remark,address: e.address,citys: e.citys})
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getcusdata/' + e.value).then((resp) => {
            resp.json().then((result) => {
                this.setState({ cusdata: result })
            })
        })
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/gettotal/' + e.value).then((resp) => {
    resp.json().then((result) => {
        this.setState({ totaldata: result })
    })
})

fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getrepayment/' + e.value).then((resp) => {
    resp.json().then((result) => {
        this.setState({ repaydata: result })
    })
})
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getrepaymentgst/' + e.value).then((resp) => {
                    resp.json().then((result) => {
                        this.setState({ repaydata1: result,blocking:false }) 
                    })
                })
}
Changeramt = (e) => {
var due = this.getdue() - (Number(this.getamount()) + Number(e.target.value));
this.setState({ramt:e.target.value,due:due})
}
Changeramtgst = (e) => {
var due = this.getdue() - (Number(this.getamountgst()) + Number(e.target.value));
var duegst = this.getduegst() - (Number(this.getamountgst()) + Number(e.target.value));
this.setState({ramt:e.target.value,due:due,duegst:duegst})
}

render() {
const numberFormat = (value) =>
new Intl.NumberFormat('en-IN', {
    style: 'currency',
    currency: 'INR'
}).format(value);
const tableCell = {padding:'4px'}
return (
    <div>
        <div style={{ width: '1200px', padding: '5px' }} className="no-printme">
            <Grid container spacing={1}>
            <Grid item xs={2}>
            &nbsp;<input type='submit' value='Print && Pdf' name='pdf' className='pdf' onClick={this.PrintPdf}></input>
        </Grid>
        <Grid item xs={2}>
        <Select options={this.state.selectOptions} onChange={this.handleChange.bind(this)}
                placeholder="Select Cus." className='textfield' />
            </Grid>
            <Grid item xs={2}>
            <Select options={this.state.selectGSTOptions} onChange={this.handleChangeGst.bind(this)}
                placeholder="Both" className='textfield' />
            </Grid>   
        </Grid>     
    </div>
    {/* <ReactJson src={this.state.repaydata1} /> */}
{this.state.customerid ?
<Grid container spacing={1}>
<Grid item xs={4}>
<p>Customer Name :&nbsp;{this.state.name}</p>
<p>Customer Id :&nbsp;{this.state.customerid}</p>
</Grid>
<Grid item xs={4}>
<p>Address :&nbsp;{this.state.address}</p>
<p>State :&nbsp;{this.state.citys}</p>
</Grid>
    <TableContainer component={Paper}>
    <Table aria-label="spanning table">
        <TableHead>
            <TableRow>
                <TableCell>Gst Include</TableCell>
                <TableCell>Bill No.</TableCell>
                <TableCell>Date</TableCell>
                <TableCell>Total</TableCell>
                <TableCell>Paid Amt.</TableCell>
                <TableCell>Due Amt.</TableCell>
            </TableRow>
        </TableHead>
        <TableBody>

        
                {
                this.state.cusdata ?
                this.state.cusdata.map((data,i) =>
                <TableRow>
                <TableCell style={tableCell}>{data.vat}</TableCell>
                <TableCell style={tableCell}>{data.bill_no}</TableCell>
                <TableCell style={tableCell}>{data.date_update}</TableCell>
                <TableCell style={tableCell}>{data.total}</TableCell>
                <TableCell style={tableCell}>{data.amt}</TableCell>
                <TableCell style={tableCell}>{data.due}</TableCell>
                </TableRow>   
                ):null
                }
                
                {
                this.state.totaldata ?
                this.state.totaldata.map((data,i) =>
                <TableRow>
                <TableCell style={tableCell}><b>Sub Total</b></TableCell>
                <TableCell style={tableCell}></TableCell>
                <TableCell style={tableCell}></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(data.total)}</b></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(data.amt)}</b></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(data.total - data.amt)}</b></TableCell>
                </TableRow>  
                ):null
                }
                {
                this.state.repaydata ?
                this.state.repaydata.map((data,i) =>
                <TableRow>
                <TableCell style={tableCell}><b>Repayment</b></TableCell>
                <TableCell style={tableCell}></TableCell>
                <TableCell style={tableCell}><b>{data.date_update}</b></TableCell>
                <TableCell style={tableCell}><b>{data.remark}</b></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(data.amount)}</b></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(data.due)}</b></TableCell>
                </TableRow>  
                ):null
                }
                {
                this.state.repaydata1 ?
                this.state.repaydata1.map((data,i) =>
                <TableRow>
                <TableCell style={tableCell}><b>Repayment</b></TableCell>
                <TableCell style={tableCell}></TableCell>
                <TableCell style={tableCell}><b>{data.date_update}</b></TableCell>
                <TableCell style={tableCell}><b>{data.remark}</b></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(data.amount)}</b></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(data.due)}</b></TableCell>
                </TableRow>  
                ):null
                }
        </TableBody>
    </Table>
</TableContainer>

{/* <div className='repayment' className="no-printme">
<div><strong>Repayment</strong></div>
<div><TextField name="date" id="standard-basic" label='Date' value={this.state.date} />&nbsp;&nbsp;
<TextField name="rmark" id="standard-basic" label="Enter Remark" value={this.state.rmark} 
onChange={(e) => { this.setState({ rmark: e.target.value }) }}/>&nbsp;&nbsp;
<TextField name="ramt" id="standard-basic" label="Enter Amount" value={this.state.ramt} 
onChange={(e) => { this.Changeramt(e) }}/>&nbsp;&nbsp;
<Button onClick={() => this.togglePrint()} color="primary" style={{marginBottom:'-34px'}}>Pay</Button></div> 
</div> */}
</Grid>:this.state.customeridgst ?
<Grid container spacing={1}>
<Grid item xs={4}>
<p>Customer Name :&nbsp;{this.state.name}</p>
<p>Customer Id :&nbsp;{this.state.customeridgst}</p>
</Grid>
<Grid item xs={4}>
<p>Address :&nbsp;{this.state.address}</p>
<p>State :&nbsp;{this.state.citys}</p>
</Grid>
    <TableContainer component={Paper}>
    <Table aria-label="spanning table">
        <TableHead>
            <TableRow>
                <TableCell>Gst Include</TableCell>
                <TableCell>Bill No.</TableCell>
                <TableCell>Date</TableCell>
                <TableCell>Total</TableCell>
                <TableCell>Paid Amt.</TableCell>
                <TableCell>Due Amt.</TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
                {
                this.state.cusdatagst ?
                this.state.cusdatagst.map((data,i) =>
                <TableRow>
                <TableCell style={tableCell}>{data.vat}</TableCell>
                <TableCell style={tableCell}>{data.bill_no}</TableCell>
                <TableCell style={tableCell}>{data.date_update}</TableCell>
                <TableCell style={tableCell}>{data.total}</TableCell>
                <TableCell style={tableCell}>{data.amt}</TableCell>
                <TableCell style={tableCell}>{data.due}</TableCell>
                </TableRow>   
                ):null
                }
                <TableRow>
                <TableCell style={tableCell}><b>Sub Total</b></TableCell>
                <TableCell style={tableCell}></TableCell>
                <TableCell style={tableCell}></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(this.gettotalgst())}</b></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(this.getpaidgst())}</b></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(this.getduegst())}</b></TableCell>
                </TableRow>  
                {
                this.state.repaydata ?
                this.state.repaydata.map((data,i) =>
                <TableRow>
                <TableCell style={tableCell}><b>Repayment</b></TableCell>
                <TableCell style={tableCell}></TableCell>
                <TableCell style={tableCell}><b>{data.date_update}</b></TableCell>
                <TableCell style={tableCell}><b>{data.remark}</b></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(data.amount)}</b></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(data.due)}</b></TableCell>
                </TableRow>  
                ):null
                }
                {
                this.state.repaydata1 ?
                this.state.repaydata1.map((data,i) =>
                <TableRow>
                <TableCell style={tableCell}><b>Repayment</b></TableCell>
                <TableCell style={tableCell}></TableCell>
                <TableCell style={tableCell}><b>{data.date_update}</b></TableCell>
                <TableCell style={tableCell}><b>{data.remark}</b></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(data.amount)}</b></TableCell>
                <TableCell style={tableCell}><b>{numberFormat(data.due)}</b></TableCell>
                </TableRow>  
                ):null
                }
        </TableBody>
    </Table>
</TableContainer>
<div className='repayment' className="no-printme">
<div><strong>Repayment</strong></div>
<div><TextField name="date" id="standard-basic" label='Date' value={this.state.date} />&nbsp;&nbsp;
<TextField name="rmark" id="standard-basic" label="Enter Remark" value={this.state.rmark} 
onChange={(e) => { this.setState({ rmark: e.target.value }) }}/>&nbsp;&nbsp;
<TextField name="ramt" id="standard-basic" label="Enter Amount" value={this.state.ramt} 
onChange={(e) => { 
    if(this.selectedGstType=='Yes'){
        {this.Changeramtgst(e)}
        }
        else {
            {this.Changeramt(e)}
        } }}/>&nbsp;&nbsp;
<Button onClick={() => this.togglePrint1()} color="primary" style={{marginBottom:'-34px'}}>Pay</Button></div> 
</div>

</Grid>:''
} <BlockUI blocking={this.state.blocking} />
</div>
)
}



gettotal() {
let grandtotal = 0;
const rowTotals = this.state.cusdata.map(
row =>
(Number(row.total)) || 0

);

if (rowTotals.length > 0) {
grandtotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandtotal;
}


getpaid() {
let grandpaid = 0;
const rowTotals = this.state.cusdata.map(
row =>
(Number(row.amt)) || 0

);

if (rowTotals.length > 0) {
grandpaid = rowTotals.reduce((acc, val) => acc + val);
}
return grandpaid;
}
getdue() {
let granddue = 0;
const rowTotals = this.state.cusdata.map(
row =>
(Number(row.due)) || 0

);

if (rowTotals.length > 0) {
granddue = rowTotals.reduce((acc, val) =>val);
}
return granddue;
}
getamount() {
let grantt = 0;
const rowTotals = this.state.repaydata.map(
row =>
(Number(row.amount)) || 0

);

if (rowTotals.length > 0) {
grantt = rowTotals.reduce((acc, val) => acc + val);
}
return grantt;
}

gettotalgst() {
let grandtotal = 0;
const rowTotals = this.state.cusdatagst.map(
row =>
(Number(row.total)) || 0

);

if (rowTotals.length > 0) {
grandtotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandtotal;
}
getpaidgst() {
let grandpaid = 0;
const rowTotals = this.state.cusdatagst.map(
row =>
(Number(row.amt)) || 0

);

if (rowTotals.length > 0) {
grandpaid = rowTotals.reduce((acc, val) => acc + val);
}
return grandpaid;
}
getduegst() {
let granddue = 0;
const rowTotals = this.state.cusdatagst.map(
row =>
(Number(row.due)) || 0

);

if (rowTotals.length > 0) {
granddue = rowTotals.reduce((acc, val) => acc + val);
}
return granddue;
}
getamountgst() {
let grantt = 0;
const rowTotals = this.state.repaydata1.map(
row =>
(Number(row.amount)) || 0

);

if (rowTotals.length > 0) {
grantt = rowTotals.reduce((acc, val) => acc + val);
}
return grantt;
}
   
}

export default Ledger;

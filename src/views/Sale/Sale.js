import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Remove';
import Select from 'react-select'
import axios from 'axios'
import { withRouter } from 'react-router-dom';
import { Grid, Row, Col } from 'react-material-responsive-grid';
import PrintSale from "./PrintSale.js";
import BlockUI from "../../BlockUI/BlockUI";
// import Ledger from './Ledger';

class Forms extends React.Component {

constructor() {
super()
var today = new Date(),
date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() ;
var today = new Date(),
time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
var today1 = new Date(),
date1 = today1.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() ;
this.state = {
maxid: "", maxidgst: "", customerid: "", customername: "", remark: "", address: "", refno: "", date: "",
commitdate: "", vat: 'no', paytype: "", itemlist: "", rowData: [], itemid: "", id: 0, selectOptions: [], id: "",
name: '', seen: false, seeni: false, category: '', print: false, billdata: "", totalamt: 0, amt: '', due: 0,
currentdate : date1, currenttime : time, nameerror:'', citys:'', statelist:'', gst:'', sgst:'', cgst:'', 
igst:'',date1:date1,onlyp:false,cgstno:'',amt1: '',premarkcash:'',premarkonline:'',mode:'',
mode1:'',payresult: []
};
this.getdata();
this.handleQuantiteChange = this.handleQuantiteChange.bind(this);
this.handlePrixChange = this.handlePrixChange.bind(this);
this.handleselectprdtChange = this.handleselectprdtChange.bind(this);
this.handleRowDelete = this.handleRowDelete.bind(this);
this.handleRowAdd = this.handleRowAdd.bind(this);
this.handleMakingchaChange = this.handleMakingchaChange.bind(this);
this.getTotal = this.getTotal.bind(this);
this.submitcus = this.submitcus.bind(this);
this.submititem = this.submititem.bind(this);
}

togglePop = () => {this.setState({seen: !this.state.seen});};
togglePopi = () => {this.setState({seeni: !this.state.seeni});};
togglePrint = () => {
    if(this.valid()){
    if(this.state.vat === 'yes'){
        this.setState({print: !this.state.print});
    }else{
        this.setState({print: !this.state.print}); 
    }
}};
state = {
    blocking: false,
    user: {}
  };
 
async getdata() {
    this.setState({blocking: true});
const res = await axios.get('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getcus')
const data = res.data
const options = data.map(d => ({
"value": d.id,
"label": d.customername,
"remark": d.remark,
"address": d.address,
"citys": d.c_state,
"cgstno":d.gstno
}))
this.setState({ blocking: false,selectOptions: options })}

handleChange(e) {
this.setState({ customerid: e.value, name: e.label, remark: e.remark, address: e.address, nameerror:'',
citys: e.citys,cgstno: e.cgstno })
}
handlecategoryChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { category: value });
this.setState({
rowData: rowDataCopy
});
}
handleremarkChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { itemremark: value });
this.setState({
rowData: rowDataCopy
});
}
handleQuantiteChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { qty: parseInt(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleselectprdtChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { itemname: value });
this.setState({
rowData: rowDataCopy
});
}
handlePrixChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { rate: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleWtChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { wt: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleUnitChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { unit: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handlePolishChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { polishcha: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleMakingchaChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { makingcha: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleDisChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { disamt: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}

valid(){
if(this.state.rowData.length===0){
    this.setState({nameerror:'Customer Name,Date, Category and Amt are required'})
}else if (this.state.name.length===0) {
    this.setState({nameerror:'Customer Name are required'})
}else if (this.state.amt==='') {
    this.setState({nameerror:'Amt are required'})
}else if(this.state.commitdate===''){
    this.setState({nameerror:'Date are required'})
}
else
    return true
}


submit() {
if(this.valid()){
this.save();
    this.togglePrint();
}}


onlyprint(){
   this.togglePrint();   
}
exit(){
    this.setState({rowData: [],customerid:'',customername:'',remark:'',amt1:'',premarkcash:'',premarkonline:'',
    address:'',paytype:'',amt: '',citys:'',vat:'',print: false,onlyp: false,mode:'',mode1:''}); 
    this.componentDidMount();
}
save() {
if(this.valid()){
if (this.state.vat === "yes") {
    this.setState({
        blocking: true
      });
let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/billgst";
let data = this.state;
fetch(url, {
method: 'POST',
headers: {
"Content-Type": "application/json",
"Accept": "application/json"
},
body: JSON.stringify(data)
}).then((result) => {
result.json().then((resp) => {
    this.setState({blocking:false})
this.setState({onlyp:true})
})})
} else {
    this.setState({
        blocking: true
      });
let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/bill";
let data = this.state;
fetch(url, {
method: 'POST',
headers: {
"Content-Type": "application/json",
"Accept": "application/json"
},
body: JSON.stringify(data)
}).then((result) => {
result.json().then((resp) => {
    this.setState({blocking:false})
this.setState({onlyp:true,})
})})}}}

componentDidMount() {
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getpaymentmode').then((resp) => {
    resp.json().then((result) => {
        this.setState({ payresult: result })
    })
})
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getitem').then((resp) => {
resp.json().then((result) => {
this.setState({ itemlist: result })
})
})
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getstate').then((resp) => {
resp.json().then((result) => {
this.setState({ statelist: result })
})
})
fetch("http://ydbjewellersserver.jkgroupmanpower.in/public/api/getgst/" + 1,{
method:'GET',
})
.then((response) => response.json())
.then((result) =>{
this.setState({
igst: result.igst,
cgst: result.cgst,
sgst: result.sgst,
gst: Number(result.sgst) + Number(result.cgst)
})
})

if (this.state.vat === 'yes') {
    this.setState({blocking: true});
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getidgst').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result,blocking:false })
})
})
} else {
    this.setState({blocking: true});
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getid').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result,blocking:false })
})
})
}
this.getdata();
}

submitcus() {
    this.setState({blocking: true});
let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/addcus";
let data = this.state;
fetch(url, {
method: 'POST',
headers: {
"Content-Type": "application/json",
"Accept": "application/json"
},
body: JSON.stringify(data)
}).then((result) => {
result.json().then((resp) => {
    this.setState({blocking:false})
this.componentDidMount();
this.togglePop();
this.setState({customername:'',remark:'', address:'',citys:'',})
})
})
};

submititem() {
    this.setState({blocking: true});
let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/additem";
let data = this.state;
fetch(url, {
method: 'POST',
headers: {
"Content-Type": "application/json",
"Accept": "application/json"
},
body: JSON.stringify(data)
}).then((result) => {
result.json().then((resp) => {
this.componentDidMount();
this.togglePopi();
this.setState({blocking:false})
})
})
};
changevat(e){
this.setState({ vat: e.target.value,blocking:true }) 
if(e.target.value==='yes'){
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getidgst').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result,blocking:false })
})
})
}else{
    this.setState({blocking: true});
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getid').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result ,blocking:false})
})
})
}
// if(e.target.value==='yes' && this.state.citys === "MADHYA PRADESH"){
// this.setState({ gstnew: this.state.gst })
// }else{
// this.setState({ gstnew: this.state.igst })
// }
}
Print(){
    this.setState({rowData: [],customerid:'',customername:'',remark:'',amt1:'',premarkonline:'',premarkcash:'',
    address:'',paytype:'',amt: '',citys:'',vat:'',print: false,onlyp: false,mode:'',mode1:''});
    window.print()
    this.componentDidMount();
}

myChangeHandler = (event) => {
   if(this.state.vat === 'yes' && this.state.citys === "MADHYA PRADESH" ){
    const total =  this.getTotal() - 0;
    const totalnew = total.toFixed(2);
    this.setState({amt: event.target.value});
    this.setState({totalamt: totalnew});
   }else if(this.state.vat === 'yes'){
    const total =  this.getTotaligst() - 0;
    const totalnew = total.toFixed(2);
    this.setState({amt: event.target.value});
    this.setState({totalamt: totalnew});
   }else{
    const total1 = this.getTotalwithout() - 0;
    const total1new = total1.toFixed(2);
    this.setState({amt: event.target.value});
    this.setState({totalamt: total1new});   
   }
  }
  
render() {

const total1 = this.getTotalwithout() - (Number(this.state.amt) + Number(this.state.amt1));
const total1new = total1.toFixed(2);
const total =  this.getTotal() - (Number(this.state.amt) + Number(this.state.amt1));
const totalnew = total.toFixed(2);
const totali =  this.getTotaligst() - (Number(this.state.amt) + Number(this.state.amt1));
const totalnewi = totali.toFixed(2);
const gettotal = this.getTotalwithout() - 0;
const getTotal1 = gettotal.toFixed(2);
const gettotal1 = this.getTotal() -0;
const getTotal2 = gettotal1.toFixed(2);
const getsgst = this.getsgst() -0;
const sgst1 = getsgst.toFixed(2);
const getcgst = this.getcgst() -0;
const cgst1 = getcgst.toFixed(2);
const getigst = this.getigst() -0;
const igst = getigst.toFixed(2);
const gettotaligst = this.getTotaligst() - 0;
const totaligst = gettotaligst.toFixed(2);
const getwt = this.getwt() - 0;
const totalwt = getwt.toFixed(2);

const mystyle = {color: "#050505",backgroundColor: "#0ABE95",padding: "10px",fontFamily: "Arial","border-radius": "10px"};
const mystyle1 = {padding: "10px"};
const mystyle2 = {padding: "10px"};
const mystyle3 = {padding: "10px"};
const mystyle4 = {padding: "10px"};
const mystyle5 = {padding: "10px"};
const mystyle6 = {padding: "10px", width: "182px"};
const mystyle7 = {padding: "10px", width: "182px"};
const mystyle8 = {padding: "10px", width: "182px"};
const cstate = {padding: "5px", width: "182px"};
const mystyleitem = {padding: "10px", width: "80px"};
const btnstyle = {padding: "10px",};
const table = {minWidth: 340,};
const mystyleq = {padding: "10px", width: "30px"};
const wt = {padding: "10px", width: "40px"};
const rate = {padding: "10px", width: "50px"};
const discount = {padding: "10px", width: "40px"};
const mystyle80 = {width: "40px"};
const makingchange = {padding: "10px", width: "60px"};
const button = {width: 13, height: 13,padding: 0};
const removebutton = {width: 13, height: 13,padding: 0};
const mystypaytype = {padding: 0, width: "195px"}
const customStyles = {
container: provided => ({...provided,width: "225px",paddingtop: "5px"})};
const addcus = {padding: "5px",}
const model = {position: 'fixed',width: '100 px',height: '100 px',};
const content = {backgroundColor: "#0ABE95",position: 'absolute',top: '0px',left: '30px',width: '500px',padding: '20px',};
const model1 = {position: 'fixed',width: '100 px',height: '100 px',};
const content1 = {backgroundColor: "#0ABE95",position: 'absolute',top: '0px',left: '30px',width: '500px',padding: '20px',
};
const modelprint = {position: 'absoulute',};
const contentprint = {backgroundColor: "white",position: 'absolute',top: '60px',left: '10px',width: '970px',padding: '20px',
};
const tableCell = {paddingRight: 10,paddingLeft: 10,minWidth:5}
const tableCell1 = {paddingRight: 10,paddingLeft: 10,minWidth:5,border: '1px solid black'}

return (
<div>{this.state.print ? 
null:<div>
<span className="no-printme"><Button color="primary" onClick={this.togglePop} >Add Customers</Button></span>
<span className="no-printme"><Button color="primary" onClick={this.togglePopi}>Add Items</Button></span>
{/* <span class="no-printme"><Button color="primary" onClick={this.togglePopi}>Ledger</Button></span> */}
{this.state.onlyp === true ?
<span><Button onClick={() => this.exit()} color="primary">Exit</Button></span>:
<span><Button onClick={() => this.save()} color="primary">Save</Button></span>
}
{this.state.onlyp === true ?
<span><Button onClick={() => this.onlyprint()} color="primary">Print Preview</Button></span>:
<span><Button onClick={() => this.submit()} color="primary">Print && PDF</Button></span>
}
</div> 
}
{this.state.seen ?
<div style={model}>
<div style={content}>
<form>
<h5>Add Customer</h5>
<TextField name="customername" value={this.state.customername} label="Customer name"
onChange={(e) => { this.setState({ customername: e.target.value }) }} style={{ padding: '5px' }} />
<TextField name="remark" value={this.state.remark} label="Contact No."
onChange={(e) => { this.setState({ remark: e.target.value }) }} style={{ padding: '5px' }} />
<TextField name="address" value={this.state.address} label="Address"
onChange={(e) => { this.setState({ address: e.target.value }) }} style={{ padding: '5px' }} />
<TextField name="cgstno" value={this.state.cgstno} label="Gst No."
onChange={(e) => { this.setState({ cgstno: e.target.value }) }} style={{ padding: '5px' }} />
<TextField name="states" id="select" select label="Select State"
value={this.state.citys} style={cstate}
onChange={(e) => { this.setState({citys: e.target.value}) }}>
{
this.state.statelist ?
    this.state.statelist.map((item, i) =>
        <MenuItem key={item.state_id} value={item.state_name}>{item.state_name}</MenuItem>
    )
    :
    null
}
</TextField><br></br><br></br>
<Button variant="contained" onClick={() => this.submitcus()} color="primary">Save</Button>&nbsp;
<Button variant="contained" onClick={() => this.togglePop()} color="primary">Cancel</Button>
</form>
</div>
</div>
: this.state.seeni ?
<div style={model1}>
<div style={content1}>
<form>
<h5>Add Item</h5>
<TextField name="itemname" value={this.state.itemname} label="Itemname"
onChange={(e) => { this.setState({ itemname: e.target.value }) }} style={{ padding: '5px' }} />
<TextField name="category" value={this.state.category} select label="Select Category"
onChange={(e) => { this.setState({ category: e.target.value }) }} style={{ padding: '5px' }} style={cstate}>
<MenuItem value='Gold'>Gold</MenuItem><MenuItem value='Silver'>Silver</MenuItem></TextField><br></br><br></br>
<Button variant="contained" onClick={() => this.submititem()} color="primary">Save</Button>&nbsp;
<Button variant="contained" onClick={() => this.togglePopi()} color="primary">Cancel</Button>
</form>
</div>
</div>
: this.state.print ?

<PrintSale data={{Print:this.Print.bind(this),name:this.state.name,invoiceno:this.state.maxid,address:this.state.
address,contact:this.state.remark,vat:this.state.vat,rowData:this.state.rowData,amt:this.state.amt,totalamt:
this.state.totalamt,citys:this.state.citys,sgst:this.state.sgst,cgst:this.state.cgst,igst:this.state.igst,
gst:this.state.gst,maxid:this.state.maxid,cgstno:this.state.cgstno,amt1:this.state.amt1,mode:this.state.mode,
mode1:this.state.mode1}}/>

: <Grid item xs={12}><form autoComplete="off">
<div style={mystyle}>

{
this.state.maxid ?
this.state.maxid.map((item, i) =>
<span>Bill No. :&nbsp;{item.id + 1} </span>
)
:
null
}
<Select styles={customStyles} options={this.state.selectOptions} onChange={this.handleChange.bind(this)}
placeholder="Select Customers..."
theme={theme => ({
...theme,
borderRadius: 0,
colors: {
...theme.colors,
primary25: 'green',
neutral0: '#c8c8c8',
neutral90: 'white'
},
})} /><p style={{color:'red',fontSize:16,fontStyle:"oblique"}}>{this.state.nameerror}</p>
<TextField style={mystyle1} name="customerid" value={this.state.customerid} id="standard-basic" label="Customer Id"
onChange={(e) => { this.setState({ customerid: e.target.value }) }}/>
<TextField style={mystyle3} name="remark" value={this.state.remark} id="standard-basic" label="Remark"
onChange={(e) => { this.setState({ remark: e.target.value }) }} />
<TextField style={mystyle4} name="address" value={this.state.address} id="standard-basic" label="Address"
onChange={(e) => { this.setState({ address: e.target.value }) }} />
<TextField style={mystyle5} name="citys" id="standard-basic" label="State" value={this.state.citys}
onChange={(e) => { this.setState({ citys: e.target.value }) }} />
<TextField style={mystyle6}
name='date' label="Date" value={this.state.currentdate}
onChange={(e) => { this.setState({ date: e.target.value }) }}
/>
<TextField style={mystyle7}
id="date"
name="commitdate"
label="Commitment Dt."
type="date"
InputLabelProps={{
shrink: true,
}}
onChange={(e) => { this.setState({ commitdate: e.target.value }) }}
/>
<TextField style={mystyle8} name="vat" id="select" label="Gst Include" select
onChange={(e) => {this.changevat(e) }}>
<MenuItem value="yes">Yes</MenuItem>
<MenuItem value="no">No</MenuItem>
</TextField>
<TextField style={mystyle8} name="paytype" id="select" label="Payment Type" select
value={this.state.paytype}
onChange={(e) => { this.setState({ paytype: e.target.value }) }}>
<MenuItem value="1">Cash</MenuItem>
<MenuItem value="2">Online</MenuItem>
</TextField>

<TableContainer component={Paper}>
<Table style={table} aria-label="spanning table">
<TableHead>

<TableRow>
<TableCell style={tableCell}><IconButton style={button} color="primary" variant="contained"
onClick={this.handleRowAdd}><AddIcon /></IconButton></TableCell>
<TableCell style={tableCell}>Item</TableCell>
<TableCell style={tableCell}>Qty.</TableCell>
<TableCell style={tableCell}>Wt.(gm.)</TableCell>
<TableCell style={tableCell}>Rate<br></br>(Per gm.)</TableCell>
<TableCell style={tableCell}>Making Charge</TableCell>
<TableCell style={tableCell}>Polish Cha(%)</TableCell>
<TableCell style={tableCell}>Amount</TableCell>
<TableCell style={tableCell}>Dis.Amt</TableCell>
{
this.state.vat === 'yes' && this.state.citys === "MADHYA PRADESH" ?
<TableCell style={tableCell}>SGST<br></br>CGST</TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}>IGST</TableCell>:''}
<TableCell style={tableCell}>Total</TableCell>
</TableRow>
</TableHead>

<TableBody>


{this.state.rowData.map((data, index) =>



<TableRow key={index} id={index}>
<TableCell style={tableCell}><IconButton style={removebutton} color="secondary" variant="contained"

onClick={(e) => this.handleRowDelete(index)} active><RemoveIcon /></IconButton></TableCell>
<TableCell style={tableCell}>

<TextField style={mystyleitem} name="itemname" id="select" label="Itemname" select
value={this.state.rowData.itemname}
onChange={(e) => this.handleselectprdtChange(index, e.target.value)}>
{
this.state.itemlist ?
    this.state.itemlist.map((item, i) =>
        <MenuItem key={item.id} value={item.itemname}>{item.itemname}</MenuItem>
    ):null
}
</TextField><hr></hr><br></br>
<TextField name="category" id="select" select label='Cat.'
value={this.state.rowData.category}
onChange={(e) => this.handlecategoryChange(index, e.target.value)}>
<MenuItem value="Gold">Gold</MenuItem>
<MenuItem value="Silver">Silver</MenuItem>
</TextField>&nbsp;&nbsp;
<TextField name="item_remark" id="standard-basic" label="remark." 
value={this.state.rowData.itemremark} onChange={(e) => this.handleremarkChange(index, e.target.value)} />
</TableCell>
<TableCell style={tableCell}><TextField style={mystyleq} name="qty" id="standard-basic" label="Qty."
value={this.state.rowData.qty} onChange={(e) => this.handleQuantiteChange(index, e.target.value)} /></TableCell>
<TableCell style={tableCell}><TextField style={wt} name="wt" id="standard-basic" label="Weight"
value={this.state.rowData.wt} onChange={(e) => this.handleWtChange(index, e.target.value)} /></TableCell>
<TableCell style={tableCell}><TextField style={rate} name="rate" id="standard-basic" label="Rate"
value={this.state.rowData.rate} onChange={(e) => this.handlePrixChange(index, e.target.value)} /></TableCell>
<TableCell style={tableCell}>
<TableRow><TableCell style={tableCell}>
<TextField style={mystyle80} name="unit" id="select" select
value={this.state.rowData.unit}
onChange={(e) => this.handleUnitChange(index, e.target.value)}>
<MenuItem value="1">Fix</MenuItem>
<MenuItem value="2">Per gm.</MenuItem>
<MenuItem value="3">Per pcs.</MenuItem>
<MenuItem value="4">None</MenuItem>
</TextField></TableCell>

<TableCell style={tableCell}>{
data.unit === 1 ? <span>{(data.makingcha).toFixed(2)}</span>
: data.unit === 2 ? <span>{((data.wt || 0) * (data.makingcha || 0)).toFixed(2)}</span>
: data.unit === 3 ? <span>{((data.makingcha || 0) * (data.qty || 0)).toFixed(2)}</span>
: data.unit === 4 ? <span>{(data.makingcha || 0).toFixed(2)}</span> : ''
}</TableCell>

</TableRow><TextField style={makingchange} name="makingcha" id="standard-basic" label="Making cha."
value={this.state.rowData.makingcha}
onChange={(e) => this.handleMakingchaChange(index, e.target.value)} />
</TableCell>
<TableCell style={tableCell}><TextField style={mystyleitem} name="polishcha" id="select" select
value={this.state.rowData.polishcha}
onChange={(e) => this.handlePolishChange(index, e.target.value)}>
<MenuItem value="5">5&nbsp;%</MenuItem>
<MenuItem value="10">10&nbsp;%</MenuItem>
<MenuItem value='0'>None</MenuItem>
</TextField></TableCell>
{data.polishcha ?
<TableCell style={tableCell}>{
    data.unit === 1 ? <span>{((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0)) 
    + (((data.wt * data.rate) * data.polishcha) / 100)).toFixed(2)}</span>
    : data.unit === 2 ? <span>{((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0))) 
    + (((data.wt * data.rate) * data.polishcha) / 100)).toFixed(2)}</span>
    : data.unit === 3 ? <span>{((((data.wt || 0) * (data.rate || 0)) + ((data.makingcha || 0) * (data.qty || 0))) 
    + (((data.wt * data.rate) * data.polishcha) / 100)).toFixed(2)}</span>
    : data.unit === 4 ? <span>{((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0))
    + (((data.wt * data.rate) * data.polishcha) / 100)).toFixed(2)}</span> : ''
}
</TableCell> :
<TableCell style={tableCell}>{
data.unit === 1 ? <span>{(((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0)).toFixed(2)}</span>
: data.unit === 2 ? <span>{(((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0))).toFixed(2)}</span>
: data.unit === 3 ? <span>{(((data.wt || 0) * (data.rate || 0)) + ((data.makingcha || 0) * (data.qty || 0))).toFixed(2)}</span>
: data.unit === 4 ? <span>{(((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0)).toFixed(2)}</span> : ''
}
</TableCell>}
<TableCell style={tableCell}><TextField style={discount} name="disamt" id="standard-basic" label="Dis."
value={this.state.rowData.disamt}
onChange={(e) => this.handleDisChange(index, e.target.value)} /></TableCell>

{this.state.vat === "yes" && this.state.citys === "MADHYA PRADESH" ?
<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{this.state.sgst}</span>&nbsp;
{
data.unit === 1 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.sgst) / 100).toFixed(2)}</span>
: data.unit === 2 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) 
+ (((data.wt * data.rate) * data.polishcha) / 100)) ) * Number(this.state.sgst) / 100).toFixed(2)}</span>
: data.unit === 3 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.makingcha || 0) * (data.qty || 0)) 
+ (((data.wt * data.rate) * data.polishcha) / 100)) ) * Number(this.state.sgst) / 100).toFixed(2)}</span>
: data.unit === 4 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) 
+ (((data.wt * data.rate) * data.polishcha) / 100)) ) * Number(this.state.sgst) / 100).toFixed(2)}</span> : ''
}
<br></br>
<span style={{ backgroundColor: "red" }}>{this.state.cgst}</span>&nbsp;
{
data.unit === 1 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.cgst) / 100).toFixed(2)}</span>
: data.unit === 2 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) 
+ (((data.wt * data.rate) * data.polishcha) / 100)) ) * Number(this.state.cgst) / 100).toFixed(2)}</span>
: data.unit === 3 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.makingcha || 0) * (data.qty || 0)) 
+ (((data.wt * data.rate) * data.polishcha) / 100)) ) * Number(this.state.cgst) / 100).toFixed(2)}</span>
: data.unit === 4 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) 
+ (((data.wt * data.rate) * data.polishcha) / 100)) ) * Number(this.state.cgst) / 100).toFixed(2)}</span> : ''
}
</TableCell>:this.state.vat === 'yes' ?
<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{this.state.igst}</span>&nbsp;
{
data.unit === 1 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.igst) / 100).toFixed(2)}</span>
: data.unit === 2 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) 
+ (((data.wt * data.rate) * data.polishcha) / 100)) ) * Number(this.state.igst) / 100).toFixed(2)}</span>
: data.unit === 3 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.makingcha || 0) * (data.qty || 0)) 
+ (((data.wt * data.rate) * data.polishcha) / 100)) ) * Number(this.state.igst) / 100).toFixed(2)}</span>
: data.unit === 4 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) 
+ (((data.wt * data.rate) * data.polishcha) / 100)) ) * Number(this.state.igst) / 100).toFixed(2)}</span> : ''
}
</TableCell>:''}
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{
data.unit === 1 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.gst) / 100) - (data.disamt || 0)).toFixed(2)}</span>
: data.unit === 2 ? 
<span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.gst) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: data.unit === 3 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.qty || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((((data.wt || 0) * (data.rate || 0)) + ((data.qty || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.gst) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: data.unit === 4 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.gst) / 100)- (data.disamt || 0)).toFixed(2)}</span> : ''
}
</TableCell>:this.state.vat === 'yes' ?
<TableCell style={tableCell}>{
data.unit === 1 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.igst) / 100) - (data.disamt || 0)).toFixed(2)}</span>
: data.unit === 2 ? 
<span>{((((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.igst) / 100) - (data.disamt || 0)).toFixed(2))}</span>
: data.unit === 3 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.qty || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((((data.wt || 0) * (data.rate || 0)) + ((data.qty || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.igst) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: data.unit === 4 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.igst) / 100)- (data.disamt || 0)).toFixed(2)}</span> : ''
}
</TableCell>:''}
{this.state.vat === 'no' ?
<TableCell style={tableCell}>{
data.unit === 1 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
- (data.disamt || 0))).toFixed(2)}</span>
: data.unit === 2 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
- (data.disamt || 0))).toFixed(2)}</span>
: data.unit === 3 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.qty || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
- (data.disamt || 0))).toFixed(2)}</span>
: data.unit === 4 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + (data.makingcha || 0) + (((data.wt * data.rate) * data.polishcha) / 100)) 
- (data.disamt || 0))).toFixed(2)}</span> : ''
}
</TableCell>
: ''
}
</TableRow>)}
<TableRow>
<TableCell rowSpan={12} />
<TableCell style={tableCell}>Subtotal</TableCell>
<TableCell style={tableCell}>{this.getqty()}</TableCell>
<TableCell style={tableCell}>{totalwt}</TableCell>
<TableCell></TableCell>
<TableCell style={tableCell}>{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TextField style={mystyleitem} name="due" id="standard-basic" label="Due"
     value={totalnew} />:this.state.vat === 'yes' ?
     <TextField style={mystyleitem} name="due" id="standard-basic" label="Due"
     value={totalnewi} />:<TextField style={mystyleitem} name="due" id="standard-basic" label="Due"
     value={total1new} />}
</TableCell>{
this.state.vat == 'yes' ?
<TableCell style={tableCell}><TextField style={mystyleitem} name="amt" id="standard-basic" label="Amt."
value={this.state.amt} onChange={this.myChangeHandler} />
<br></br>
<TextField style={mystyleitem} name="amt1" id="standard-basic" label="Amt."
onChange={(e) => { this.setState({ amt1: e.target.value }) }} /></TableCell> :''
}
<TableCell style={tableCell}><TextField style={mystyleitem} name="premark" id="standard-basic" label="Advance"
value={this.state.premarkcash} onChange={(e) => { this.setState({ premarkcash: e.target.value }) }} /><br></br>
<TextField style={mystyleitem} name="premark1" id="standard-basic" label="Remark"
value={this.state.premarkonline} onChange={(e) => { this.setState({ premarkonline: e.target.value }) }} /></TableCell>
<TableCell style={tableCell}><TextField name="mode" id="select" select label='Mode'
value={this.state.mode} style={mystyleitem} onChange={(e) => { this.setState({ mode: e.target.value }) }}>
{
this.state.payresult ?
    this.state.payresult.map((item, i) =>
        <MenuItem key={item.id} value={item.paymode}>{item.paymode}</MenuItem>
    )
    :
    null
}
</TextField><br></br>
<TextField name="mode1" id="select" select label='Mode'
value={this.state.mode1} style={mystyleitem} onChange={(e) => { this.setState({ mode1: e.target.value }) }}>
{
this.state.payresult ?
    this.state.payresult.map((item, i) =>
        <MenuItem key={item.id} value={item.paymode}>{item.paymode}</MenuItem>
    )
    :
    null
}
</TextField></TableCell>
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{sgst1}<br></br>{cgst1}</TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}>{igst}</TableCell>
:<TableCell style={tableCell}><TextField style={mystyleitem} name="amt" id="standard-basic" label="Amt."
value={this.state.amt} onChange={this.myChangeHandler} />
<br></br>
<TextField style={mystyleitem} name="amt1" id="standard-basic" label="Amt." value={this.state.amt1}
onChange={(e) => { this.setState({ amt1: e.target.value }) }} /></TableCell>}
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TableCell style={tableCell}><span>{getTotal2}</span></TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}><span>{totaligst}</span></TableCell>
:<TableCell style={tableCell}><span>{getTotal1}</span></TableCell>}
</TableRow>
</TableBody>
</Table>
</TableContainer>
</div>
</form></Grid>}
<BlockUI blocking={this.state.blocking} />
</div >
)

}

getTotal() {
let grandTotal = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === 1 ? (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) + (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.gst) / 100)- (row.disamt || 0))
    : row.unit === 2 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) + (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.gst)  / 100)- (row.disamt || 0))
    : row.unit === 3 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.qty || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) + (((((row.wt || 0) * (row.rate || 0)) + ((row.qty || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.gst)  / 100)- (row.disamt || 0))
    : row.unit === 4 ? (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) + (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.gst)  / 100)- (row.disamt || 0)) : ''
}
);
if (rowTotals.length > 0) {
grandTotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandTotal;
}

getTotaligst() {
let grandTotal = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === 1 ? (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) + (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.igst) / 100)- (row.disamt || 0))
    : row.unit === 2 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) + (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.igst)  / 100)- (row.disamt || 0))
    : row.unit === 3 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.qty || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) + (((((row.wt || 0) * (row.rate || 0)) + ((row.qty || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.igst)  / 100)- (row.disamt || 0))
    : row.unit === 4 ? (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) + (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.igst)  / 100)- (row.disamt || 0)) : ''
}
);
if (rowTotals.length > 0) {
grandTotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandTotal;
}

getTotalwithout() {
let grandTotal = 0;
const rowTotals = this.state.rowData.map(


row => {
return row.unit === 1 ? ((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * (row.polishcha)) / 100)) 
    - (row.disamt || 0))
    : row.unit === 2 ? ((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * (row.polishcha)) / 100)) 
    - (row.disamt || 0))
    : row.unit === 3 ? ((((row.wt || 0) * (row.rate || 0)) + ((row.qty || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * (row.polishcha)) / 100)) 
    - (row.disamt || 0))
    : row.unit === 4 ? ((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * (row.polishcha)) / 100)) 
    - (row.disamt || 0)) : ''
}
);

if (rowTotals.length > 0) {
grandTotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandTotal;
}

getsgst() {
let grandsgst = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === 1 ? (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.sgst) / 100)
    : row.unit === 2 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * (this.state.sgst) / 100)
    : row.unit === 3 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.makingcha || 0) * (row.qty || 0)) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * (this.state.sgst) / 100)
    : row.unit === 4 ? (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * (this.state.sgst) / 100) : ''
}
);
if (rowTotals.length > 0) {
grandsgst = rowTotals.reduce((acc, val) => acc + val);
}
return grandsgst;
}

getcgst() {
let grandcgst = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === 1 ? (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.cgst) / 100)
    : row.unit === 2 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * (this.state.cgst) / 100)
    : row.unit === 3 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.makingcha || 0) * (row.qty || 0)) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * (this.state.cgst) / 100)
    : row.unit === 4 ? (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * (this.state.cgst) / 100) : ''
}
);
if (rowTotals.length > 0) {
grandcgst = rowTotals.reduce((acc, val) => acc + val);
}
return grandcgst;
}

getigst() {
let grandigst = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === 1 ? (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.igst) / 100)
    : row.unit === 2 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * (this.state.igst) / 100)
    : row.unit === 3 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.makingcha || 0) * (row.qty || 0)) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * (this.state.igst) / 100)
    : row.unit === 4 ? (((((row.wt || 0) * (row.rate || 0)) + (row.makingcha || 0) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * (this.state.igst) / 100) : ''
}
);
if (rowTotals.length > 0) {
grandigst = rowTotals.reduce((acc, val) => acc + val);
}
return grandigst;
}

getqty() {
let grandqty = 0;
const rowTotals = this.state.rowData.map(
row =>
(row.qty) || 0

);

if (rowTotals.length > 0) {
grandqty = rowTotals.reduce((acc, val) => acc + val);
}
return grandqty;
}

getwt() {
let grandwt = 0;
const rowTotals = this.state.rowData.map(
row =>
(row.wt) || 0

);

if (rowTotals.length > 0) {
grandwt = rowTotals.reduce((acc, val) => acc + val);
}
return grandwt;
}



handleRowDelete(row) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy.splice(row, 1);
this.setState({
rowData: rowDataCopy
});
}
handleRowAdd() {
let id = this.state.id;
id = id++;
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy.push({
itemname: "",
qty: 0,
wt: 0,
rate: 0,
unit: "",
polishcha: 0,
makingcha: 0,
disamt: 0,
category:'',
itemremark:''

});
this.setState({
rowData: rowDataCopy,
id: id
});
}
}
export default withRouter(Forms);

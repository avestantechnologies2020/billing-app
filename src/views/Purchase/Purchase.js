import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Remove';
import Select from 'react-select'
import axios from 'axios'
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import PurchasePrint from './PurchasePrint';
import Report from "./Report" ;
import {Link,NavLink} from 'react-router-dom';
import BlockUI from "../../BlockUI/BlockUI";

class Purchase extends React.Component {

constructor() {
super()
var today = new Date(),
time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
var today1 = new Date(),
date = today1.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() ;
// var igst = 3;
// var sgst = 1.5;
// var cgst = 1.5;
// var gst = cgst + sgst;
this.state = {
maxid: "", supplierid: "", suppliername: "", contact: "", address: "", date: "",
commitdate: "", vat: 'no', paytype: "", itemlist: "", rowData: [], itemid: "", id: 0, selectOptions: [], id: "",
name: '', seen: false, seeni: false, category: '', print: false, billdata: "",currentdate : date, currenttime : time,
errorsuppliername:'', citys:'', statelist:'', gst:'', sgst:'', cgst:'', gstnew:'',
igst:'',amt:'',gm:'',totalamt:'',cgstno:'',lastdata:[],sum: 0,suminrate: 0,lastdataG:[],sumG: 0,suminrateG: 0
};
this.getdata();
this.handleQuantiteChange = this.handleQuantiteChange.bind(this);
this.handlePrixChange = this.handlePrixChange.bind(this);
this.handleselectprdtChange = this.handleselectprdtChange.bind(this);
this.handleRowDelete = this.handleRowDelete.bind(this);
this.handleRowAdd = this.handleRowAdd.bind(this);
this.getTotalwithout = this.getTotalwithout.bind(this);
this.getTotalGram = this.getTotalGram.bind(this);
this.handleMakingchaChange = this.handleMakingchaChange.bind(this);
this.submitsup = this.submitsup.bind(this);
this.submititem = this.submititem.bind(this);
}
state = {
    blocking: false,
    user: {}
  };
togglePop = () => {
this.setState({
seen: !this.state.seen
});
};

togglePopi = () => {
this.setState({
seeni: !this.state.seeni
});
};

togglePrint = () => {console.log(this.state.sum)
if(this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH'){
    const total =  this.getTotal() - 0;
    const totalnew = total.toFixed(2);
    const totalgram = this.getTotalGram()-0;
    const gm = totalgram.toFixed(2);
    this.setState({totalamt: totalnew});
    this.setState({gm:gm});
    }else if(this.state.vat === 'yes'){
    const total =  this.getTotaligst() - 0;
    const totalnew = total.toFixed(2);
    const totalgram = this.getTotalGram()-0;
    const gm = totalgram.toFixed(2);
    this.setState({totalamt: totalnew});
    this.setState({gm:gm});
    }else{
    const total1 = this.getTotalwithout() - 0;
    const total1new = total1.toFixed(2);
    const totalgram = this.getTotalGram()-0;
    const gm = totalgram.toFixed(2);
    this.setState({totalamt: total1new});
    this.setState({gm:gm});
    }
this.setState({
print: !this.state.print
});
};

print = () => {
this.setState({
print: !this.state.print,
rowData:[],
supplierid:'',
contact:'',
address:'',
paytype:'',
citys:''
});
window.print();
};

async getdata() {
    this.setState({blocking:true})
const res = await axios.get('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getsup')
const data = res.data
const options = data.map(d => ({
"value": d.id,
"label": d.supplier_name,
"contact": d.contact_no,
"address": d.address,
"citys": d.c_state,
"cgstno":d.gstno
}))
this.setState({ selectOptions: options,blocking:false })
}

handleChange(e) {
    this.setState({blocking:true})
this.setState({ supplierid: e.value, suppliername: e.label, contact: e.contact, address: e.address, 
    errorsuppliername:'',  citys: e.citys,cgstno: e.cgstno,})

    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getlastdata/' + e.value).then((resp) => {
        resp.json().then((result) => {
        this.setState({ lastdata: result })
        })
        })
    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getlastdataG/' + e.value).then((resp) => {
        resp.json().then((result) => {
        this.setState({ lastdataG: result,blocking:false })
        })
        })
}

handleQuantiteChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { qty: parseInt(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleselectprdtChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { itemname: value });
this.setState({
rowData: rowDataCopy
});
}
handlecategoryChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { category: value });
this.setState({
rowData: rowDataCopy
});
}
handleremarkChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { itemremark: value });
this.setState({
rowData: rowDataCopy
});
}
handlePrixChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { rate: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handlePurityChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { purity: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleWasteChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { waste: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleWtChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { wt: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleUnitChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { unit: parseFloat(value, 10) });
this.setState({
    rowData: rowDataCopy
    });
}
handleMakingchaChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { makingcha: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
changevat(e){
this.setState({blocking:true ,vat: e.target.value }) 
if(e.target.value==='yes'){
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getidpurgst').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result ,blocking:false})
})
})
}else{
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getidpur').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result,blocking:false })
})
})
}
if(e.target.value==='yes' && this.state.citys === "MADHYA PRADESH"){
this.setState({ gstnew: this.state.gst })
}else{
this.setState({ gstnew: this.state.igst })
}
}

valid(){
    if(this.state.rowData.length===0){
        this.setState({errorsuppliername:'Supplier Name,Date and Category Are required'})
    }else if(this.state.suppliername.length === 0){
        this.setState({errorsuppliername:'Supplier Name Are required'})
    }else if(this.state.commitdate === ''){
        this.setState({errorsuppliername:'Date Are required'})
    }else if(this.state.category===''){
        this.setState({errorsuppliername:'Category Are required'})
    }else{
        return true
    }
}

submit() {this.setState({blocking:true})
    if(this.valid()){
    if(this.state.vat === 'yes'){
        
    let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/purchase_billgst";
    let data = this.state;
    fetch(url, {
    method: 'POST',
    headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
    },
    body: JSON.stringify(data)
    }).then((result) => {
    result.json().then((resp) => {
        this.setState({blocking:false})
    this.componentDidMount();
    this.togglePrint()
    })
    })
    }else{
    let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/purchase_bill";
    let data = this.state;
    fetch(url, {
    method: 'POST',
    headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
    },
    body: JSON.stringify(data)
    }).then((result) => {
    result.json().then((resp) => {
        this.setState({blocking:false})
    this.componentDidMount();
    this.togglePrint()
    })
    })
    }
}
}
save() {
    if(this.valid()){  
    if(this.state.vat === 'yes'){
        this.setState({blocking:true})
    let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/purchase_billgst";
    debugger
    let data = this.state;
    data.rowData.forEach((e,i)=>{
let tax_per =0;
        if(data.citys != "MADHYA PRADESH"){
             tax_per =  parseFloat(((((((e.wt * e.purity) / 100) + ((e.wt * e.waste) / 100)) * (e.rate))
            +(e.makingcha)) * Number(data.igst) / 100).toFixed(2));
        }else{

            tax_per =  parseFloat(((((((e.wt * e.purity) / 100) + ((e.wt * e.waste) / 100)) * (e.rate))
            +(e.makingcha)) * Number(data.gst) / 100).toFixed(2));
        }
        

    if(i == 0){ debugger
    data.rowData[i]["sum"] = 
    
    (e['category'] === 'Silver' ? 
    (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100)+ ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.sum) : 
    
    (e['category'] === 'Gold' ?
    (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.sumG): '')).toFixed(2)

   
    
    data.rowData[i]["sum_inrate"] =
    (e['category']) === 'Silver' ? (parseFloat(e['rate']) !== 0 ? 
    (parseFloat(e['unit']) === 1 ? parseFloat(e['makingcha']):
    (parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt'])  :
    (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
    (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
    (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + (parseFloat(e['wt']) * parseFloat(e['waste']) / 100)) * parseFloat(e['rate']):
    ('0')) + (parseFloat(data.suminrate)+tax_per).toFixed(2):
    
    (e['category'] === 'Gold' ? (parseFloat(e['rate']) !== 0 ? 
    (parseFloat(e['unit']) === 1 ? parseFloat(e['makingcha']):
    (parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
    (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
    (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
    (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) * parseFloat(e['rate']) :
    ('0')) + parseFloat(data.suminrateG)+tax_per: '').toFixed(2)
    
}else{
    data.rowData[i]["sum"] = 
    (e['category'] === 'Silver' ? 
    (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.rowData[i-1].sum) : 
    
    (e['category'] === 'Gold' ?
    (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.rowData[i-1].sum): '')).toFixed(2)
   
    data.rowData[i]["sum_inrate"] =
            (e['category']) === 'Silver' ? (parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
            parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
            (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
            (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
            (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + (parseFloat(e['wt']) * parseFloat(e['waste']) / 100)) * parseFloat(e['rate']):
            ('0')) + (parseFloat(data.rowData[i-1].sum_inrate)+tax_per).toFixed(2) :
            
            (e['category'] === 'Gold' ?
            (parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
            parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
            (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
            (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
            (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) * parseFloat(e['rate']):
            ('0')) + parseFloat(data.rowData[i-1].sum_inrate)+tax_per: '').toFixed(2)
}
      
    })
    // let data = this.state;

    data.rowData.forEach((e,i)=>{debugger
if(i == 0){
    data.rowData[i]["sum"] = (e['category'] === 'Silver' ? (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) 
    + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.sum) : (e['category'] === 'Gold' ?
    (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.sumG): ''))
    
    data.rowData[i]["sum_inrate"] = (e['category'] === 'Silver' ? (parseFloat(e['rate'])) !== 0 ? (parseFloat(e['unit']) === 1 ?
    parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
    (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
    (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
    (((parseFloat(e['wt']) * parseFloat(e['purity']) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste']) / 100)) * parseFloat(e['rate']))):('0')+parseFloat(data.suminrate): 
    
    
    
    (e['category'] === 'Gold' ? parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
    parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
    (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
    (parseFloat(e['unit']) === 4 ? (parseFloat(e['makingcha'])) : '')))) +
    (((parseFloat(e['wt']) * (parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * (parseFloat(e['waste'])) / 100)) * (parseFloat(e['rate'])))):('0')+(parseFloat(data.suminrateG)): ''))

}
else{
    data.rowData[i]["sum_inrate"] = (e['category'] === 'Silver' ? (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) 
    + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.rowData[i-1].sum) : (e['category'] === 'Gold' ?
    (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.rowData[i-1].sum): ''))

}

    })
    // data.rowData.forEach((e,i)=>{
    //     if(i == 0){
        
    //         data.rowData[i]["sum_inrate"] = (e['category'] === 'Silver' ? parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
    //         parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
    //         (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
    //         (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
    //         (((parseFloat(e['wt']) * parseFloat(e['purity']) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste']) / 100)) * parseFloat(e['rate']))):('0')+parseFloat(data.sum): 
           
    //         (e['category'] === 'Gold' ? parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
    //         parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
    //         (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
    //         (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
    //         (((parseFloat(e['wt']) * parseFloat(e['purity']) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste']) / 100)) * parseFloat(e['rate']))):('0')+parseFloat(data.sumG): ''))
        
    //     }    
    //     else{


    //     }
  
    fetch(url, {
    method: 'POST',
    headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
    },
    body: JSON.stringify(data)
    }).then((result) => {
    result.json().then((resp) => {
    this.componentDidMount();
    this.setState({rowData: [],supplierid:'',suppliername:'',contact:'',
    address:'',paytype:'',amt: '',citys:'',vat:'',sumG:'',blocking:false});
    })
    })
    }else{
        this.setState({blocking:true})
    let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/purchase_bill";
    let data = this.state;
    data.rowData.forEach((e,i)=>{
        if(i == 0){
            data.rowData[i]["sum"] = (e['category'] === 'Silver' ? (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) 
            + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.sum) : (e['category'] === 'Gold' ?
            (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.sumG): '')).toFixed(2)
        
            data.rowData[i]["sum_inrate"] =(e['category']) === 'Silver' ? (parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
                            parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
                            (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
                            (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
                            (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + (parseFloat(e['wt']) * parseFloat(e['waste']) / 100)) * parseFloat(e['rate']):
                            ('0')) + parseFloat(data.suminrate).toFixed(2) :(e['category'] === 'Gold' ?
                            (parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
                            parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
                            (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
                            (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
                            (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) * parseFloat(e['rate']):
                            ('0')) + parseFloat(data.suminrateG): '').toFixed(2)
        }else { 
    
            data.rowData[i]["sum"] = (e['category'] === 'Silver' ? (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) 
            + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.rowData[i-1].sum) : (e['category'] === 'Gold' ?
            (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.rowData[i-1].sum): '')).toFixed(2)
        
            data.rowData[i]["sum_inrate"] =(e['category']) === 'Silver' ? (parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
            parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
            (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
            (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
            (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + (parseFloat(e['wt']) * parseFloat(e['waste']) / 100)) * parseFloat(e['rate']):
            ('0')) + parseFloat(data.rowData[i-1].sum_inrate).toFixed(2) :(e['category'] === 'Gold' ?
            (parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
            parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
            (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
            (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
            (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) * parseFloat(e['rate']):
            ('0')) + parseFloat(data.rowData[i-1].sum_inrate): '').toFixed(2)
    
        }
        debugger
    if(i == 0){
        data.rowData[i]["sum"] = (e['category'] === 'Silver' ? (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) 
        + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.sum) : (e['category'] === 'Gold' ?
        (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.sumG): ''))
    
        data.rowData[i]["sum_inrate"] =(e['category']) === 'Silver' ? (parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
                        parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
                        (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
                        (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
                        (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + (parseFloat(e['wt']) * parseFloat(e['waste']) / 100)) * parseFloat(e['rate']):
                        ('0')) + parseFloat(data.suminrate) :(e['category'] === 'Gold' ?
                        (parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
                        parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
                        (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
                        (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
                        (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) * parseFloat(e['rate']):
                        ('0')) + parseFloat(data.suminrateG): '')
    }else { 

        console.log(data);
        data.rowData[i]["sum"] = (e['category'] === 'Silver' ? (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) 
        + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.rowData[i-1].sum) : (e['category'] === 'Gold' ?
        (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) + parseFloat(data.rowData[i-1].sum): ''))
    
        data.rowData[i]["sum_inrate"] =(e['category']) === 'Silver' ? (parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
        parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
        (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
        (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
        (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + (parseFloat(e['wt']) * parseFloat(e['waste']) / 100)) * parseFloat(e['rate']):
        ('0')) + parseFloat(data.rowData[i-1].sum_inrate) :(e['category'] === 'Gold' ?
        (parseFloat(e['rate']) !== 0 ? (parseFloat(e['unit']) === 1 ?
        parseFloat(e['makingcha']):(parseFloat(e['unit']) === 2 ? parseFloat(e['makingcha']) * parseFloat(e['wt']) :
        (parseFloat(e['unit']) === 3 ? parseFloat(e['makingcha']) * parseFloat(e['qty']) :
        (parseFloat(e['unit']) === 4 ? parseFloat(e['makingcha']) : '')))) +
        (((parseFloat(e['wt']) * parseFloat(e['purity'])) / 100) + ((parseFloat(e['wt']) * parseFloat(e['waste'])) / 100)) * parseFloat(e['rate']):
        ('0')) + parseFloat(data.rowData[i-1].sum_inrate): '')

    }
})
    fetch(url, {
    method: 'POST',
    headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
    },
    body: JSON.stringify(data)
    }).then((result) => {
    result.json().then((resp) => {
    this.componentDidMount();
    this.setState({rowData: [],supplierid:'',suppliername:'',contact:'',
    address:'',paytype:'',amt: '',citys:'',vat:'',blocking:false});
    })
    })
         }
    }
    }
componentDidMount() {
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getstate').then((resp) => {
resp.json().then((result) => {
this.setState({ statelist: result })
})
})
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getitem').then((resp) => {
resp.json().then((result) => {
this.setState({ itemlist: result })
})
})
fetch("http://ydbjewellersserver.jkgroupmanpower.in/public/api/getgst/" + 1,{
method:'GET',
})
.then((response) => response.json())
.then((result) =>{
this.setState({
igst: result.igst,
cgst: result.cgst,
sgst: result.sgst,
gst: Number(result.sgst) + Number(result.cgst)
})
})
if(this.state.vat === 'yes'){
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getidpurgst').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result })
})
})
}else{
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getidpur').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result })
})
})
}
this.getdata();
}

submitsup() {
    this.setState({blocking:true})
let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/addsup";
let data = this.state;
fetch(url, {
method: 'POST',
headers: {
"Content-Type": "application/json",
"Accept": "application/json"
},
body: JSON.stringify(data)
}).then((result) => {
result.json().then((resp) => {
this.componentDidMount();
this.togglePop();
this.setState({customername:'',contact:'', address:'',citys:'',blocking:false})
})
})
};

submititem() {
    this.setState({blocking:true})
let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/additem";
let data = this.state;
fetch(url, {
method: 'POST',
headers: {
"Content-Type": "application/json",
"Accept": "application/json"
},
body: JSON.stringify(data)
}).then((result) => {
result.json().then((resp) => {
    this.setState({blocking:false})
this.componentDidMount();
this.togglePopi();})})
};

Print(){
    this.setState({rowData: [],supplierid:'',suppliername:'',contact:'',
    address:'',paytype:'',amt: '',citys:'',vat:'',print: false,});
    window.print()
    this.componentDidMount();
}

render() {
const total1 = this.getTotalwithout() - this.state.amt;
const total1new = total1.toFixed(2);
const total =  this.getTotal() - this.state.amt;
const totalnew = total.toFixed(2);
const totalamt = this.getTotalwithout()-0;
const amt = totalamt.toFixed(2);
const totalgram = this.getTotalGram()-0;
const gm = totalgram.toFixed(2);
const weight = this.getwt()-0;
const wt1 = weight.toFixed(2);
const gettotal1 = this.getTotal() -0;
const getTotal2 = gettotal1.toFixed(2);
const getsgst = this.getsgst() -0;
const sgst1 = getsgst.toFixed(2);
const getcgst = this.getcgst() -0;
const cgst1 = getcgst.toFixed(2);
const getigst = this.getigst() -0;
const igst = getigst.toFixed(2);
const gettotaligst = this.getTotaligst() - 0;
const totaligst = gettotaligst.toFixed(2);

const mystyle = {color: "white",backgroundColor: "#0ABE95",padding: "10px",fontFamily: "Arial"};
const mystyle1 = {padding: "10px"};
const mystyle2 = {padding: "10px"};
const mystyle3 = {padding: "10px"};
const mystyle4 = {padding: "10px"};
const mystyle5 = {padding: "10px"};
const mystyle6 = {padding: "10px", width: "182px"};
const mystyle7 = {padding: "10px", width: "182px"};
const mystyle8 = {padding: "10px", width: "182px"};
const mystyleitem = {padding: "10px", width: "60px"};
const btnstyle = {padding: "10px",};
const table = {minWidth: 700};
const mystyleq = {padding: "10px", width: "30px"};
const wt = {padding: "10px", width: "40px"};
const rate = {padding: "10px", width: "50px"};
const discount = {padding: "10px", width: "40px"};
const mystyle80 = {width: "40px"};
const makingchange = {padding: "10px", width: "60px"};
const button = {width: 13, height: 13,padding: 0};
const removebutton = {width: 13, height: 13,padding: 0};
const mystypaytype = {padding: 0, width: "195px"}
const customStyles = {
container: provided => ({...provided,width: "225px",paddingtop: "5px"})};
const addcus = {padding: "5px",}
const model = {position: 'fixed',width: '100 px',height: '100 px',};
const content = {backgroundColor: "#0ABE95",position: 'absolute',top: '0px',left: '30px',width: '500px',
padding: '20px',};
const model1 = {position: 'fixed',width: '100 px',height: '100 px'};
const content1 = {backgroundColor: "#0ABE95",position: 'absolute',top: '0px',left: '30px',width: '500px',
padding: '20px',};
const modelprint = {position: 'absolute',};
const contentprint = {backgroundColor: "white",position: 'absolute',top: '-30px',left: '-5px',width: '970px',padding: '20px',
};
const tableCell = {paddingRight: 10,paddingLeft: 10,minWidth:5}
const cstate = { padding: "5px", width: "182px"};
console.log(this.props);
return (

<div>
{this.state.print === false ?
<div>
<span class="no-printme"><Button color="primary" onClick={this.togglePop} >Add Suppliers</Button></span>
<span class="no-printme"><Button color="primary" onClick={this.togglePopi}>Add Items</Button></span>
<span><Button onClick={() => this.save()} color="primary">Save</Button></span>
<span><Button onClick={() => this.submit()} color="primary">Print & Pdf</Button></span>

<span><NavLink to="./Report" exact>
<Button color="primary">Report</Button>
</NavLink>
{this.props.children}</span>

</div>:null
}

{this.state.seen ?
<div style={model}>
<div style={content}>
<form>
<h5>Add Supplier</h5>
<TextField name="suppliername" value={this.state.suppliername} label="Supplier name"
onChange={(e) => { this.setState({ suppliername: e.target.value }) }} style={{ padding: '5px' }} />
<TextField name="contact" value={this.state.contact} label="Contact No."
onChange={(e) => { this.setState({ contact: e.target.value }) }} style={{ padding: '5px' }} />
<TextField name="address" value={this.state.address} label="Address"
onChange={(e) => { this.setState({ address: e.target.value }) }} style={{ padding: '5px' }} />
<TextField name="cgstno" value={this.state.cgstno} label="Gst No."
onChange={(e) => { this.setState({ cgstno: e.target.value }) }} style={{ padding: '5px' }} />
<TextField name="states" id="select" select label="Select State"
value={this.state.citys} style={cstate}
onChange={(e) => { this.setState({citys: e.target.value}) }}>
{
this.state.statelist ?
    this.state.statelist.map((item, i) =>
        <MenuItem key={item.state_id} value={item.state_name}>{item.state_name}</MenuItem>
    )
    :
    null
}
</TextField><br></br><br></br>
<Button variant="contained" onClick={() => this.submitsup()} color="primary">Save</Button>&nbsp;
<Button variant="contained" onClick={() => this.togglePop()} color="primary">Cancel</Button>
</form>
</div>
</div>
: this.state.seeni ?
<div style={model1}>
<div style={content1}>
<form>
<h5>Add Item</h5>
<TextField name="itemname" value={this.state.itemname} label="Itemname"
onChange={(e) => { this.setState({ itemname: e.target.value }) }} style={{ padding: '5px' }} />
<TextField name="category" value={this.state.category} select label="Select Category"
onChange={(e) => { this.setState({ category: e.target.value }) }} style={{ padding: '5px' }} style={cstate}>
<MenuItem value='Gold'>Gold</MenuItem><MenuItem value='Silver'>Silver</MenuItem></TextField><br></br><br></br>
<Button variant="contained" onClick={() => this.submititem()} color="primary">Save</Button>&nbsp;
<Button variant="contained" onClick={() => this.togglePopi()} color="primary">Cancel</Button>
</form>
</div>
</div>
: this.state.print ?

<PurchasePrint data={{Print:this.Print.bind(this),name:this.state.suppliername,invoiceno:this.state.maxid,address:this.state.
address,contact:this.state.contact,vat:this.state.vat,rowData:this.state.rowData,totalamt:this.state.totalamt,
citys:this.state.citys,sgst:this.state.sgst,cgst:this.state.cgst,igst:this.state.igst,gst:this.state.gst,
gm:this.state.gm,cgstno:this.state.cgstno}}/>

: <form autoComplete="off">
<div style={mystyle}>
{
this.state.maxid ?
this.state.maxid.map((item, i) =>
<span>Bill No. :&nbsp;{item.id + 1} </span>):null}
<Select styles={customStyles} options={this.state.selectOptions} onChange={this.handleChange.bind(this)}
placeholder="Select Supplier..."
theme={theme => ({...theme,borderRadius: 0,colors: {...theme.colors,primary25: 'green',neutral0: '#c8c8c8',neutral90: 'white'
},
})} /><p style={{color:'red',fontSize:16,fontStyle:"oblique"}}>{this.state.errorsuppliername}</p>
<TextField style={mystyle1} name="supplierid" value={this.state.supplierid} id="standard-basic" label="Supplier Id"
onChange={(e) => { this.setState({ supplierid: e.target.value }) }} />
<TextField style={mystyle3} name="contact" value={this.state.contact} id="standard-basic" label="Contact No."
onChange={(e) => { this.setState({contact: e.target.value }) }} />
<TextField style={mystyle4} name="address" value={this.state.address} id="standard-basic" label="Address"
onChange={(e) => { this.setState({ address: e.target.value }) }} />
<TextField style={mystyle5} name="citys" id="standard-basic" label="State" value={this.state.citys}
onChange={(e) => { this.setState({ citys: e.target.value }) }} />
<TextField style={mystyle6}
name='date' label="Date" value={this.state.currentdate}
onChange={(e) => { this.setState({ date: e.target.value }) }}
/>
<TextField style={mystyle7}
id="date"
name="commitdate"
label="Commitment Dt."
type="date"
InputLabelProps={{
shrink: true,
}}
onChange={(e) => { this.setState({ commitdate: e.target.value }) }}/>
<TextField style={mystyle8} name="vat" id="select" label="Gst Include" select
onChange={(e) => {this.changevat(e) }}>
<MenuItem value="yes">Yes</MenuItem>
<MenuItem value="no">No</MenuItem>
</TextField>
<TextField style={mystyle7} name="paytype" id="select" label="Payment Type" select
value={this.state.paytype}
onChange={(e) => { this.setState({ paytype: e.target.value }) }}>
<MenuItem value="1">Cash</MenuItem>
<MenuItem value="2">Online</MenuItem>
</TextField>

<TableContainer component={Paper}>
<Table style={table} aria-label="spanning table">
<TableHead>

<TableRow>
<TableCell><IconButton style={button} color="primary" variant="contained"
onClick={this.handleRowAdd}><AddIcon /></IconButton></TableCell>
<TableCell>Item</TableCell>
<TableCell>Qty.</TableCell>
<TableCell>Wt.(gm.)</TableCell>
<TableCell>Purity(%)</TableCell>
<TableCell>Waste(%)</TableCell>
<TableCell>Rate<br></br>(Per gm.)</TableCell>
<TableCell>Making Charge</TableCell>
{this.state.vat === 'yes' && this.state.citys === "MADHYA PRADESH" ?
<TableCell style={tableCell}>SGST<br></br>CGST</TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}>IGST</TableCell>:''}
<TableCell>Amount/Fine</TableCell>
</TableRow>
</TableHead>

<TableBody>
{this.state.rowData.map((data, index) =>
<TableRow key={index} id={index}>
<TableCell><IconButton style={removebutton} color="secondary" variant="contained"
onClick={(e) => this.handleRowDelete(index)} active><RemoveIcon /></IconButton></TableCell>
<TableCell>
<TextField style={mystyleitem} name="itemname" id="select" label="Itemname" select
value={this.state.rowData.itemname}
onChange={(e) => this.handleselectprdtChange(index, e.target.value)}>
{
this.state.itemlist ?
    this.state.itemlist.map((item, i) =>
        <MenuItem key={item.id} value={item.itemname}>{item.itemname}</MenuItem>):null
}
</TextField><hr></hr><br></br>
<TextField name="category" id="select" select label='Cat.'
value={this.state.rowData.category}
onChange={(e) => this.handlecategoryChange(index, e.target.value)}>
<MenuItem value="Gold">Gold</MenuItem>
<MenuItem value="Silver">Silver</MenuItem>
</TextField>&nbsp;&nbsp;
<TextField name="item_remark" id="standard-basic" label="remark." 
value={this.state.rowData.itemremark} onChange={(e) => this.handleremarkChange(index, e.target.value)} />
</TableCell>
<TableCell><TextField style={mystyleq} name="qty" id="standard-basic" label="Qty."
value={this.state.rowData.qty} onChange={(e) => this.handleQuantiteChange(index, e.target.value)} /></TableCell>
<TableCell><TextField style={wt} name="wt" id="standard-basic" label="Weight"
value={this.state.rowData.wt} onChange={(e) => this.handleWtChange(index, e.target.value)} /></TableCell>
<TableCell><TextField style={wt} name="purity" id="standard-basic" label="Purity"
value={this.state.rowData.purity} onChange={(e) => this.handlePurityChange(index, e.target.value)} /></TableCell>
<TableCell><TextField style={wt} name="waste" id="standard-basic" label="Waste"
value={this.state.rowData.waste} onChange={(e) => this.handleWasteChange(index, e.target.value)} /></TableCell>
<TableCell><TextField style={rate} name="rate" id="standard-basic" label="Rate"
value={this.state.rowData.rate} onChange={(e) => this.handlePrixChange(index, e.target.value)} /></TableCell>
<TableCell>
<TableRow><TableCell>
<TextField style={mystyle80} name="unit" id="select" select
value={this.state.rowData.unit}
onChange={(e) => this.handleUnitChange(index, e.target.value)}>
<MenuItem value="1">Fix</MenuItem>
<MenuItem value="2">Per gm.</MenuItem>
<MenuItem value="3">Per pcs.</MenuItem>
<MenuItem value="4">None</MenuItem>
</TextField></TableCell>

<TableCell>{
data.unit === 1 ? <span>{(data.makingcha).toFixed(2)}</span>
: data.unit === 2 ? <span>{((data.wt || 0) * (data.makingcha || 0)).toFixed(2)}</span>
: data.unit === 3 ? <span>{((data.makingcha || 0) * (data.qty || 0)).toFixed(2)}</span>
: data.unit === 4 ? <span>{(data.makingcha || 0).toFixed(2)}</span> : ''
}</TableCell>

</TableRow><TextField style={makingchange} name="makingcha" id="standard-basic" label="Making cha."
value={this.state.rowData.makingcha}
onChange={(e) => this.handleMakingchaChange(index, e.target.value)} />
</TableCell>
{this.state.vat === "yes" && this.state.citys === "MADHYA PRADESH" ?
<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{this.state.sgst}</span>&nbsp;
{
data.unit === 1 ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) * (data.rate).toFixed(2))
+(data.makingcha)) * Number(this.state.sgst) / 100).toFixed(2)}</span>
:data.unit === 2 ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) *(data.rate).toFixed(2))
+(data.wt * data.makingcha)) * Number(this.state.sgst) / 100).toFixed(2)}</span>:
data.unit === 3 ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))* (data.rate).toFixed(2))
+(data.qty * data.makingcha)) * Number(this.state.sgst) / 100).toFixed(2)}</span>:
data.unit === 4 ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) * (data.rate))
+(data.makingcha)) * Number(this.state.sgst) / 100).toFixed(2)}</span>:''
}
<br></br>
<span style={{ backgroundColor: "red" }}>{this.state.cgst}</span>&nbsp;
{
data.unit === 1 ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) * (data.rate).toFixed(2))
+(data.makingcha)) * Number(this.state.cgst) / 100).toFixed(2)}</span>
:data.unit === 2 ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) *(data.rate).toFixed(2))
+(data.wt * data.makingcha)) * Number(this.state.cgst) / 100).toFixed(2)}</span>:
data.unit === 3 ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))* (data.rate).toFixed(2))
+(data.qty * data.makingcha)) * Number(this.state.cgst) / 100).toFixed(2)}</span>:
data.unit === 4 ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) * (data.rate).toFixed(2))
+(data.makingcha)) * Number(this.state.cgst) / 100).toFixed(2)}</span>:''
}
</TableCell>:this.state.vat === 'yes' ?
<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{this.state.igst}</span>&nbsp;
{
data.unit === 1 ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) * (data.rate).toFixed(2))
+(data.makingcha)) * Number(this.state.igst) / 100).toFixed(2)}</span>
:data.unit === 2 ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) *(data.rate).toFixed(2))
+(data.wt * data.makingcha)) * Number(this.state.igst) / 100).toFixed(2)}</span>:
data.unit === 3 ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))* (data.rate).toFixed(2))
+(data.qty * data.makingcha)) * Number(this.state.igst) / 100).toFixed(2)}</span>:
data.unit === 4 ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) * (data.rate).toFixed(2))
+(data.makingcha)) * Number(this.state.igst) / 100).toFixed(2)}</span>:''
}
</TableCell>:''}
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{
data.rate === 0 ?
<span>{(((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:data.unit === 1 ? <span>{(((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) * (data.rate).toFixed(2))
+(data.makingcha)) * Number(this.state.gst) / 100) + (((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100)) * (data.rate)) + (data.makingcha))).toFixed(2)}</span>
:data.unit === 2 ? <span>{(((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) *(data.rate).toFixed(2))
+(data.wt * data.makingcha)) * Number(this.state.gst) / 100)+(((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100)) *(data.rate)) + (data.wt * data.makingcha))).toFixed(2)}</span>:
data.unit === 3 ? <span>{(((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))* (data.rate).toFixed(2))
+(data.qty * data.makingcha))* Number(this.state.gst) /100) + (((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100))* (data.rate)) + (data.qty * data.makingcha))).toFixed(2)}</span>:
data.unit === 4 ? <span>{(((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) * (data.rate).toFixed(2))
+(data.makingcha))* Number(this.state.gst) / 100) + (((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100)) * (data.rate)) + (data.makingcha))).toFixed(2)}</span>:''
}
</TableCell>:this.state.vat === 'yes' ?
<TableCell style={tableCell}>{
data.rate === 0 ?
<span>{(((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:data.unit === 1 ? <span>{(((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) * (data.rate).toFixed(2))
+(data.makingcha)) * Number(this.state.igst) / 100) + (((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100)) * (data.rate)) + (data.makingcha))).toFixed(2)}</span>
:data.unit === 2 ? <span>{(((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) *(data.rate).toFixed(2))
+(data.wt * data.makingcha)) * Number(this.state.igst) / 100)+(((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100)) *(data.rate)) + (data.wt * data.makingcha))).toFixed(2)}</span>:
data.unit === 3 ? <span>{(((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))* (data.rate).toFixed(2))
+(data.qty * data.makingcha))* Number(this.state.igst) /100) + (((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100))* (data.rate)) + (data.qty * data.makingcha))).toFixed(2)}</span>:
data.unit === 4 ? <span>{(((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) * (data.rate).toFixed(2))
+(data.makingcha))* Number(this.state.igst) / 100) + (((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100)) * (data.rate)) + (data.makingcha))).toFixed(2)}</span>:''
}
</TableCell>:''}
{this.state.vat === 'no' ?
<TableCell>{
data.rate === 0 ?
<span>{(((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:data.unit === 1 ? <span>{(((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) * (data.rate).toFixed(2))
+(data.makingcha)).toFixed(2)}</span>
:data.unit === 2 ? <span>{(((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) *(data.rate).toFixed(2))
+(data.wt * data.makingcha)).toFixed(2)}</span>:
data.unit === 3 ? <span>{(((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))* (data.rate).toFixed(2))
+(data.qty * data.makingcha)).toFixed(2)}</span>:
data.unit === 4 ? <span>{(((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) * (data.rate).toFixed(2))
+(data.makingcha)).toFixed(2)}</span>:''
}</TableCell>
: ''
}
</TableRow>)}
<TableRow>
<TableCell rowSpan={12} />
<TableCell>Subtotal</TableCell>
<TableCell>{this.getqty()}</TableCell>
<TableCell>{wt1}</TableCell>
<TableCell></TableCell>
<TableCell></TableCell>
<TableCell style={tableCell}>
</TableCell>{
this.state.vat == 'yes' ?
<TableCell style={tableCell}></TableCell> :''
}
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{sgst1}<br></br>{cgst1}</TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}>{igst}</TableCell>
:<TableCell style={tableCell}></TableCell>}
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TableCell style={tableCell}><span>{getTotal2}</span></TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}><span>{totaligst}</span></TableCell>
:<TableCell><span>{amt}<br></br>{gm}&nbsp;gm</span></TableCell>}
</TableRow>
</TableBody>
</Table>
</TableContainer>
</div>
</form>} <BlockUI blocking={this.state.blocking} />
</div> 
)}

getTotal() {
let grandTotal = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === 1 ? (((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) * (row.rate))
    +(row.makingcha)) * Number(this.state.gst) / 100) + (((((row.wt * row.purity) / 100) 
    + ((row.wt * row.waste) / 100)) * (row.rate)) + (row.makingcha)))
    :row.unit === 2 ? (((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) *(row.rate))
    +(row.wt * row.makingcha)) * Number(this.state.gst) / 100)+(((((row.wt * row.purity) / 100) 
    + ((row.wt * row.waste) / 100)) *(row.rate)) + (row.wt * row.makingcha))):
    row.unit === 3 ? (((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100))* (row.rate))
    +(row.qty * row.makingcha))* Number(this.state.gst) /100) + (((((row.wt * row.purity) / 100) 
    + ((row.wt * row.waste) / 100))* (row.rate)) + (row.qty * row.makingcha))):
    row.unit === 4 ? (((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) * (row.rate))
    +(row.makingcha))* Number(this.state.gst) / 100) + (((((row.wt * row.purity) / 100) 
    + ((row.wt * row.waste) / 100)) * (row.rate)) + (row.makingcha))):''
});
if (rowTotals.length > 0) {
grandTotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandTotal;
}
    
getTotaligst() {
let grandTotal = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === 1 ? (((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) * (row.rate))
    +(row.makingcha)) * Number(this.state.igst) / 100) + (((((row.wt * row.purity) / 100) 
    + ((row.wt * row.waste) / 100)) * (row.rate)) + (row.makingcha)))
    :row.unit === 2 ? (((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) *(row.rate))
    +(row.wt * row.makingcha)) * Number(this.state.igst) / 100)+(((((row.wt * row.purity) / 100) 
    + ((row.wt * row.waste) / 100)) *(row.rate)) + (row.wt * row.makingcha))):
    row.unit === 3 ? (((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100))* (row.rate))
    +(row.qty * row.makingcha))* Number(this.state.igst) /100) + (((((row.wt * row.purity) / 100) 
    + ((row.wt * row.waste) / 100))* (row.rate)) + (row.qty * row.makingcha))):
    row.unit === 4 ? (((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) * (row.rate))
    +(row.makingcha))* Number(this.state.igst) / 100) + (((((row.wt * row.purity) / 100) 
    + ((row.wt * row.waste) / 100)) * (row.rate)) + (row.makingcha))):''
});
if (rowTotals.length > 0) {
grandTotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandTotal;
}
getTotalwithout() {
let grandTotal = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === 1 ? (((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) * (row.rate))
    +(row.makingcha))
    :row.unit === 2 ? (((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) *(row.rate))
    +(row.wt * row.makingcha)):
    row.unit === 3 ? (((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100))* (row.rate))
    +(row.qty * row.makingcha)):
    row.unit === 4 ? (((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) * (row.rate))
    +(row.makingcha)):''
});
if (rowTotals.length > 0) {
grandTotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandTotal;
}
getsgst() {
let grandsgst = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === 1 ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) * (row.rate))
    +(row.makingcha)) * Number(this.state.sgst) / 100)
    :row.unit === 2 ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) *(row.rate))
    +(row.wt * row.makingcha)) * Number(this.state.sgst) / 100):
    row.unit === 3 ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100))* (row.rate))
    +(row.qty * row.makingcha)) * Number(this.state.sgst) / 100):
    row.unit === 4 ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) * (row.rate))
    +(row.makingcha)) * Number(this.state.sgst) / 100):''
});
if (rowTotals.length > 0) {
grandsgst = rowTotals.reduce((acc, val) => acc + val);
}
return grandsgst;
}

getcgst() {
let grandcgst = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === 1 ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) * (row.rate))
    +(row.makingcha)) * Number(this.state.cgst) / 100)
    :row.unit === 2 ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) *(row.rate))
    +(row.wt * row.makingcha)) * Number(this.state.cgst) / 100):
    row.unit === 3 ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100))* (row.rate))
    +(row.qty * row.makingcha)) * Number(this.state.cgst) / 100):
    row.unit === 4 ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) * (row.rate))
    +(row.makingcha)) * Number(this.state.cgst) / 100):''
});
if (rowTotals.length > 0) {
grandcgst = rowTotals.reduce((acc, val) => acc + val);
}
return grandcgst;
}

getigst() {
let grandigst = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === 1 ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) * (row.rate))
    +(row.makingcha)) * Number(this.state.igst) / 100)
    :row.unit === 2 ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) *(row.rate))
    +(row.wt * row.makingcha)) * Number(this.state.igst) / 100):
    row.unit === 3 ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100))* (row.rate))
    +(row.qty * row.makingcha)) * Number(this.state.igst) / 100):
    row.unit === 4 ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) * (row.rate))
    +(row.makingcha)) * Number(this.state.igst) / 100):''
}
);
if (rowTotals.length > 0) {
grandigst = rowTotals.reduce((acc, val) => acc + val);
}
return grandigst;
}
getTotalGram() {
    let grandTotal = 0;
    const rowTotals = this.state.rowData.map(
    row => {
    return ((((row.wt || 0) * (row.purity || 0)) / 100) + (((row.wt||0) * (row.waste||0)) / 100))
    }
    );
    if (rowTotals.length > 0) {
    grandTotal = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandTotal;
    }

getqty() {
let grandqty = 0;
const rowTotals = this.state.rowData.map(
row =>
(row.qty) || 0

);

if (rowTotals.length > 0) {
grandqty = rowTotals.reduce((acc, val) => acc + val);
}
return grandqty;
}
getwt() {
let grandwt = 0;
const rowTotals = this.state.rowData.map(
row =>
(row.wt) || 0

);

if (rowTotals.length > 0) {
grandwt = rowTotals.reduce((acc, val) => acc + val);
}
return grandwt;
}

getsum() {
let grandwt = 0;
const rowTotals = this.state.lastdata.map(
row =>
(row.sum) || 0

);

if (rowTotals.length > 0) {
grandwt = rowTotals.reduce((acc, val) => acc + val);
}
return grandwt;
}
getsuminrate() {
let grandwt = 0;
const rowTotals = this.state.lastdata.map(
row =>
(row.sum_inrate) || 0

);

if (rowTotals.length > 0) {
grandwt = rowTotals.reduce((acc, val) => acc + val);
}
return grandwt;
}
getsumG() {
let grandwt = 0;
const rowTotals = this.state.lastdataG.map(
row =>
(row.sum) || 0

);

if (rowTotals.length > 0) {
grandwt = rowTotals.reduce((acc, val) => acc + val);
}
return grandwt;
}
getsuminrateG() {
let grandwt = 0;
const rowTotals = this.state.lastdataG.map(
row =>
(row.sum_inrate) || 0

);

if (rowTotals.length > 0) {
grandwt = rowTotals.reduce((acc, val) => acc + val);
}
return grandwt;
}



handleRowDelete(row) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy.splice(row, 1);
this.setState({
rowData: rowDataCopy
});
}
handleRowAdd() {
this.setState({sum:this.getsum(),suminrate:this.getsuminrate(),sumG:this.getsumG(),suminrateG:this.getsuminrateG()})
let id = this.state.id;
id = id++;
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy.push({
itemname: "",
category:'',
itemremark:'',
qty: 0,
wt: 0,
purity:'',
waste:'',
rate: 0,
unit: "",
makingcha: 0,
});
this.setState({
rowData: rowDataCopy,
id: id
});
}
}
export default withRouter(Purchase);

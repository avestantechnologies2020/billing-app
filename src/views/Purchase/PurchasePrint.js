import React from 'react';
import avatar1 from '../../views/logo.jpg';
import { ToWords } from 'to-words';

class PurchasePrint extends React.Component {
    constructor(props) {
        super(props)
        var today = new Date(),
        time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
        var today1 = new Date(),
        date = today1.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() ;
        this.state = {maxid:'',time:time,date:date,vat:this.props.data.vat,citys:this.props.data.citys,
        rowData:this.props.data.rowData,cgst:this.props.data.cgst,sgst:this.props.data.sgst,igst:this.props.data.igst,
        gst:this.props.data.gst,comment:''
        };
    }
    getterms(){
        fetch("http://ydbjewellersserver.jkgroupmanpower.in/public/api/getterms/" + 1,{
        method:'GET',
        })
        .then((response) => response.json())
        .then((result) =>{
        this.setState({
        comment: result.comment,
        })
    })}

    getvat(){
        if(this.state.vat === "yes")
        {
            return ("TAX INVOICE");
        }
        else{
            return("ESTIMATE");
        }
    }
    getvat1(){
        if(this.state.vat === "yes")
        {
            return (this.props.data.cgstno);
        }
        else{
            return null;
        }
    }

    getvat2(){
        if(this.state.vat === "yes")
        {
            return ("23AHFPB6762Q1Z1");
        }
        else{
            return null;
        }
    }

componentDidMount() {
this.getterms();
if (this.state.vat === 'yes') {
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getidpurgst').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result })
})
})
} else {
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getidpur').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result })
})
})
}
}

render() {
const numberFormat = (value) =>
new Intl.NumberFormat('en-IN', {
    style: 'currency',
    currency: 'INR',
    minimumFractionDigits: 0
}).format(value);

const toWords = new ToWords({
    localeCode: 'en-IN',
    converterOptions: {
      currency: true,
      ignoreDecimal: false,
      ignoreZeroCurrency: false,
    }
  });

    return (
        <>
        <><table><tr><td></td></tr><tr><td></td></tr></table></>
        <div className='main'>
            <div className='content'>
            <div className='name'>YDB JEWELLERS</div>
            <div className='gstd'>{this.props.data.vat === 'yes' ?
                <span className='rgst'></span>:
                <span className='rgst'><br></br></span>}
                    </div>
            </div><br/>
            <div className='logoside'>
            <div className="logoside1"><img style={{height:'85px', width:'90px', marginTop: '-40px', marginBottom: '-40px' }} 
            src={avatar1} /></div>
            </div>

            <div className="date" >
                    <div className="date1"><b><span>INVOICE DATE : {this.state.date}&nbsp;&nbsp;&nbsp;Time : 
                    {this.state.time}</span></b></div>
            <div className="date2"> {
            this.state.maxid ?
            this.state.maxid.map((item, i) =>
            <b><span className='invno'>INVOICE NO. :&nbsp; 
            {this.props.data.editid ? this.props.data.editid : item.id + 1} 
            </span></b>
            ):null}</div>
                </div>
                <table className="table1">
                <tr className="tr">
                    <th className="th2" width="60px">
                    
                    <span>Bill TO: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GST N:&nbsp;&nbsp;
                    {this.getvat1()}</span>
                    </th>
                    <th className="th3" width="60px">
                    <span>{this.getvat()}</span>
                    </th>
                    <th className="th1" width='60px'>
                    BILL BY: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GST N:&nbsp;&nbsp;
                    {this.getvat2()}
                    </th>
                </tr></table><br/>

                <table className="table" border='1'>
                <tr className="tr">
                    <th className="th10" width="90px">
                    <div className='address'>
                    {/* <span>CUSTOMER NAME : {this.props.data.name}</span><br/>
                    <span>ADDRESS : {this.props.data.address}</span><br/>
                    <span>CONTACT NO. : {this.props.data.contact}</span> */}
                    <span>{this.props.data.name}</span><br/>
                    <span>{this.props.data.address}</span><br/>
                    {/* <span>{this.props.data.address}</span><br/> */}
                    <span>{this.props.data.contact}</span>
                    </div>
                    </th>
                    <th className="th11" width='90px'>
                    <div className='address1'>
                        <span>Yashpal Dharmendra Kumar Bhardwaj</span><br/>
                        <span>MOR GALI, SARAFA BAZAR,</span><br/>
                        <span>LASHKAR,GWALIOR, M.P.</span><br/>
                        <span>Telephone : 07514084382</span>
                    </div>
                    </th>
                </tr></table><br/><br/>
                <div className='invhead'><strong>PRODUCT DESCRIPTION</strong></div>
                




            {/* <div className='address'><span>SUPPLIER NAME : {this.props.data.name}</span>
            <span>ADDRESS : {this.props.data.address}</span>
            <span>CONTACT NO. : {this.props.data.contact}</span></div>
            <div className='invhead'><strong>INVOICE</strong></div>
            <div className='datetime'>{
            this.state.maxid ?
            this.state.maxid.map((item, i) =>
            <span className='invno'>INVOICE NO. :&nbsp;{this.props.data.editid ? this.props.data.editid : item.id} </span>
            ):null}
            {this.props.data.cgstno ?
            <span>GST NO : {this.props.data.cgstno}</span>:<span><br></br></span>
            }
            <span>DATE : {this.state.date}&nbsp;Time : {this.state.time}</span></div> */}
            <div>
            <table className="table2">
                <tr className="tr">
                    <th className="th" width="150px">ITEM</th>
                    <th className="th" width='30px'>QT</th>
                    <th className="th" width='35px'>WT<br></br>(GM)</th>
                    <th className="th">PURITY<br></br>(%)</th>
                    <th className="th">WASTE<br></br>(%)</th>
                    <th className="th">RATE</th>
                    <th className="th">MAKING</th>
                    {this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
                    <th className="th">SGST({Number(this.state.sgst)}%)<br></br> 
                        CGST({Number(this.state.cgst)}%)</th>:
                    this.state.vat === 'yes' ?
                    <th className="th">IGST({Number(this.state.igst)}%)</th>:
                    <th className="th">GST</th>
                    }
                    <th className="th">TOTAL</th>
                </tr>
                {this.state.rowData ?
                 this.state.rowData.map((item, i) =>
                 <tr className="tr">
                 <td className="td">{item.itemname}<br></br>{item.itemremark}</td>
                 <td className="td">{item.qty}</td>
                 <td className="td">{item.wt}</td>
                 <td className="td">{item.purity}</td>
                 <td className="td">{item.waste}</td>
                 <td className="td">{numberFormat(item.rate)}</td>
                 <td className="td">{
                 item.unit === 1 || item.unit === '1' ? <span>{numberFormat(Number(item.makingcha))}(FIX)</span>
                : item.unit === 2 || item.unit === '2' ? <span>{numberFormat(item.makingcha)}(PER GM)</span>
                : item.unit === 3 || item.unit === '3' ? <span>{numberFormat(item.makingcha)}(PER PC)</span>
                : item.unit === 4 || item.unit === '4' ? <span>{numberFormat(Number(item.makingcha))}(NONE)</span> : ''}
                 </td>
{this.state.vat === "yes" && this.state.citys === "MADHYA PRADESH" ?
<td className="td"><b>
{
item.unit === 1 || item.unit === '1' ? <span>{numberFormat((((Number((item.wt * item.purity) / 100) + Number((item.wt * item.waste) / 100)) * (item.rate))
+Number(item.makingcha)) * Number(this.state.sgst) / 100)}</span>
:item.unit === 2 || item.unit === '2' ? <span>{numberFormat((((((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100)) *(item.rate))
+(item.wt * item.makingcha)) * Number(this.state.sgst) / 100)}</span>:
item.unit === 3 || item.unit === '3' ? <span>{numberFormat((((((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100))* (item.rate))
+(item.qty * item.makingcha)) * Number(this.state.sgst) / 100)}</span>:
item.unit === 4 || item.unit === '4' ? <span>{numberFormat((((Number((item.wt * item.purity) / 100) + Number((item.wt * item.waste) / 100)) * (item.rate))
+Number(item.makingcha)) * Number(this.state.sgst) / 100)}</span>:''
}<br></br>
{
item.unit === 1 || item.unit === '1' ? <span>{numberFormat((((Number((item.wt * item.purity) / 100) + Number((item.wt * item.waste) / 100)) * (item.rate))
+Number(item.makingcha)) * Number(this.state.cgst) / 100)}</span>
:item.unit === 2 || item.unit === '2' ? <span>{numberFormat((((((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100)) *(item.rate))
+(item.wt * item.makingcha)) * Number(this.state.cgst) / 100)}</span>:
item.unit === 3 || item.unit === '3' ? <span>{numberFormat((((((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100))* (item.rate))
+(item.qty * item.makingcha)) * Number(this.state.cgst) / 100)}</span>:
item.unit === 4 || item.unit === '4' ? <span>{numberFormat((((Number((item.wt * item.purity) / 100) + Number((item.wt * item.waste) / 100)) * (item.rate))
+Number(item.makingcha)) * Number(this.state.cgst) / 100)}</span>:''
}
</b></td>:this.state.vat === "yes" ?
<td className="td">
{
item.unit === 1 || item.unit === '1' ? <span>{numberFormat((((Number((item.wt * item.purity) / 100) + Number((item.wt * item.waste) / 100)) * (item.rate))
+Number(item.makingcha)) * Number(this.state.igst) / 100)}</span>
:item.unit === 2 || item.unit === '2' ? <span>{numberFormat((((((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100)) *(item.rate))
+(item.wt * item.makingcha)) * Number(this.state.igst) / 100)}</span>:
item.unit === 3 || item.unit === '3' ? <span>{numberFormat((((((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100))* (item.rate))
+(item.qty * item.makingcha)) * Number(this.state.igst) / 100)}</span>:
item.unit === 4 || item.unit === '4' ? <span>{numberFormat((((Number((item.wt * item.purity) / 100) + Number((item.wt * item.waste) / 100)) * (item.rate))
+Number(item.makingcha)) * Number(this.state.igst) / 100)}</span>:''
}
</td>:<td className="td">-</td>}
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<td className="td">
 {
 item.rate === 0 || item.rate === '0' ?
 <span>{(((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
 :item.unit === 1 || item.unit === '1' ? <span>{numberFormat(((((Number((item.wt * item.purity) / 100) + Number((item.wt * item.waste) / 100)) * (item.rate))
 +Number(item.makingcha)) * Number(this.state.gst) / 100) + Number(((((item.wt * item.purity) / 100) 
 + Number((item.wt * item.waste) / 100)) * (item.rate)) + Number(item.makingcha)))}</span>
 :item.unit === 2 || item.unit === '2' ? <span>{numberFormat(((((((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100)) *(item.rate))
 +(item.wt * item.makingcha)) * Number(this.state.gst) / 100)+(((((item.wt * item.purity) / 100) 
 + ((item.wt * item.waste) / 100)) *(item.rate)) + (item.wt * item.makingcha)))}</span>:
 item.unit === 3 || item.unit === '3' ? <span>{numberFormat(((((((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100))* (item.rate))
 +(item.qty * item.makingcha))* Number(this.state.gst) /100) + (((((item.wt * item.purity) / 100) 
 + ((item.wt * item.waste) / 100))* (item.rate)) + (item.qty * item.makingcha)))}</span>:
 item.unit === 4 || item.unit === '4' ? <span>{numberFormat(((((Number((item.wt * item.purity) / 100) + Number((item.wt * item.waste) / 100)) * (item.rate))
 +Number(item.makingcha))* Number(this.state.gst) / 100) + Number(((((item.wt * item.purity) / 100) 
 + Number((item.wt * item.waste) / 100)) * (item.rate)) + Number(item.makingcha)))}</span>:''
 }   
</td>:this.state.vat === 'yes' ?
<td className="td"><b>
{
item.rate === 0 || item.rate === '0' ?
<span>{(((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:item.unit === 1 || item.unit === '1' ? <span>{numberFormat(((((Number((item.wt * item.purity) / 100) + Number((item.wt * item.waste) / 100)) * (item.rate))
+Number(item.makingcha)) * Number(this.state.igst) / 100) + Number(((((item.wt * item.purity) / 100) 
+ ((item.wt * item.waste) / 100)) * (item.rate)) + (item.makingcha)))}</span>
:item.unit === 2 || item.unit === '2' ? <span>{numberFormat(((((((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100)) *(item.rate))
+(item.wt * item.makingcha)) * Number(this.state.igst) / 100)+(((((item.wt * item.purity) / 100) 
+ ((item.wt * item.waste) / 100)) *(item.rate)) + (item.wt * item.makingcha)))}</span>:
item.unit === 3 || item.unit === '3' ? <span>{numberFormat(((((((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100))* (item.rate))
+(item.qty * item.makingcha))* Number(this.state.igst) /100) + (((((item.wt * item.purity) / 100) 
+ ((item.wt * item.waste) / 100))* (item.rate)) + (item.qty * item.makingcha)))}</span>:
item.unit === 4 || item.unit === '4' ? <span>{numberFormat(((((Number((item.wt * item.purity) / 100) + Number((item.wt * item.waste) / 100)) * (item.rate))
+Number(item.makingcha))* Number(this.state.igst) / 100) + Number(((((item.wt * item.purity) / 100) 
+ Number((item.wt * item.waste) / 100)) * (item.rate)) + Number(item.makingcha)))}</span>:''
}</b></td>:this.state.vat === 'no' ?
<td className="td"><b>
{
item.rate === 0 || item.rate === '0' ?
<span>{(((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:item.unit === 1 || item.unit === '1' ? <span>{numberFormat(((Number((item.wt * item.purity) / 100) + Number((item.wt * item.waste) / 100)) * (item.rate))
+Number(item.makingcha))}</span>
:item.unit === 2 || item.unit === '2' ? <span>{numberFormat(((((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100)) *(item.rate))
+(item.wt * item.makingcha))}</span>:
item.unit === 3 || item.unit === '3' ? <span>{numberFormat(((((item.wt * item.purity) / 100) + ((item.wt * item.waste) / 100))* (item.rate))
+(item.qty * item.makingcha))}</span>:
item.unit === 4 || item.unit === '4' ? <span>{numberFormat(((Number((item.wt * item.purity) / 100) + Number((item.wt * item.waste) / 100)) * (item.rate))
+Number(item.makingcha))}</span>:''
} </b>
</td>:null}
             </tr>):null}
             
                <tr className="tr">
                    <td colspan="7" className="td">
                        <div className="words">
                        <span className="termsp">{toWords.convert(this.props.data.totalamt)}</span>
                        </div>
                    </td>
                <td className="tdtotal"><b>TOTAL</b><br></br><b>GRAM</b></td>
                <td className="tdtotal"><b>{numberFormat(this.props.data.totalamt)}</b><br></br>
                <b>{this.props.data.gm}</b></td>
                </tr>
            </table>
            </div><br/>
            <div className="foo1">
                <strong>SUPPLIER SIGNATURE</strong>
                <strong className="auths">AUTHORISED SIGNATURE</strong>
            </div>
        </div>
        <div className="foo2">
        {this.state.comment}
        </div>
        <button onClick={()=>{this.props.data.Print()}} class="no-printme">Print</button>
        </>
    )
    }
}
export default PurchasePrint;
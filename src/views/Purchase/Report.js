import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Select from 'react-select';
import axios from 'axios';
import { Grid } from '@material-ui/core';
import BlockUI from "../../BlockUI/BlockUI";

export default class Report extends React.Component {
    constructor() {
        super()
        var today = new Date(),
        date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() ;
        this.state = { selectOptions : [], date:date,repaydata: [], s_id:'', name:'', remark:'', address:'',
        citys:'',purdata: [],purdataG: [],lastdata: [],sum: 0,suminrate: 0,
        lastdataG:[],sumG: 0,suminrateG: 0,};
    }
PrintPdf(){
    window.print()
}
state = {
    blocking: false,
    user: {}
  };
async getdata() {
    this.setState({blocking:true})
    const res = await axios.get('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getsup')
    const data = res.data
    
    const options = data.map(d => ({
    "value": d.id,
    "label": d.supplier_name,
    "remark": d.contact_no,
    "address": d.address,
    "citys": d.c_state }))
    this.setState({ selectOptions: options,blocking:false })
    }
handleChange(e) {
    this.setState({blocking: true});
this.setState({ s_id: e.value, name: e.label, remark: e.remark,address: e.address,citys: e.citys})
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getpurchasebal/' + e.value).then((resp) => {
                resp.json().then((result) => {
                    this.setState({ purdata: result })
                })
            })
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getpurchasebalG/' + e.value).then((resp) => {
    resp.json().then((result) => {
        this.setState({ purdataG: result })
    })
})
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getlastdata/' + e.value).then((resp) => {
        resp.json().then((result) => {
        this.setState({ lastdata: result })
        })
        })
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getlastdataG/' + e.value).then((resp) => {
  resp.json().then((result) => {
  this.setState({ lastdataG: result,blocking:false })
  })
  })
}

componentDidMount(){
    this.getdata();
}

render() {
const numberFormat = (value) =>
new Intl.NumberFormat('en-IN', {
    style: 'currency',
    currency: 'INR'
}).format(value);
const tableCell = {padding:'4px'}
return (
    <div>
        <div style={{ width: '1200px', padding: '5px' }} class="no-printme">
            <Grid container spacing={1}>
            <Grid item xs={2}>
            &nbsp;<input type='submit' value='Print && Pdf' name='pdf' className='pdf' onClick={this.PrintPdf}></input>
        </Grid>
        <Grid item xs={2}>
        <Select options={this.state.selectOptions} onChange={this.handleChange.bind(this)}
                placeholder="Select Sup." className='textfield' />
            </Grid>  
        </Grid>     
    </div>
        
{this.state.s_id ?
<Grid container spacing={1}>
<Grid item xs={4}>
<p>Supplier Name :&nbsp;{this.state.name}</p>
<p>Supplier Id :&nbsp;{this.state.s_id}</p>
</Grid>
<Grid item xs={4}>
<p>Address :&nbsp;{this.state.address}</p>
<p>State :&nbsp;{this.state.citys}</p>
</Grid>
    <TableContainer component={Paper}>
    <Table aria-label="spanning table">
        <TableHead>
            <TableRow>
                <TableCell align='center'>Invoice No.</TableCell>
                <TableCell align='center'>Date</TableCell>
                <TableCell align='center'>Remark</TableCell>
                <TableCell align='center'>Debit Amt.</TableCell>
                <TableCell align='center'>Credit Amt.</TableCell>
                <TableCell align='center'>Balance</TableCell>
                <TableCell align='center'>Debit Gold</TableCell>
                <TableCell align='center'>Credit Gold</TableCell>
                <TableCell align='center'>Balance</TableCell>
                <TableCell align='center'>Debit Silver</TableCell>
                <TableCell align='center'>Credit Silver</TableCell>
                <TableCell align='center'>Balance</TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
                {
                this.state.purdata ?
                this.state.purdata.map((data,i) =>
                <TableRow>
                <TableCell align='center'>{data.type}/{data.bill_no}</TableCell>
                <TableCell align='center'>{data.date_update}</TableCell>
                <TableCell align='center'>{data.itemremark}</TableCell>
                <TableCell align='center'>{data.total_ratei}</TableCell>
                <TableCell align='center'>{data.total_rate}</TableCell>
                <TableCell align='center'>{data.sum_inrate}</TableCell>
                <TableCell align='center'>-</TableCell>
                <TableCell align='center'>-</TableCell>
                <TableCell align='center'>-</TableCell>
                <TableCell align='center'>{data.total_grami}</TableCell>
                <TableCell align='center'>{data.total_gram}</TableCell>
                <TableCell align='center'>{data.sum}</TableCell>
                </TableRow>   
                ):null
                }
                
                {
                this.state.purdataG ?
                this.state.purdataG.map((data,i) =>
                <TableRow>
                <TableCell align='center'>{data.type}/{data.bill_no}</TableCell>
                <TableCell align='center'>{data.date_update}</TableCell>
                <TableCell align='center'>{data.itemremark}</TableCell>
                <TableCell align='center'>{data.total_ratei}</TableCell>
                <TableCell align='center'>{data.total_rate}</TableCell>
                <TableCell align='center'>{data.sum_inrate}</TableCell>
                <TableCell align='center'>{data.total_grami}</TableCell>
                <TableCell align='center'>{data.total_gram}</TableCell>
                <TableCell align='center'>{data.sum}</TableCell>
                <TableCell align='center'>-</TableCell>
                <TableCell align='center'>-</TableCell>
                <TableCell align='center'>-</TableCell>
                </TableRow>   
                ):null
                }
                
                <TableRow>
                <TableCell align='center'><b>Balance</b></TableCell>
                <TableCell align='center'><strong>Silver</strong></TableCell>
                <TableCell align='center'><b>{(this.getsum())}(Gm)</b></TableCell>
                <TableCell align='center'><b>{numberFormat(this.getsuminrate())}</b></TableCell>
                <TableCell align='center'></TableCell>
                <TableCell align='center'><strong>Gold</strong></TableCell>
                <TableCell align='center'><b>{(this.getsumG())}(Gm)</b></TableCell>
                <TableCell align='center'><b>{numberFormat(this.getsuminrateG())}</b></TableCell>
                </TableRow>  
        </TableBody>
    </Table>
</TableContainer>
</Grid>:''
}
<BlockUI blocking={this.state.blocking} />
</div>
)
}
gettotal() {
let grandtotal = 0;
const rowTotals = this.state.cusdata.map(
row =>
(Number(row.total)) || 0

);

if (rowTotals.length > 0) {
grandtotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandtotal;
}
getpaid() {
let grandpaid = 0;
const rowTotals = this.state.cusdata.map(
row =>
(Number(row.amt)) || 0

);

if (rowTotals.length > 0) {
grandpaid = rowTotals.reduce((acc, val) => acc + val);
}
return grandpaid;
}
getdue() {
let granddue = 0;
const rowTotals = this.state.cusdata.map(
row =>
(Number(row.due)) || 0

);

if (rowTotals.length > 0) {
granddue = rowTotals.reduce((acc, val) => acc + val);
}
return granddue;
}
getamount() {
let grantt = 0;
const rowTotals = this.state.repaydata.map(
row =>
(Number(row.amount)) || 0

);

if (rowTotals.length > 0) {
grantt = rowTotals.reduce((acc, val) => acc + val);
}
return grantt;
}    
getsum() {
let grandwt = 0;
const rowTotals = this.state.lastdata.map(
row =>
(row.sum) || 0

);

if (rowTotals.length > 0) {
grandwt = rowTotals.reduce((acc, val) => acc + val);
}
return grandwt;
}
getsuminrate() {
let grandwt = 0;
const rowTotals = this.state.lastdata.map(
row =>
(row.sum_inrate) || 0

);

if (rowTotals.length > 0) {
grandwt = rowTotals.reduce((acc, val) => acc + val);
}
return grandwt;
}
getsumG() {debugger
let grandwt = 0;
const rowTotals = this.state.lastdataG.map(
row =>
(row.sum) || 0

);

if (rowTotals.length > 0) {
grandwt = rowTotals.reduce((acc, val) => acc + val);
}
return grandwt;
}
getsuminrateG() {
let grandwt = 0;
const rowTotals = this.state.lastdataG.map(
row =>
(row.sum_inrate) || 0

);

if (rowTotals.length > 0) {
grandwt = rowTotals.reduce((acc, val) => acc + val);
}
return grandwt;
}
}


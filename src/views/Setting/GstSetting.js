import React from 'react';
import { Grid } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

class GstSetting extends React.Component {
    constructor() {
        super()
        var today = new Date(),
        date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() ;
        this.state = { date:date, igst:'', sgst:'', cgst:'',gst:'',gstseen: false,invoiceseen: false,
        comment:'',payseen: false,paymode:'',payresult: []};
    }

getgst(){
fetch("http://ydbjewellersserver.jkgroupmanpower.in/public/api/getgst/" + 1,{
method:'GET',
})
.then((response) => response.json())
.then((result) =>{
this.setState({
igst: result.igst,
cgst: result.cgst,
sgst: result.sgst,
gst: result.sgst + result.cgst
})
})}

getterms(){
fetch("http://ydbjewellersserver.jkgroupmanpower.in/public/api/getterms/" + 1,{
method:'GET',
})
.then((response) => response.json())
.then((result) =>{
this.setState({
comment: result.comment,
})
})}

Gst = () => {
    this.setState({gstseen: !this.state.gstseen})
}

updategst = () => {
let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/gstupdate/" + 1;
let data1 = this.state;
fetch(url, {
method: 'PUT',
headers: {
"Content-Type": "application/json",
"Accept": "application/json"
},
body: JSON.stringify(data1)
}).then((result) => {
result.json().then((resp) => {
    this.componentDidMount();
})})  
}

updateterms = () => {
let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/termsupdate/" + 1;
let data1 = this.state;
fetch(url, {
method: 'PUT',
headers: {
"Content-Type": "application/json",
"Accept": "application/json"
},
body: JSON.stringify(data1)
}).then((result) => {
result.json().then((resp) => {
    this.componentDidMount();
})})  
}

Invoice = () => {
    this.setState({invoiceseen: !this.state.invoiceseen}) 
}

Payment = () => {
    this.setState({payseen: !this.state.payseen}) 
}

getpaymode = () => {
let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/paymentmode";
let data1 = this.state;
fetch(url, {
method: 'POST',
headers: {
"Content-Type": "application/json",
"Accept": "application/json"
},
body: JSON.stringify(data1)
}).then((result) => {
result.json().then((resp) => {
    this.componentDidMount();
    this.setState({paymode:''})
})}) 
}

componentDidMount(){
    this.getgst();
    this.getterms();

    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getpaymentmode').then((resp) => {
        resp.json().then((result) => {
            this.setState({ payresult: result })
        })
    })
}

render() {
return (
    <div>
        <div style={{ width: '600px', padding: '10px' }} class="no-printme">
            <Grid container spacing={1}>
            <Grid item xs={2}>
            <input type='submit' value='Gst Setting' name='gst' className='pdf' onClick={this.Gst}></input>
        </Grid>
        <Grid item xs={2}>
        <input type='submit' value='Invoice Setting' name='invoice' className='pdf' onClick={this.Invoice}></input>
            </Grid>  
            <Grid item xs={2}>
        <input type='submit' value='Payment Mode' name='payment' className='pdf' onClick={this.Payment}></input>
            </Grid>   
        </Grid>     
    </div>
        
{this.state.gstseen === true ?
<div>
<span className='gst'>IGST&nbsp;(%)&nbsp;<TextField name="igst" value={this.state.igst}  
onChange={(e) => { this.setState({ igst: e.target.value }) }}/></span>
<span className='gst'>CGST&nbsp;(%)&nbsp;<TextField name="cgst" value={this.state.cgst} 
onChange={(e) => { this.setState({ cgst: e.target.value }) }} /></span>
<span className='gst'>SGST&nbsp;(%)&nbsp;<TextField name="sgst" value={this.state.sgst}
onChange={(e) => { this.setState({ sgst: e.target.value }) }} /></span><br></br><br></br>
&nbsp;&nbsp;&nbsp;&nbsp;<Button variant="contained" onClick={() => this.updategst()} color="primary">Update</Button>&nbsp;
&nbsp;<Button variant="contained" onClick={() => this.Gst()} color="secondary">Cancel</Button>
</div>:this.state.invoiceseen === true ?
<div className="termscondition">
<strong>Terms && Conditions</strong><br></br>
<textarea rows="5" cols="70" name="comment" form="usrform" value={this.state.comment}
onChange={(e) => { this.setState({ comment: e.target.value }) }}>
Write Terms && Condition here...</textarea><br></br>
<Button variant="contained" onClick={() => this.updateterms()} color="primary">Update</Button>&nbsp;
&nbsp;<Button variant="contained" onClick={() => this.Invoice()} color="secondary">Cancel</Button>
</div>:this.state.payseen === true ?

<div><TextField name="paymode" id="standard-basic" label='Payment Mode' value={this.state.paymode}
onChange={(e) => { this.setState({ paymode: e.target.value }) }}/>&nbsp;&nbsp;&nbsp;&nbsp;
<Button onClick={() => this.getpaymode()} color="primary" style={{marginBottom:'-34px'}}>Add</Button><br></br><br></br>

<table className="table">
    <tr className="tr">
        <th className="th">#</th>
        <th className="th">Pament Mode</th>
        <th className="th">Action</th>
    </tr>
    {this.state.payresult ?
     this.state.payresult.map((data,i) =>
     <tr className="tr">
        <td className="td">{i + 1}</td>
        <td className="td">{data.paymode}</td>
        <td className="td"></td>
    </tr> ):''
    }
</table></div>:''
}
</div>)
}
}
export default GstSetting;

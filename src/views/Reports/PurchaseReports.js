import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import PurchaseEdit from "./EditPurchase.js";
import Select from 'react-select'
import { Grid } from '@material-ui/core';
import axios from 'axios';
import { ThemeProvider } from 'react-bootstrap';
import BlockUI from "../../BlockUI/BlockUI";

class PurchaseReports extends React.Component {
    constructor() {
        super()
        this.state = {
            saledata: '', visibleedit: '', visibleeditgst:'', show: '', saledatagst: '', searchdata: '', searchdatagst: '',
            startdate:'', enddate:'', daterange:'', daterangegst:'', cname:'', paidamt:'', vat:'',c_id:'',remark:'',
            address:'',state:'',cdate:'',commitdate:'',paytype:'',due:'',total:'',purity:'',waste:'', s_id:'',
            remark:'', citys:'', supdata:'',selectOptions:'',totaldata:'',totaldata1:'',totaldataw:'',totaldata1w:'',
            totaldatawgst:'',totaldata1wgst:'',invdata:'',supdata:'',totalsum:'',totalsumgold:'',totaldatainv:'',
            igst:'',cgst:'',sgst:'',invdatasilver: [],invdatagold: [],purdatasilver: [],purdatagold: [],cgstno:''
        };
    }
    state = {
        blocking: false,
        user: {}
      };
    async getdata() {
        this.setState({blocking:true})
        const res = await axios.get('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getsup')
        const data = res.data
        
        const options = data.map(d => ({
        "value": d.id,
        "label": d.supplier_name,
        "remark": d.contact_no,
        "address": d.address,
        "citys": d.c_state,
        "cgstno":d.gstno }))
        this.setState({ selectOptions: options,blocking:false })
        }
    handleChange(e) {
    this.setState({blocking:true, s_id: e.value, name: e.label, remark: e.remark,address: e.address,citys: e.citys,
    daterange: '', daterangegst: '', searchdatagst: '', searchdata: '',saledata:'',saledatagst:'',cgstno:e.cgstno})
    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getpurchasebal/' + e.value).then((resp) => {
                resp.json().then((result) => {
                    this.setState({ supdata: result })
                    this.setState({
                        totalsum: [...this.state.supdata, ...this.state.invdata]
                      })
                })
            })
    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getinvbal/' + e.value).then((resp) => {
        resp.json().then((result) => {
            this.setState({ invdata: result })
            this.setState({
                totalsum: [...this.state.supdata, ...this.state.invdata]
              })
        })
    })
    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getinvsilver/' + e.value).then((resp) => {
        resp.json().then((result) => {
            this.setState({ invdatasilver: result })
        })
    })
    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getinvgold/' + e.value).then((resp) => {
        resp.json().then((result) => {
            this.setState({ invdatagold: result })
        })
    })
    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getpursilver/' + e.value).then((resp) => {
        resp.json().then((result) => {
            this.setState({ purdatasilver: result })
        })
    })
    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getpurgold/' + e.value).then((resp) => {
        resp.json().then((result) => {
            this.setState({ purdatagold: result,blocking:false })
        })
    })
    }

    SubmitGst = (data) => {
    this.setState({ visibleeditgst:'',blocking:true })
    let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/purchase_billgstupdate/" + data.billno;
    let data1 = data;
    fetch(url, {
    method: 'PUT',
    headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
    },
    body: JSON.stringify(data1)
    }).then((result) => {
    result.json().then((resp) => {
        this.setState({blocking:false})
    this.componentDidMount();
    })
    })
    }

    Submit = (data) => {
    this.setState({ visibleedit:'',blocking:true })
    let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/purchaseupdate/" + data.billno;
    let data1 = data;
    fetch(url, {
    method: 'PUT',
    headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
    },
    body: JSON.stringify(data1)
    }).then((result) => {
    result.json().then((resp) => {
        this.setState({blocking:false})
    this.componentDidMount();
    })
    })
    }

    togglegst = (e) => {
        this.setState({
            show: e.target.value,
            daterange: '',
            daterangegst: '',
            searchdatagst: '',
            searchdata: '',
            visibleedit:'',
            s_id: ''
        })
        this.componentDidMount()
    }
    search = (key) => {
        if (this.state.show === 'yes') {
            fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getgstsearchp/' + key).then((resp) => {
                resp.json().then((result) => {
                    this.setState({ searchdatagst: result })
                })
            })
        } else {
            fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getbillsearchp/' + key).then((resp) => {
                resp.json().then((result) => {
                    this.setState({ searchdata: result })
                })
            })
        }
    };
    getDate =() => {
        if(this.state.show === 'yes') {
            fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/daterangegstp/' + this.state.startdate + '/' + this.state.enddate).then((resp) => {
                resp.json().then((result) => {
                    this.setState({ daterangegst: result })
                })
            })
        } else {
            fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/daterangep/' + this.state.startdate + '/' + this.state.enddate).then((resp) => {
                resp.json().then((result) => {
                    this.setState({ daterange: result })
                })
            })
        }
    }
    PrintPdf(){
        window.print()
    }
    
    componentDidMount() {
        
        fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getpurchasegst').then((resp) => {
            resp.json().then((result) => {
                this.setState({ saledatagst: result })
            })
        })
        fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getpurchase').then((resp) => {
            resp.json().then((result) => {
                this.setState({ saledata: result })
            })
        })
        fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/gettotalwithoutgold').then((resp) => {
            resp.json().then((result) => {
                this.setState({ totaldataw: result })
            })
        })
        fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/gettotalwithoutsilver').then((resp) => {
            resp.json().then((result) => {
                this.setState({ totaldata1w: result })
            })
        })
        fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/gettotalgstgold').then((resp) => {
            resp.json().then((result) => {
                this.setState({ totaldatawgst: result })
            })
        })
        fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/gettotalgstsilver').then((resp) => {
            resp.json().then((result) => {
                this.setState({ totaldata1wgst: result })
            })
        })
        this.getdata();
        //this.getYourData();
        // setInterval(this.getYourData.bind(this), 5000); 
    }
    
    edit = (data) => {
        this.setState({visibleedit: data.id,cname: data.name,paidamt: data.amt,vat: data.vat,c_id: data.c_id,remark: data.remark,
            address: data.address,state: data.state,cdate: data.cdate,commitdate: data.commitdate,paytype: data.paytype,due: data.due,
            total: data.total,purity:data.purity,waste:data.waste,saledata:'',searchdata:'',daterange:'',
            cgstno: data.cgstno,
        })
        console.log(data)
    }
    editgst = (data) => {
        this.setState({visibleeditgst: data.id,cname: data.name,paidamt: data.amt,vat: data.vat,c_id: data.c_id,remark: data.remark,
            address: data.address,state: data.state,cdate: data.cdate,commitdate: data.commitdate,paytype: data.paytype,due: data.due,
            total: data.total,purity:data.purity,waste:data.waste,saledatagst:'',searchdatagst:'',daterangegst:'',
            igst: data.igst,cgst: data.cgst,sgst: data.sgst,cgstno:data.cgstno
        })
    }

    Cancel = () => {
        this.setState({
            visibleedit: '',
        })
        this.componentDidMount()
    }
    CancelGst = () => {
        this.setState({
            visibleeditgst: '',
        })
        this.componentDidMount()
    }

    render() {
        const tableCell = {padding:'10px'}
        return (
            <div>{this.state.visibleedit === '' && this.state.visibleeditgst === '' ?
                <div style={{ width: '1200px', padding: '5px' }} class="no-printme">
                    <Grid container spacing={1}>
                    <Grid item xs={8}>
                    <select className='textfield' value={this.state.show} onChange={this.togglegst}>
                        <option value="yes">With GST</option>
                        <option value="">Without GST</option>
                    </select>&nbsp;
                <input type='text' className='textfield1' placeholder='Search supplier...' onChange={(event) => this.search(event.target.value)} />
                &nbsp;<input type='date' className='textfield1' name='startdate' value={this.state.startdate} 
                onChange={(e)=>this.setState({startdate: e.target.value})}></input>
                &nbsp;<input type='date' className='textfield1' name='enddate' value={this.state.enddate} 
                onChange={(e)=>this.setState({enddate: e.target.value})}></input>
                &nbsp;<input type='submit' value='Get' name='get' className='btn' onClick={this.getDate}></input>
                &nbsp;<input type='submit' value='Print && Pdf' name='pdf' className='pdf' onClick={this.PrintPdf}></input>
                </Grid>
                </Grid> 
            </div>:''}
                {this.state.show === '' && this.state.visibleedit === '' && this.state.searchdata === '' && 
                this.state.daterange === '' && this.state.s_id === '' ?
                    <TableContainer component={Paper}>
                        <Table aria-label="spanning table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Bill No.</TableCell>
                                    <TableCell>Supplier Name</TableCell>
                                    <TableCell>Date</TableCell>
                                    <TableCell align='center'>Item Details</TableCell>
                                    {/* <TableCell>Due</TableCell>
                                    <TableCell>Paid Amt.</TableCell>
                                    <TableCell>Sub Total</TableCell> */}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                    {
                                    this.state.saledata ?
                                    this.state.saledata.map((data,i) =>
                                    <TableRow>
                                    <TableCell>{data.id}</TableCell>
                                    <TableCell>
                                    <b><Button onClick={() => this.edit(data)}>{data.name}</Button></b><br></br>
                                    <span>&nbsp;&nbsp;{data.address}</span><br></br>
                                    <span>&nbsp;&nbsp;{data.state}</span><br></br>
                                    <span>&nbsp;&nbsp;{data.remark}</span>
                                    </TableCell>
                                    <TableCell>{data.date}</TableCell>
                                    <TableRow>
                                    <TableCell style={tableCell}>Itemname</TableCell>
                                    <TableCell style={tableCell}>Qt.</TableCell>
                                    <TableCell style={tableCell}>Wt.</TableCell>
                                    <TableCell style={tableCell}>Purity</TableCell>
                                    <TableCell style={tableCell}>Waste</TableCell>
                                    <TableCell style={tableCell}>Rate</TableCell>
                                    <TableCell style={tableCell}>Making cha.</TableCell>
                                    <TableCell style={tableCell}>Total</TableCell>
                                    </TableRow>
                                    {data.details.map((det) =>
                                    <TableRow>
                                    <TableCell style={tableCell}>{det.itemname}</TableCell>
                                    <TableCell style={tableCell}>{det.qty}</TableCell>
                                    <TableCell style={tableCell}>{det.wt}&nbsp;Gm</TableCell>
                                    <TableCell style={tableCell}>{det.purity}&nbsp;%</TableCell>
                                    <TableCell style={tableCell}>{det.waste}&nbsp;%</TableCell>
                                    <TableCell style={tableCell}>{det.rate}</TableCell>
                                    <TableCell style={tableCell}>{
                                    det.unit === '1' ? <span>{Number((det.makingcha)).toFixed(2)}</span>
                                    : det.unit === '2' ? <span>{((det.wt || 0) * (det.makingcha || 0)).toFixed(2)}</span>
                                    : det.unit === '3' ? <span>{((det.makingcha || 0) * (det.qty || 0)).toFixed(2)}</span>
                                    : det.unit === '4' ? <span>{Number((det.makingcha || 0)).toFixed(2)}</span> : ''}
                                    </TableCell>
                                    <TableCell style={tableCell}>{
                                    det.rate === '0' ?
                                    <span>{(((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
                                    :det.unit === '1' ? <span>{(((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                                    +Number(det.makingcha)).toFixed(2)}</span>
                                    :det.unit === '2' ? <span>{(((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
                                    +(det.wt * det.makingcha)).toFixed(2)}</span>:
                                    det.unit === '3' ? <span>{(((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
                                    +(det.qty * det.makingcha)).toFixed(2)}</span>:
                                    det.unit === '4' ? <span>{(((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                                    +Number(det.makingcha)).toFixed(2)}</span>:''
                                    }</TableCell>
                                    </TableRow>
                                    )}
                                    {/* <TableCell style={tableCell}>{data.due}</TableCell>
                                    <TableCell style={tableCell}>{data.amt}</TableCell>
                                    <TableCell style={tableCell}>{data.total}</TableCell> */}
                                    </TableRow>   
                                    ):null
                                    }
                            </TableBody>
                            {
            this.state.totaldataw ?
            this.state.totaldataw.map((data,i) =>
             <TableRow>
                 <TableCell><b>Subtotal</b></TableCell>
                 <TableCell><b>Gold</b></TableCell>
                 <TableCell></TableCell>
                 <TableCell><b>Total Wt.(Gm):</b>&nbsp;{data.totalwt}&nbsp;&nbsp;&nbsp;&nbsp;<b>Final Total(Gm)</b>&nbsp;{data.totalgram}
                 &nbsp;&nbsp;&nbsp;&nbsp;<b>Total Amt.</b>&nbsp;{data.totalrate}</TableCell>
             </TableRow>
             ):null
            }
            {
            this.state.totaldata1w ?
            this.state.totaldata1w.map((data,i) =>
             <TableRow>
                 <TableCell></TableCell>
                 <TableCell><b>Silver</b></TableCell>
                 <TableCell></TableCell>
                 <TableCell><b>Total Wt.(Gm):</b>&nbsp;{data.totalwt}&nbsp;&nbsp;&nbsp;&nbsp;<b>Final Total(Gm)</b>&nbsp;{data.totalgram}
                 &nbsp;&nbsp;&nbsp;&nbsp;<b>Total Amt.</b>&nbsp;{data.totalrate}</TableCell>
             </TableRow>
             ):null
            }
                        </Table>
                    </TableContainer>:this.state.show === '' && this.state.visibleedit ?
                    <div><PurchaseEdit data={{editid:this.state.visibleedit,Submit:this.Submit.bind(this),
                    Cancel:this.Cancel.bind(this),purity:this.state.purity,waste:this.state.waste,
                    customername:this.state.cname,amount:this.state.paidamt,vat:this.state.vat,c_id:this.state.c_id,
                    remark:this.state.remark,address:this.state.address,state:this.state.state,cdate:this.state.cdate,
                    commitdate:this.state.commitdate,paytype:this.state.paytype,due:this.state.due,total:this.state.total,
                    cgstno:this.state.cgstno}}/>
                    </div>:''}
                    {this.state.show === '' && this.state.searchdata ?
                     <TableContainer component={Paper}>
                     <Table aria-label="spanning table">
                         <TableHead>
                             <TableRow>
                                 <TableCell>Bill No.</TableCell>
                                 <TableCell>Supplier Name</TableCell>
                                 <TableCell>Date</TableCell>
                                 <TableCell align='center'>Item Details</TableCell>
                                 {/* <TableCell>Due</TableCell>
                                 <TableCell>Paid Amt.</TableCell>
                                 <TableCell>Sub Total</TableCell> */}
                             </TableRow>
                         </TableHead>
                         <TableBody>
                                 {
                                 this.state.searchdata ?
                                 this.state.searchdata.map((data,i) =>
                                 <TableRow>
                                 <TableCell>{data.id}</TableCell>
                                 <TableCell>
                                 <b><Button onClick={() => this.edit(data)}>{data.name}</Button></b><br></br>
                                    <span>&nbsp;&nbsp;{data.address}</span><br></br>
                                    <span>&nbsp;&nbsp;{data.state}</span><br></br>
                                    <span>&nbsp;&nbsp;{data.remark}</span>
                                 </TableCell>
                                 <TableCell>{data.date}</TableCell>
                                 <TableRow>
                                 <TableCell style={tableCell}>Itemname</TableCell>
                                 <TableCell style={tableCell}>Qt.</TableCell>
                                 <TableCell style={tableCell}>Wt.</TableCell>
                                 <TableCell style={tableCell}>Purity</TableCell>
                                 <TableCell style={tableCell}>Waste</TableCell>
                                 <TableCell style={tableCell}>Rate</TableCell>
                                 <TableCell style={tableCell}>Making cha.</TableCell>
                                 <TableCell style={tableCell}>Total</TableCell>
                                 </TableRow>
                                 {data.details.map((det) =>
                                 <TableRow>
                                 <TableCell style={tableCell}>{det.itemname}</TableCell>
                                 <TableCell style={tableCell}>{det.qty}</TableCell>
                                 <TableCell style={tableCell}>{det.wt}&nbsp;Gm</TableCell>
                                 <TableCell style={tableCell}>{det.purity}&nbsp;%</TableCell>
                                 <TableCell style={tableCell}>{det.waste}&nbsp;%</TableCell>
                                 <TableCell style={tableCell}>{det.rate}</TableCell>
                                 <TableCell style={tableCell}>{
                                 det.unit === '1' ? <span>{Number((det.makingcha)).toFixed(2)}</span>
                                 : det.unit === '2' ? <span>{((det.wt || 0) * (det.makingcha || 0)).toFixed(2)}</span>
                                 : det.unit === '3' ? <span>{((det.makingcha || 0) * (det.qty || 0)).toFixed(2)}</span>
                                 : det.unit === '4' ? <span>{Number((det.makingcha || 0)).toFixed(2)}</span> : ''}
                                 </TableCell>
                                 <TableCell style={tableCell}>{
                                 det.rate === '0' ?
                                 <span>{(((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
                                 :det.unit === '1' ? <span>{(((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                                 +Number(det.makingcha)).toFixed(2)}</span>
                                 :det.unit === '2' ? <span>{(((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
                                 +(det.wt * det.makingcha)).toFixed(2)}</span>:
                                 det.unit === '3' ? <span>{(((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
                                 +(det.qty * det.makingcha)).toFixed(2)}</span>:
                                 det.unit === '4' ? <span>{(((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                                 +Number(det.makingcha)).toFixed(2)}</span>:''
                                 }</TableCell>
                                 </TableRow>
                                 )}
                                 {/* <TableCell style={tableCell}>{data.due}</TableCell>
                                 <TableCell style={tableCell}>{data.amt}</TableCell>
                                 <TableCell style={tableCell}>{data.total}</TableCell> */}
                                 </TableRow>   
                                 ):null
                                 }
                         </TableBody>
                     </Table>
                 </TableContainer>
                    :this.state.show === '' && this.state.daterange ?
                    <TableContainer component={Paper}>
                        <Table aria-label="spanning table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Bill No.</TableCell>
                                    <TableCell>Supplier Name</TableCell>
                                    <TableCell>Date</TableCell>
                                    <TableCell align='center'>Item Details</TableCell>
                                    {/* <TableCell>Due. Amt.</TableCell>
                                    <TableCell>Paid Amt.</TableCell>
                                    <TableCell>Sub Total</TableCell> */}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                    {
                                    this.state.daterange ?
                                    this.state.daterange.map((data,i) =>
                                    <TableRow>
                                 <TableCell>{data.id}</TableCell>
                                 <TableCell>
                                 <b><Button onClick={() => this.edit(data)}>{data.name}</Button></b><br></br>
                                    <span>&nbsp;&nbsp;{data.address}</span><br></br>
                                    <span>&nbsp;&nbsp;{data.state}</span><br></br>
                                    <span>&nbsp;&nbsp;{data.remark}</span>
                                 </TableCell>
                                 <TableCell>{data.date}</TableCell>
                                 <TableRow>
                                 <TableCell style={tableCell}>Itemname</TableCell>
                                 <TableCell style={tableCell}>Qt.</TableCell>
                                 <TableCell style={tableCell}>Wt.</TableCell>
                                 <TableCell style={tableCell}>Purity</TableCell>
                                 <TableCell style={tableCell}>Waste</TableCell>
                                 <TableCell style={tableCell}>Rate</TableCell>
                                 <TableCell style={tableCell}>Making cha.</TableCell>
                                 <TableCell style={tableCell}>Total</TableCell>
                                 </TableRow>
                                 {data.details.map((det) =>
                                 <TableRow>
                                 <TableCell style={tableCell}>{det.itemname}</TableCell>
                                 <TableCell style={tableCell}>{det.qty}</TableCell>
                                 <TableCell style={tableCell}>{det.wt}&nbsp;Gm</TableCell>
                                 <TableCell style={tableCell}>{det.purity}&nbsp;%</TableCell>
                                 <TableCell style={tableCell}>{det.waste}&nbsp;%</TableCell>
                                 <TableCell style={tableCell}>{det.rate}</TableCell>
                                 <TableCell style={tableCell}>{
                                 det.unit === '1' ? <span>{Number((det.makingcha)).toFixed(2)}</span>
                                 : det.unit === '2' ? <span>{((det.wt || 0) * (det.makingcha || 0)).toFixed(2)}</span>
                                 : det.unit === '3' ? <span>{((det.makingcha || 0) * (det.qty || 0)).toFixed(2)}</span>
                                 : det.unit === '4' ? <span>{Number((det.makingcha || 0)).toFixed(2)}</span> : ''}
                                 </TableCell>
                                 <TableCell style={tableCell}>{
                                 det.rate === '0' ?
                                 <span>{(((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
                                 :det.unit === '1' ? <span>{(((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                                 +Number(det.makingcha)).toFixed(2)}</span>
                                 :det.unit === '2' ? <span>{(((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
                                 +(det.wt * det.makingcha)).toFixed(2)}</span>:
                                 det.unit === '3' ? <span>{(((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
                                 +(det.qty * det.makingcha)).toFixed(2)}</span>:
                                 det.unit === '4' ? <span>{(((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                                 +Number(det.makingcha)).toFixed(2)}</span>:''
                                 }</TableCell>
                                 </TableRow>
                                    )}
                                    {/* <TableCell>{data.due}</TableCell>
                                    <TableCell>{data.amt}</TableCell>
                                    <TableCell>{data.total}</TableCell> */}
                                    </TableRow>   
                                    ):null
                                    }
                            </TableBody>
                        </Table>
                    </TableContainer>:''}
                    {this.state.show === 'yes' && this.state.saledatagst && this.state.searchdatagst === '' && this.state.daterangegst === '' ?
                    <TableContainer component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Bill No.</TableCell>
                                <TableCell>Supplier Name</TableCell>
                                <TableCell>Date</TableCell>
                                <TableCell align='center'>Item Details</TableCell>
                                {/* <TableCell>Due. Amt.</TableCell>
                                <TableCell>Paid Amt.</TableCell>
                                <TableCell>Sub Total</TableCell> */}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                                {
                                this.state.saledatagst ?
                                this.state.saledatagst.map((data,i) =>
                                <TableRow>
                                <TableCell style={tableCell}>{data.id}</TableCell>
                                <TableCell style={tableCell}>
                                <b><Button onClick={() => this.editgst(data)}>{data.name}</Button></b><br></br>
                                    <span>&nbsp;&nbsp;{data.address}</span><br></br>
                                    <span>&nbsp;&nbsp;{data.state}</span><br></br>
                                    <span>&nbsp;&nbsp;{data.remark}</span>
                                </TableCell>
                                <TableCell style={tableCell}>{data.date}</TableCell>
                                <TableRow>
                                <TableCell style={tableCell}>Itemname</TableCell>
                                <TableCell style={tableCell}>Qt.</TableCell>
                                <TableCell style={tableCell}>Wt.</TableCell>
                                <TableCell style={tableCell}>Purity</TableCell>
                                <TableCell style={tableCell}>Waste</TableCell>
                                <TableCell style={tableCell}>Rate</TableCell>
                                <TableCell style={tableCell}>Making cha.</TableCell>
                                {data.state === 'MADHYA PRADESH' ?
                                <TableCell style={tableCell}>Sgst<br></br>Cgst</TableCell>   
                                :<TableCell style={tableCell}>Igst</TableCell>}
                                <TableCell style={tableCell}>Total</TableCell>
                                </TableRow>
                                {data.details.map((det) =>
                                <TableRow>
                                <TableCell style={tableCell}>{det.itemname}</TableCell>
                                <TableCell style={tableCell}>{det.qty}</TableCell>
                                <TableCell style={tableCell}>{det.wt}</TableCell>
                                <TableCell style={tableCell}>{det.purity}&nbsp;%</TableCell>
                                <TableCell style={tableCell}>{det.waste}&nbsp;%</TableCell>
                                <TableCell style={tableCell}>{det.rate}</TableCell>
                                <TableCell style={tableCell}>{
                                det.unit === '1' ? <span>{Number((det.makingcha)).toFixed(2)}</span>
                                : det.unit === '2'  ? <span>{((det.wt || 0) * (det.makingcha || 0)).toFixed(2)}</span>
                                : det.unit === '3' ? <span>{((det.makingcha || 0) * (det.qty || 0)).toFixed(2)}</span>
                                : det.unit === '4' ? <span>{Number((det.makingcha || 0)).toFixed(2)}</span> : ''}
                                </TableCell>
                                {data.state === 'MADHYA PRADESH' ?
                                <TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{data.sgst}</span>&nbsp;
                                {
                                det.unit === '1'  ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                                +Number(det.makingcha)) * Number(data.sgst) / 100).toFixed(2)}</span>
                                :det.unit === '2' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
                                +(det.wt * det.makingcha)) * Number(data.sgst) / 100).toFixed(2)}</span>:
                                det.unit === '3' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
                                +(det.qty * det.makingcha)) * Number(data.sgst) / 100).toFixed(2)}</span>:
                                det.unit === '4' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                                +Number(det.makingcha)) * Number(data.sgst) / 100).toFixed(2)}</span>:''
                                }
                                <br></br>
                                <span style={{ backgroundColor: "red" }}>{data.cgst}</span>&nbsp;
                                {
                                det.unit === '1' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                                +Number(det.makingcha)) * Number(data.cgst) / 100).toFixed(2)}</span>
                                :det.unit === '2' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
                                +(det.wt * det.makingcha)) * Number(data.cgst) / 100).toFixed(2)}</span>:
                                det.unit === '3' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
                                +(det.qty * det.makingcha)) * Number(data.cgst) / 100).toFixed(2)}</span>:
                                det.unit === '4' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                                +Number(det.makingcha)) * Number(data.cgst) / 100).toFixed(2)}</span>:''
                                }
                                </TableCell>   
                                :<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{data.igst}</span>&nbsp;
                                {
                                det.unit === '1' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                                +Number(det.makingcha)) * Number(data.igst) / 100).toFixed(2)}</span>
                                :det.unit === '2' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
                                +(det.wt * det.makingcha)) * Number(data.igst) / 100).toFixed(2)}</span>:
                                det.unit === '3' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
                                +(det.qty * det.makingcha)) * Number(data.igst) / 100).toFixed(2)}</span>:
                                det.unit === '4' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                                +Number(det.makingcha)) * Number(data.igst) / 100).toFixed(2)}</span>:''
                                }
                                </TableCell>}
{data.state === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{
det.rate === '0' ?
<span>{(((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:det.unit === '1' ? <span>{(((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
+Number(det.makingcha)) * (Number(data.sgst) + Number(data.cgst)) / 100) + Number(((((det.wt * det.purity) / 100) 
+ Number((det.wt * det.waste) / 100)) * (det.rate)) + Number(det.makingcha))).toFixed(2)}</span>
:det.unit === '2' ? <span>{(((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
+(det.wt * det.makingcha)) * (Number(data.sgst) + Number(data.cgst)) / 100) + (((((det.wt * det.purity) / 100) 
+ ((det.wt * det.waste) / 100)) *(det.rate)) + (det.wt * det.makingcha))).toFixed(2)}</span>:
det.unit === '3' ? <span>{(((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
+(det.qty * det.makingcha))* (Number(data.sgst) + Number(data.cgst)) /100) + (((((det.wt * det.purity) / 100) 
+ ((det.wt * det.waste) / 100))* (det.rate)) + (det.qty * det.makingcha))).toFixed(2)}</span>:
det.unit === '4' ? <span>{(((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
+Number(det.makingcha))* (Number(data.sgst) + Number(data.cgst)) / 100) + Number(((((det.wt * det.purity) / 100) 
+ Number((det.wt * det.waste) / 100)) * (det.rate)) + Number(det.makingcha))).toFixed(2)}</span>:''
}
</TableCell>:
<TableCell style={tableCell}>{
det.rate === '0' ?
<span>{(((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:det.unit === '1' ? <span>{(((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
+Number(det.makingcha)) * Number(data.igst) / 100) + Number(((((det.wt * det.purity) / 100) 
+ Number((det.wt * det.waste) / 100)) * (det.rate)) + Number(det.makingcha))).toFixed(2)}</span>
:det.unit === '2' ? <span>{(((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
+(det.wt * det.makingcha)) * Number(data.igst) / 100)+(((((det.wt * det.purity) / 100) 
+ ((det.wt * det.waste) / 100)) *(det.rate)) + (det.wt * det.makingcha))).toFixed(2)}</span>:
det.unit === '3' ? <span>{(((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
+(det.qty * det.makingcha))* Number(data.igst) /100) + (((((det.wt * det.purity) / 100) 
+ ((det.wt * det.waste) / 100))* (det.rate)) + (det.qty * det.makingcha))).toFixed(2)}</span>:
det.unit === '4' ? <span>{(((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
+Number(det.makingcha))* Number(data.igst) / 100) + Number(((((det.wt * det.purity) / 100) 
+ Number((det.wt * det.waste) / 100)) * (det.rate)) + Number(det.makingcha))).toFixed(2)}</span>:''
}
</TableCell>}
                </TableRow>
                )}
                {/* <TableCell style={tableCell}>{data.due}</TableCell>
                <TableCell style={tableCell}>{data.amt}</TableCell>
                <TableCell style={tableCell}>{data.total}</TableCell> */}
                </TableRow>   
                ):null
                }
        </TableBody>
        {
            this.state.totaldatawgst ?
            this.state.totaldatawgst.map((data,i) =>
             <TableRow>
                 <TableCell><b>Subtotal</b></TableCell>
                 <TableCell><b>Gold</b></TableCell>
                 <TableCell></TableCell>
                 <TableCell><b>Total Wt.(Gm):</b>&nbsp;{(data.totalwt)}&nbsp;&nbsp;&nbsp;&nbsp;<b>Final Total(Gm)</b>&nbsp;{(data.totalgram)}
                 &nbsp;&nbsp;&nbsp;&nbsp;<b>Total Amt.</b>&nbsp;{(data.totalrate)}</TableCell>
             </TableRow>
             ):null
            }
            {
            this.state.totaldata1wgst ?
            this.state.totaldata1wgst.map((data,i) =>
             <TableRow>
                 <TableCell></TableCell>
                 <TableCell><b>Silver</b></TableCell>
                 <TableCell></TableCell>
                 <TableCell><b>Total Wt.(Gm):</b>&nbsp;{(data.totalwt)}&nbsp;&nbsp;&nbsp;&nbsp;<b>Final Total(Gm)</b>&nbsp;{(data.totalgram)}
                 &nbsp;&nbsp;&nbsp;&nbsp;<b>Total Amt.</b>&nbsp;{(data.totalrate)}</TableCell>
             </TableRow>
             ):null
            }
                    </Table>
                </TableContainer>:''}
                {this.state.show === 'yes' && this.state.visibleeditgst ?
                <div><PurchaseEdit data={{editid:this.state.visibleeditgst,SubmitGst:this.SubmitGst.bind(this),
                CancelGst:this.CancelGst.bind(this),
                customername:this.state.cname,amount:this.state.paidamt,vat:this.state.vat,c_id:this.state.c_id,
                remark:this.state.remark,address:this.state.address,state:this.state.state,cdate:this.state.cdate,
                commitdate:this.state.commitdate,paytype:this.state.paytype,due:this.state.due,total:this.state.total,
                igst:this.state.igst,cgst:this.state.cgst,sgst:this.state.sgst,cgstno:this.state.cgstno}}/>
                </div>:''}
                {this.state.show === 'yes' && this.state.searchdatagst ?
                <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Bill No.</TableCell>
                            <TableCell>Supplier Name</TableCell>
                            <TableCell>Date</TableCell>
                            <TableCell align='center'>Item Details</TableCell>
                            {/* <TableCell>Due. Amt.</TableCell>
                            <TableCell>Paid Amt.</TableCell>
                            <TableCell>Sub Total</TableCell> */}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                            {
                            this.state.searchdatagst ?
                            this.state.searchdatagst.map((data,i) =>
                            <TableRow>
                            <TableCell style={tableCell}>{data.id}</TableCell>
                            <TableCell style={tableCell}>
                            <b><Button onClick={() => this.editgst(data)}>{data.name}</Button></b><br></br>
                                    <span>&nbsp;&nbsp;{data.address}</span><br></br>
                                    <span>&nbsp;&nbsp;{data.state}</span><br></br>
                                    <span>&nbsp;&nbsp;{data.remark}</span>
                            </TableCell>
                            <TableCell style={tableCell}>{data.date}</TableCell>
                            <TableRow>
                            <TableCell style={tableCell}>Itemname</TableCell>
                            <TableCell style={tableCell}>Qt.</TableCell>
                            <TableCell style={tableCell}>Wt.</TableCell>
                            <TableCell style={tableCell}>Purity</TableCell>
                            <TableCell style={tableCell}>Waste</TableCell>
                            <TableCell style={tableCell}>Rate</TableCell>
                            <TableCell style={tableCell}>Making cha.</TableCell>
                            {data.state === 'MADHYA PRADESH' ?
                            <TableCell style={tableCell}>Sgst<br></br>Cgst</TableCell>   
                            :<TableCell style={tableCell}>Igst</TableCell>}
                            <TableCell style={tableCell}>Total</TableCell>
                            </TableRow>
                            {data.details.map((det) =>
                            <TableRow>
                            <TableCell style={tableCell}>{det.itemname}</TableCell>
                            <TableCell style={tableCell}>{det.qty}</TableCell>
                            <TableCell style={tableCell}>{det.wt}</TableCell>
                            <TableCell style={tableCell}>{det.purity}&nbsp;%</TableCell>
                            <TableCell style={tableCell}>{det.waste}&nbsp;%</TableCell>
                            <TableCell style={tableCell}>{det.rate}</TableCell>
                            <TableCell style={tableCell}>{
                            det.unit === '1' ? <span>{Number((det.makingcha)).toFixed(2)}</span>
                            : det.unit === '2'  ? <span>{((det.wt || 0) * (det.makingcha || 0)).toFixed(2)}</span>
                            : det.unit === '3' ? <span>{((det.makingcha || 0) * (det.qty || 0)).toFixed(2)}</span>
                            : det.unit === '4' ? <span>{Number((det.makingcha || 0)).toFixed(2)}</span> : ''}
                            </TableCell>
                            {data.state === 'MADHYA PRADESH' ?
                            <TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{data.sgst}</span>&nbsp;
                            {
                            det.unit === '1'  ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                            +Number(det.makingcha)) * Number(data.sgst) / 100).toFixed(2)}</span>
                            :det.unit === '2' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
                            +(det.wt * det.makingcha)) * Number(data.sgst) / 100).toFixed(2)}</span>:
                            det.unit === '3' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
                            +(det.qty * det.makingcha)) * Number(data.sgst) / 100).toFixed(2)}</span>:
                            det.unit === '4' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                            +Number(det.makingcha)) * Number(data.sgst) / 100).toFixed(2)}</span>:''
                            }
                            <br></br>
                            <span style={{ backgroundColor: "red" }}>{data.cgst}</span>&nbsp;
                            {
                            det.unit === '1' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                            +Number(det.makingcha)) * Number(data.cgst) / 100).toFixed(2)}</span>
                            :det.unit === '2' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
                            +(det.wt * det.makingcha)) * Number(data.cgst) / 100).toFixed(2)}</span>:
                            det.unit === '3' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
                            +(det.qty * det.makingcha)) * Number(data.cgst) / 100).toFixed(2)}</span>:
                            det.unit === '4' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                            +Number(det.makingcha)) * Number(data.cgst) / 100).toFixed(2)}</span>:''
                            }
                            </TableCell>   
                            :<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{data.igst}</span>&nbsp;
                            {
                            det.unit === '1' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                            +Number(det.makingcha)) * Number(data.igst) / 100).toFixed(2)}</span>
                            :det.unit === '2' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
                            +(det.wt * det.makingcha)) * Number(data.igst) / 100).toFixed(2)}</span>:
                            det.unit === '3' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
                            +(det.qty * det.makingcha)) * Number(data.igst) / 100).toFixed(2)}</span>:
                            det.unit === '4' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                            +Number(det.makingcha)) * Number(data.igst) / 100).toFixed(2)}</span>:''
                            }
                            </TableCell>}
{data.state === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{
det.rate === '0' ?
<span>{(((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:det.unit === '1' ? <span>{(((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
+Number(det.makingcha)) * (Number(data.sgst) + Number(data.cgst)) / 100) + Number(((((det.wt * det.purity) / 100) 
+ Number((det.wt * det.waste) / 100)) * (det.rate)) + Number(det.makingcha))).toFixed(2)}</span>
:det.unit === '2' ? <span>{(((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
+(det.wt * det.makingcha)) * (Number(data.sgst) + Number(data.cgst)) / 100)+(((((det.wt * det.purity) / 100) 
+ ((det.wt * det.waste) / 100)) *(det.rate)) + (det.wt * det.makingcha))).toFixed(2)}</span>:
det.unit === '3' ? <span>{(((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
+(det.qty * det.makingcha))* (Number(data.sgst) + Number(data.cgst)) /100) + (((((det.wt * det.purity) / 100) 
+ ((det.wt * det.waste) / 100))* (det.rate)) + (det.qty * det.makingcha))).toFixed(2)}</span>:
det.unit === '4' ? <span>{(((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
+Number(det.makingcha))* (Number(data.sgst) + Number(data.cgst)) / 100) + Number(((((det.wt * det.purity) / 100) 
+ Number((det.wt * det.waste) / 100)) * (det.rate)) + Number(det.makingcha))).toFixed(2)}</span>:''
}
</TableCell>:
<TableCell style={tableCell}>{
det.rate === '0' ?
<span>{(((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:det.unit === '1' ? <span>{(((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
+Number(det.makingcha)) * Number(data.igst) / 100) + Number(((((det.wt * det.purity) / 100) 
+ Number((det.wt * det.waste) / 100)) * (det.rate)) + Number(det.makingcha))).toFixed(2)}</span>
:det.unit === '2' ? <span>{(((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
+(det.wt * det.makingcha)) * Number(data.igst) / 100)+(((((det.wt * det.purity) / 100) 
+ ((det.wt * det.waste) / 100)) *(det.rate)) + (det.wt * det.makingcha))).toFixed(2)}</span>:
det.unit === '3' ? <span>{(((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
+(det.qty * det.makingcha))* Number(data.igst) /100) + (((((det.wt * det.purity) / 100) 
+ ((det.wt * det.waste) / 100))* (det.rate)) + (det.qty * det.makingcha))).toFixed(2)}</span>:
det.unit === '4' ? <span>{(((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
+Number(det.makingcha))* Number(data.igst) / 100) + Number(((((det.wt * det.purity) / 100) 
+ Number((det.wt * det.waste) / 100)) * (det.rate)) + Number(det.makingcha))).toFixed(2)}</span>:''
}
</TableCell>}
            </TableRow>
            )}
            {/* <TableCell style={tableCell}>{data.due}</TableCell>
            <TableCell style={tableCell}>{data.amt}</TableCell>
            <TableCell style={tableCell}>{data.total}</TableCell> */}
            </TableRow>   
            ):null
            }
    </TableBody>
                </Table>
            </TableContainer>
                :this.state.show === 'yes' && this.state.daterangegst ?
                <TableContainer component={Paper}>
                    <Table aria-label="spanning table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Bill No.</TableCell>
                                <TableCell>Customers Name</TableCell>
                                <TableCell>Date</TableCell>
                                <TableCell align='center'>Item Details</TableCell>
                                {/* <TableCell>Due. Amt.</TableCell>
                                <TableCell>Paid Amt.</TableCell>
                                <TableCell>Sub Total</TableCell> */}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                                {
                                this.state.daterangegst ?
                                this.state.daterangegst.map((data,i) =>
                                <TableRow>
                            <TableCell style={tableCell}>{data.id}</TableCell>
                            <TableCell style={tableCell}>
                            <b><Button onClick={() => this.editgst(data)}>{data.name}</Button></b><br></br>
                                    <span>&nbsp;&nbsp;{data.address}</span><br></br>
                                    <span>&nbsp;&nbsp;{data.state}</span><br></br>
                                    <span>&nbsp;&nbsp;{data.remark}</span>
                            </TableCell>
                            <TableCell style={tableCell}>{data.date}</TableCell>
                            <TableRow>
                            <TableCell style={tableCell}>Itemname</TableCell>
                            <TableCell style={tableCell}>Qt.</TableCell>
                            <TableCell style={tableCell}>Wt.</TableCell>
                            <TableCell style={tableCell}>Purity</TableCell>
                            <TableCell style={tableCell}>Waste</TableCell>
                            <TableCell style={tableCell}>Rate</TableCell>
                            <TableCell style={tableCell}>Making cha.</TableCell>
                            {data.state === 'MADHYA PRADESH' ?
                            <TableCell style={tableCell}>Sgst<br></br>Cgst</TableCell>   
                            :<TableCell style={tableCell}>Igst</TableCell>}
                            <TableCell style={tableCell}>Total</TableCell>
                            </TableRow>
                            {data.details.map((det) =>
                            <TableRow>
                            <TableCell style={tableCell}>{det.itemname}</TableCell>
                            <TableCell style={tableCell}>{det.qty}</TableCell>
                            <TableCell style={tableCell}>{det.wt}</TableCell>
                            <TableCell style={tableCell}>{det.purity}&nbsp;%</TableCell>
                            <TableCell style={tableCell}>{det.waste}&nbsp;%</TableCell>
                            <TableCell style={tableCell}>{det.rate}</TableCell>
                            <TableCell style={tableCell}>{
                            det.unit === '1' ? <span>{Number((det.makingcha)).toFixed(2)}</span>
                            : det.unit === '2'  ? <span>{((det.wt || 0) * (det.makingcha || 0)).toFixed(2)}</span>
                            : det.unit === '3' ? <span>{((det.makingcha || 0) * (det.qty || 0)).toFixed(2)}</span>
                            : det.unit === '4' ? <span>{Number((det.makingcha || 0)).toFixed(2)}</span> : ''}
                            </TableCell>
                            {data.state === 'MADHYA PRADESH' ?
                            <TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{data.sgst}</span>&nbsp;
                            {
                            det.unit === '1'  ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                            +Number(det.makingcha)) * Number(data.sgst) / 100).toFixed(2)}</span>
                            :det.unit === '2' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
                            +(det.wt * det.makingcha)) * Number(data.sgst) / 100).toFixed(2)}</span>:
                            det.unit === '3' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
                            +(det.qty * det.makingcha)) * Number(data.sgst) / 100).toFixed(2)}</span>:
                            det.unit === '4' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                            +Number(det.makingcha)) * Number(data.sgst) / 100).toFixed(2)}</span>:''
                            }
                            <br></br>
                            <span style={{ backgroundColor: "red" }}>{data.cgst}</span>&nbsp;
                            {
                            det.unit === '1' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                            +Number(det.makingcha)) * Number(data.cgst) / 100).toFixed(2)}</span>
                            :det.unit === '2' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
                            +(det.wt * det.makingcha)) * Number(data.cgst) / 100).toFixed(2)}</span>:
                            det.unit === '3' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
                            +(det.qty * det.makingcha)) * Number(data.cgst) / 100).toFixed(2)}</span>:
                            det.unit === '4' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                            +Number(det.makingcha)) * Number(data.cgst) / 100).toFixed(2)}</span>:''
                            }
                            </TableCell>   
                            :<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{data.igst}</span>&nbsp;
                            {
                            det.unit === '1' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                            +Number(det.makingcha)) * Number(data.igst) / 100).toFixed(2)}</span>
                            :det.unit === '2' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
                            +(det.wt * det.makingcha)) * Number(data.igst) / 100).toFixed(2)}</span>:
                            det.unit === '3' ? <span>{((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
                            +(det.qty * det.makingcha)) * Number(data.igst) / 100).toFixed(2)}</span>:
                            det.unit === '4' ? <span>{((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
                            +Number(det.makingcha)) * Number(data.igst) / 100).toFixed(2)}</span>:''
                            }
                            </TableCell>}
{data.state === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{
det.rate === '0' ?
<span>{(((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:det.unit === '1' ? <span>{(((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
+Number(det.makingcha)) * Number(Number(data.sgst) + Number(data.cgst)) / 100) + Number(((((det.wt * det.purity) / 100) 
+ Number((det.wt * det.waste) / 100)) * (det.rate)) + Number(det.makingcha))).toFixed(2)}</span>
:det.unit === '2' ? <span>{(((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
+(det.wt * det.makingcha)) * Number(Number(data.sgst) + Number(data.cgst)) / 100)+(((((det.wt * det.purity) / 100) 
+ ((det.wt * det.waste) / 100)) *(det.rate)) + (det.wt * det.makingcha))).toFixed(2)}</span>:
det.unit === '3' ? <span>{(((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
+(det.qty * det.makingcha))* Number(Number(data.sgst) + Number(data.cgst)) /100) + (((((det.wt * det.purity) / 100) 
+ ((det.wt * det.waste) / 100))* (det.rate)) + (det.qty * det.makingcha))).toFixed(2)}</span>:
det.unit === '4' ? <span>{(((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
+Number(det.makingcha))* Number(Number(data.sgst) + Number(data.cgst)) / 100) + Number(((((det.wt * det.purity) / 100) 
+ Number((det.wt * det.waste) / 100)) * (det.rate)) + Number(det.makingcha))).toFixed(2)}</span>:''
}
</TableCell>:
<TableCell style={tableCell}>{
det.rate === '0' ?
<span>{(((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:det.unit === '1' ? <span>{(((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
+Number(det.makingcha)) * Number(data.igst) / 100) + Number(((((det.wt * det.purity) / 100) 
+ Number((det.wt * det.waste) / 100)) * (det.rate)) + Number(det.makingcha))).toFixed(2)}</span>
:det.unit === '2' ? <span>{(((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100)) *(det.rate))
+(det.wt * det.makingcha)) * Number(data.igst) / 100)+(((((det.wt * det.purity) / 100) 
+ ((det.wt * det.waste) / 100)) *(det.rate)) + (det.wt * det.makingcha))).toFixed(2)}</span>:
det.unit === '3' ? <span>{(((((((det.wt * det.purity) / 100) + ((det.wt * det.waste) / 100))* (det.rate))
+(det.qty * det.makingcha))* Number(data.igst) /100) + (((((det.wt * det.purity) / 100) 
+ ((det.wt * det.waste) / 100))* (det.rate)) + (det.qty * det.makingcha))).toFixed(2)}</span>:
det.unit === '4' ? <span>{(((((Number((det.wt * det.purity) / 100) + Number((det.wt * det.waste) / 100)) * (det.rate))
+Number(det.makingcha))* Number(data.igst) / 100) + Number(((((det.wt * det.purity) / 100) 
+ Number((det.wt * det.waste) / 100)) * (det.rate)) + Number(det.makingcha))).toFixed(2)}</span>:''
}
</TableCell>}
            </TableRow>
                )}
                {/* <TableCell style={tableCell}>{data.due}</TableCell>
                <TableCell style={tableCell}>{data.amt}</TableCell>
                <TableCell style={tableCell}>{data.total}</TableCell> */}
                </TableRow>   
                ):null
                }
        </TableBody>
        </Table>
        </TableContainer>:''
        }   <BlockUI blocking={this.state.blocking} />
        </div>
        )
    }
    getinvsilver() {
    let grandwt = 0;
    const rowTotals = this.state.invdatasilver.map(
    row =>
    Number(row.total_grami) || 0
    
    );
    
    if (rowTotals.length > 0) {
    grandwt = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandwt;
    }
    getinvgold() {
    let grandwt = 0;
    const rowTotals = this.state.invdatagold.map(
    row =>
    Number(row.total_grami) || 0
    
    );
    
    if (rowTotals.length > 0) {
    grandwt = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandwt;
    }
    getpursilver() {
    let grandwt = 0;
    const rowTotals = this.state.purdatasilver.map(
    row =>
    Number(row.total_gram) || 0
    
    );
    
    if (rowTotals.length > 0) {
    grandwt = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandwt;
    }
    getpurgold() {
    let grandwt = 0;
    const rowTotals = this.state.purdatagold.map(
    row =>
    Number(row.total_gram) || 0
    
    );
    
    if (rowTotals.length > 0) {
    grandwt = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandwt;
    }

    getinvsilverir() {
        let grandwt = 0;
        const rowTotals = this.state.invdatasilver.map(
        row =>
        Number(row.total_ratei) || 0
        
        );
        
        if (rowTotals.length > 0) {
        grandwt = rowTotals.reduce((acc, val) => acc + val);
        }
        return grandwt;
        }
        getinvgoldir() {
        let grandwt = 0;
        const rowTotals = this.state.invdatagold.map(
        row =>
        Number(row.total_ratei) || 0
        
        );
        
        if (rowTotals.length > 0) {
        grandwt = rowTotals.reduce((acc, val) => acc + val);
        }
        return grandwt;
        }
        getpursilverr() {
        let grandwt = 0;
        const rowTotals = this.state.purdatasilver.map(
        row =>
        Number(row.total_rate) || 0
        
        );
        
        if (rowTotals.length > 0) {
        grandwt = rowTotals.reduce((acc, val) => acc + val);
        }
        return grandwt;
        }
        getpurgoldr() {
        let grandwt = 0;
        const rowTotals = this.state.purdatagold.map(
        row =>
        Number(row.total_rate) || 0
        
        );
        
        if (rowTotals.length > 0) {
        grandwt = rowTotals.reduce((acc, val) => acc + val);
        }
        return grandwt;
        }
}
export default PurchaseReports;

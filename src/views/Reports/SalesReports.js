import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import SaleEdit from "./EditSale.js";
import Select from 'react-select';
import axios from 'axios';
import { Grid } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import BlockUI from "../../BlockUI/BlockUI";


class SalesReports extends React.Component {
    constructor() {
        super()
        this.state = {
            saledata: '', visibleedit: '', visibleeditgst:'', show: '', saledatagst: '', searchdata: '', searchdatagst: '',
            startdate:'', enddate:'', daterange:'', daterangegst:'', cname:'', paidamt:'', vat:'',c_id:'',remark:'',
            address:'',state:'',cdate:'',commitdate:'',paytype:'',due:'',total:'', customerid:'',name:'',
            selectOptions:'',cusdata:'',cusdatagst:'',totaldata:'',gettotalwgst:'',gettotalgst:'',
            cgst:'', sgst:'', igst:'',cgstno:'',amt1:'',premarkcash:'',
            premarkonline:'',mode:'',mode1:''

        };
    }
    state = {
        blocking: false,
        user: {}
      };
    SubmitGst = (data) => {
    this.setState({blocking:true, visibleeditgst:'' })
    let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/billgstupdate/" + data.billno;
    let data1 = data;
    fetch(url, {
    method: 'PUT',
    headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
    },
    body: JSON.stringify(data1)
    }).then((result) => {
    result.json().then((resp) => {
        this.setState({blocking:false})
        this.componentDidMount();
    })})}

    Submit = (data) => {
    this.setState({ visibleedit:'',blocking:true })
    let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/billupdate/" + data.billno;
    let data1 = data;
    fetch(url, {
    method: 'PUT',
    headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
    },
    body: JSON.stringify(data1)
    }).then((result) => {
    result.json().then((resp) => {
        this.setState({blocking:false})
        this.componentDidMount();
    })})}
    
    async getdata() {
        this.setState({blocking:true})
    const res = await axios.get('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getcus')
    const data = res.data
    
    const options = data.map(d => ({
    "value": d.id,
    "label": d.customername,
    "remark": d.remark,
    "address": d.address,
    "citys": d.c_state }))
    this.setState({ selectOptions: options,blocking:false })
    }
    
    handleChange(e) {this.setState({blocking:true})
    this.setState({ customerid: e.value, name: e.label, remark: e.remark,address: e.address,citys: e.citys,
    daterange: '', daterangegst: '', searchdatagst: '', searchdata: '',saledata:'',saledatagst:''})
    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getcusdata/' + e.value).then((resp) => {
                resp.json().then((result) => {
                    this.setState({ cusdata: result })
                })
            })
    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/gettotal/' + e.value).then((resp) => {
        resp.json().then((result) => {
            this.setState({ totaldata: result,blocking:false })
        })
    })
    }

    togglegst = (e) => {
        this.setState({ show: e.target.value, daterange: '', daterangegst: '', searchdatagst: '', searchdata: '',
        visibleedit:'',customerid:'' })
        this.componentDidMount()
        e.preventDefault();
    }
    search = (key) => {  
        if (this.state.show === 'yes') {
            fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getgstsearch/' + key).then((resp) => {
                resp.json().then((result) => {
                    this.setState({ searchdatagst: result })
                })
            })
        } else {
            fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getbillsearch/' + key).then((resp) => {
                resp.json().then((result) => {
                    this.setState({ searchdata: result })
                })
            })
        }
    };
    getDate =() => {
        if(this.state.show === 'yes') {
            fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/daterangegst/' + this.state.startdate + '/' + this.state.enddate).then((resp) => {
                resp.json().then((result) => {
                    this.setState({ daterangegst: result })
                })
            })
        } else {
            fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/daterange/' + this.state.startdate + '/' + this.state.enddate).then((resp) => {
                resp.json().then((result) => {
                    this.setState({ daterange: result })
                })
            })
        }
    }
    PrintPdf(){
        window.print()
    }

    repay(){  
        if(this.state.ramt){
            let data={
                ramt:this.state.ramt
            }
            fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/repaymentgst', {
            method: 'POST',
            headers: { "Content-Type": "application/json",
            "Accept": "application/json"
            },
            body: JSON.stringify(data)
            }).then((result) => {
            result.json().then((resp) => {
                this.setState({m:'Upadated'})
            })
            })
        }
       
        };

    componentDidMount() {
        fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getsalegst').then((resp) => {
            resp.json().then((result) => {
                this.setState({ saledatagst: result })
            })
        })
        fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getsale').then((resp) => {
            resp.json().then((result) => {
                this.setState({ saledata: result })
            })
        })
        fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/gettotalwgst').then((resp) => {
            resp.json().then((result) => {
                this.setState({ gettotalwgst: result })
            })
        })
        fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/gettotalgst').then((resp) => {
            resp.json().then((result) => {
                this.setState({ gettotalgst: result })
            })
        })
        this.getdata();
        // setInterval(this.getYourData.bind(this), 5000); 
    }
    
    edit = (data) => {
        this.setState({visibleedit: data.id,cname: data.name,paidamt: data.amount,vat: data.vat,c_id: data.c_id,remark: data.remark,
            address: data.address,state: data.state,cdate: data.cdate,commitdate: data.commitdate,paytype: data.paytype,due: data.due,
            total: data.total,cgstno:data.cgstno,amt1:data.amt1,premarkcash:data.premarkcash,
            premarkonline:data.premarkonline,mode:data.mode,mode1:data.mode1
        })
    }
    editgst = (data) => {
        this.setState({visibleeditgst: data.id,cname: data.name,paidamt: data.amount,vat: data.vat,c_id: data.c_id,remark: data.remark,
            address: data.address,state: data.state,cdate: data.cdate,commitdate: data.commitdate,paytype: data.paytype,due: data.due,
            total: data.total,saledatagst:'',cgst:data.cgst,sgst:data.sgst,igst:data.igst,cgstno:data.cgstno,
            amt1:data.amt1,premarkcash:data.premarkcash,
            premarkonline:data.premarkonline,mode:data.mode,mode1:data.mode1
        })
    }

    Cancel = () => {
        this.setState({
            visibleedit: '',
        })
    }
    CancelGst = () => {
        this.setState({
            visibleeditgst: '',
        })
        this.componentDidMount()
    }

    render() {
        const numberFormat = (value) =>
        new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR'
        }).format(value);
        const tableCell = {padding:'4px'}
        return (
         


            <div>{this.state.visibleedit === '' && this.state.visibleeditgst === '' ?
                <div style={{ width: '1200px', padding: '5px' }} class="no-printme">
                    <Grid container spacing={1}>
                    <Grid item xs={8}>
                    <select className='textfield' value={this.state.show} onChange={this.togglegst}>
                        <option value="yes">With GST</option>
                        <option value="">Without GST</option>
                    </select>&nbsp;
                <input type='text' className='textfield1' placeholder='Search customers...' onChange={(event) => this.search(event.target.value)} />
                &nbsp;<input type="date" className='textfield1' name='startdate' value={this.state.startdate} 
                onChange={(e)=>this.setState({startdate: e.target.value})}></input>
                &nbsp;<input type='date' className='textfield1' name='enddate' value={this.state.enddate} 
                onChange={(e)=>this.setState({enddate: e.target.value})}></input>
                &nbsp;<input type='submit' value='Get' name='get' className='btn' onClick={this.getDate}></input>
                &nbsp;<input type='submit' value='Print && Pdf' name='pdf' className='pdf' onClick={this.PrintPdf}></input>
                </Grid>
                {/* <Grid item xs={2}>
                    <Select options={this.state.selectOptions} onChange={this.handleChange.bind(this)}
                     placeholder="Select Cus." className='textfield' />
                    </Grid> */}
                </Grid>     
            </div>:''}
                {this.state.show === '' && this.state.visibleedit === '' && this.state.searchdata === '' 
                && this.state.daterange === '' && this.state.customerid === '' ?
                    <TableContainer component={Paper}>
                        <Table aria-label="spanning table">
                            <TableHead>
                                <TableRow>
                                    <TableCell style={tableCell}>Bill No.</TableCell>
                                    <TableCell style={tableCell}>Customers Name</TableCell>
                                    <TableCell style={tableCell}>Date</TableCell>
                                    <TableCell style={tableCell} align='center'>Item Details</TableCell>
                                    <TableCell style={tableCell}>Sub Total</TableCell>
                                    <TableCell style={tableCell}>Paid Amt.</TableCell>
                                    <TableCell style={tableCell}>Due. Amt.</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                    {
                                    this.state.saledata ?
                                    this.state.saledata.map((data,i) =>
                                    <TableRow>
                                    <TableCell style={tableCell}>{data.id}</TableCell>
                                    <TableCell style={tableCell}>
                                        <b><Button onClick={() => this.edit(data)}>{data.name}</Button></b><br></br>
                                        <span>{data.address}</span><br></br>
                                        <span>{data.state}</span><br></br>
                                        <span>{data.remark}</span>
                                    </TableCell>
                                    <TableCell style={tableCell}>{data.date}</TableCell>
                                    <TableRow>
                                    <TableCell style={tableCell}>Itemname</TableCell>
                                    <TableCell style={tableCell}>Qt.</TableCell>
                                    <TableCell style={tableCell}>Wt.</TableCell>
                                    <TableCell style={tableCell}>Rate</TableCell>
                                    <TableCell style={tableCell}>Mak.Cha</TableCell>
                                    <TableCell style={tableCell}>Polish Cha</TableCell>
                                    <TableCell style={tableCell}>Dis.</TableCell>
                                    <TableCell style={tableCell}>Total</TableCell>
                                    </TableRow>
                                    {data.details.map((det) =>
                                    <TableRow>
                                    <TableCell style={tableCell}>{det.itemname}</TableCell>
                                    <TableCell style={tableCell}>{det.qty}</TableCell>
                                    <TableCell style={tableCell}>{det.wt}</TableCell>
                                    <TableCell style={tableCell}>{det.rate}</TableCell>
                                    <TableCell style={tableCell}>{
                                    det.unit === '1' ? <span>{Number((det.makingcha)).toFixed(2)}</span>
                                    : det.unit === '2' ? <span>{((det.wt || 0) * (det.makingcha || 0)).toFixed(2)}</span>
                                    : det.unit === '3' ? <span>{((det.makingcha || 0) * (det.qty || 0)).toFixed(2)}</span>
                                    : det.unit === '4' ? <span>{Number((det.makingcha || 0)).toFixed(2)}</span> : ''}
                                    </TableCell>
                                    <TableCell style={tableCell}>{det.polishcha}(%)<br></br>
                                    {(((det.wt * det.rate) * det.polishcha) / 100).toFixed(2)}</TableCell>
                                    <TableCell style={tableCell}>{det.disamt}</TableCell>
                                    <TableCell style={tableCell}>{
                                    det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                                    )).toFixed(2)}</span>
                                    : det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
                                    )).toFixed(2)}</span>
                                    : det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
                                    )).toFixed(2)}</span>
                                    : det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                                    )).toFixed(2)}</span> : ''
                                    }</TableCell>
                                    </TableRow>
                                    )}
                                    <TableCell style={tableCell}>
                                 <b>{numberFormat(data.total)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                                 <b>{data.mode}</b><br></br>
                                 <b>{numberFormat(data.amount)}</b><br></br>
                                 <b>{data.mode1}</b><br></br>
                                 <b>{numberFormat(data.amt1)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                                 <b>{numberFormat(data.due)}</b>
                            </TableCell>
                                    </TableRow>   
                                    ):null
                                    }
                            </TableBody>
                            {
            this.state.gettotalwgst ?
            this.state.gettotalwgst.map((data,i) =>
             <TableRow>
                 <TableCell><b>Subtotal</b></TableCell>
                 <TableCell></TableCell>
                 <TableCell></TableCell>
                 <TableCell></TableCell>
                             <TableCell style={tableCell}>{data.total === null ?
                             <b>No Data</b>:
                                 <b>{numberFormat(data.total)}</b> }
                            </TableCell>
                             <TableCell style={tableCell}>{data.amt === null ?
                                <b>No Data</b>:
                                 <b>{numberFormat(Number(data.amt) + Number(data.amt1))}</b>}
                            </TableCell>
                             <TableCell style={tableCell}>{data.due === null ?
                                <b>No Data</b>:
                                 <b>{numberFormat(data.due)}</b>}
                            </TableCell>
             </TableRow>
             ):null
            }
                        </Table>
                    </TableContainer>:this.state.show === '' && this.state.visibleedit ?
                    <div><SaleEdit data={{editid:this.state.visibleedit,Submit:this.Submit.bind(this),
                    Cancel:this.Cancel.bind(this),
                    customername:this.state.cname,amount:this.state.paidamt,vat:this.state.vat,c_id:this.state.c_id,
                    remark:this.state.remark,address:this.state.address,state:this.state.state,cdate:this.state.cdate,
                    commitdate:this.state.commitdate,paytype:this.state.paytype,due:this.state.due,total:this.state.total,
                    cgstno:this.state.cgstno,amt1:this.state.amt1,premarkcash:this.state.premarkcash,
                    premarkonline:this.state.premarkonline,mode:this.state.mode,mode1:this.state.mode1}}/>
                    </div>:''}
                    {this.state.show === '' && this.state.searchdata ?
                     <TableContainer component={Paper}>
                     <Table aria-label="spanning table">
                         <TableHead>
                             <TableRow>
                                 <TableCell>Bill No.</TableCell>
                                 <TableCell>Customers Name</TableCell>
                                 <TableCell>Date</TableCell>
                                 <TableCell align='center'>Item Details</TableCell>
                                 <TableCell>Sub Total</TableCell>
                                    <TableCell>Paid Amt.</TableCell>
                                    <TableCell>Due. Amt.</TableCell>
                             </TableRow>
                         </TableHead>
                         <TableBody>
                                 {
                                 this.state.searchdata ?
                                 this.state.searchdata.map((data,i) =>
                                 <TableRow>
                                 <TableCell style={tableCell}>{data.id}</TableCell>
                                 <TableCell style={tableCell}>
                                 <b><Button onClick={() => this.edit(data)}>{data.name}</Button></b><br></br>
                                        <span>{data.address}</span><br></br>
                                        <span>{data.state}</span><br></br>
                                        <span>{data.remark}</span>
                                 </TableCell>
                                 <TableCell style={tableCell}>{data.date}</TableCell>
                                 <TableRow>
                                 <TableCell style={tableCell}>Itemname</TableCell>
                                 <TableCell style={tableCell}>Qt.</TableCell>
                                 <TableCell style={tableCell}>Wt.</TableCell>
                                 <TableCell style={tableCell}>Rate</TableCell>
                                 <TableCell style={tableCell}>Mak.Cha</TableCell>
                                 <TableCell style={tableCell}>Polish Cha</TableCell>
                                 <TableCell style={tableCell}>Dis.</TableCell>
                                 <TableCell style={tableCell}>Total</TableCell>
                                 </TableRow>
                                 {data.details.map((det) =>
                                 <TableRow>
                                 <TableCell style={tableCell}>{det.itemname}</TableCell>
                                 <TableCell style={tableCell}>{det.qty}</TableCell>
                                 <TableCell style={tableCell}>{det.wt}</TableCell>
                                 <TableCell style={tableCell}>{det.rate}</TableCell>
                                 <TableCell style={tableCell}>{
                                 det.unit === '1' ? <span>{Number((det.makingcha)).toFixed(2)}</span>
                                 : det.unit === '2' ? <span>{((det.wt || 0) * (det.makingcha || 0)).toFixed(2)}</span>
                                 : det.unit === '3' ? <span>{((det.makingcha || 0) * (det.qty || 0)).toFixed(2)}</span>
                                 : det.unit === '4' ? <span>{Number((det.makingcha || 0)).toFixed(2)}</span> : ''}
                                 </TableCell>
                                 <TableCell style={tableCell}>{det.polishcha}(%)<br></br>
                                    {(((det.wt * det.rate) * det.polishcha) / 100).toFixed(2)}</TableCell>
                                 <TableCell style={tableCell}>{det.disamt}</TableCell>
                                 <TableCell style={tableCell}>{
                                 det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                                 - (det.disamt || 0))).toFixed(2)}</span>
                                 : det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
                                 - (det.disamt || 0))).toFixed(2)}</span>
                                 : det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
                                 - (det.disamt || 0))).toFixed(2)}</span>
                                 : det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                                 - (det.disamt || 0))).toFixed(2)}</span> : ''
                                 }</TableCell>
                                 </TableRow>
                                 )}
                                    <TableCell style={tableCell}>
                                 <b>{numberFormat(data.total)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                             <b>{data.mode}</b><br></br>
                                 <b>{numberFormat(data.amount)}</b><br></br>
                                 <b>{data.mode1}</b><br></br>
                                 <b>{numberFormat(data.amt1)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                                 <b>{numberFormat(data.due)}</b>
                            </TableCell>
                                 </TableRow>   
                                 ):null
                                 }
                         </TableBody>
                     </Table>
                 </TableContainer>
                    :this.state.show === '' && this.state.daterange ?
                    <TableContainer component={Paper}>
                        <Table aria-label="spanning table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Bill No.</TableCell>
                                    <TableCell>Customers Name</TableCell>
                                    <TableCell>Date</TableCell>
                                    <TableCell align='center'>Item Details</TableCell>
                                    <TableCell>Sub Total</TableCell>
                                    <TableCell>Paid Amt.</TableCell>
                                    <TableCell>Due. Amt.</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                    {
                                    this.state.daterange ?
                                    this.state.daterange.map((data,i) =>
                                    <TableRow>
                                    <TableCell style={tableCell}>{data.id}</TableCell>
                                    <TableCell style={tableCell}>
                                    <b><Button onClick={() => this.edit(data)}>{data.name}</Button></b><br></br>
                                        <span>{data.address}</span><br></br>
                                        <span>{data.state}</span><br></br>
                                        <span>{data.remark}</span>
                                    </TableCell>
                                    <TableCell style={tableCell}>{data.date}</TableCell>
                                    <TableRow>
                                    <TableCell style={tableCell}>Itemname</TableCell>
                                    <TableCell style={tableCell}>Qt.</TableCell>
                                    <TableCell style={tableCell}>Wt.</TableCell>
                                    <TableCell style={tableCell}>Rate</TableCell>
                                    <TableCell style={tableCell}>Mak.Cha</TableCell>
                                    <TableCell style={tableCell}>Polish Cha</TableCell>
                                    <TableCell style={tableCell}>Dis.</TableCell>
                                    <TableCell style={tableCell}>Total</TableCell>
                                    </TableRow>
                                    {data.details.map((det) =>
                                    <TableRow>
                                    <TableCell style={tableCell}>{det.itemname}</TableCell>
                                    <TableCell style={tableCell}>{det.qty}</TableCell>
                                    <TableCell style={tableCell}>{det.wt}</TableCell>
                                    <TableCell style={tableCell}>{det.rate}</TableCell>
                                    <TableCell style={tableCell}>{
                                    det.unit === '1' ? <span>{Number((det.makingcha)).toFixed(2)}</span>
                                    : det.unit === '2' ? <span>{((det.wt || 0) * (det.makingcha || 0)).toFixed(2)}</span>
                                    : det.unit === '3' ? <span>{((det.makingcha || 0) * (det.qty || 0)).toFixed(2)}</span>
                                    : det.unit === '4' ? <span>{Number((det.makingcha || 0)).toFixed(2)}</span> : ''}
                                    </TableCell>
                                    <TableCell style={tableCell}>{det.polishcha}(%)<br></br>
                                    {(((det.wt * det.rate) * det.polishcha) / 100).toFixed(2)}</TableCell>
                                    <TableCell style={tableCell}>{det.disamt}</TableCell>
                                    <TableCell style={tableCell}>{
                                    det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                                    - (det.disamt || 0))).toFixed(2)}</span>
                                    : det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
                                    - (det.disamt || 0))).toFixed(2)}</span>
                                    : det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
                                    - (det.disamt || 0))).toFixed(2)}</span>
                                    : det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                                    - (det.disamt || 0))).toFixed(2)}</span> : ''
                                    }</TableCell>
                                    </TableRow>
                                    )}
                                    <TableCell style={tableCell}>
                                 <b>{numberFormat(data.total)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                             <b>{data.mode}</b><br></br>
                                 <b>{numberFormat(data.amount)}</b><br></br>
                                 <b>{data.mode1}</b><br></br>
                                 <b>{numberFormat(data.amt1)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                                 <b>{numberFormat(data.due)}</b>
                            </TableCell>
                                    </TableRow>   
                                    ):null
                                    }
                            </TableBody>
                        </Table>
                    </TableContainer>:''}
                    {this.state.show === 'yes' && this.state.saledatagst && this.state.searchdatagst === '' && this.state.daterangegst === '' ?
                    <TableContainer component={Paper}>
                    <Table aria-label="spanning table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Bill No.</TableCell>
                                <TableCell>Customers Name</TableCell>
                                <TableCell>Date</TableCell>
                                <TableCell align='center'>Item Details</TableCell>
                                <TableCell>Sub Total</TableCell>
                                    <TableCell>Paid Amt.</TableCell>
                                    <TableCell>Due. Amt.</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                                {
                                this.state.saledatagst ?
                                this.state.saledatagst.map((data,i) =>
                                <TableRow>
                                <TableCell style={tableCell}>{data.id}</TableCell>
                                <TableCell style={tableCell}>
                                <b><Button onClick={() => this.editgst(data)}>{data.name}</Button></b><br></br>
                                        <span>{data.address}</span><br></br>
                                        <span>{data.state}</span><br></br>
                                </TableCell>
                                <TableCell style={tableCell}>{data.date}</TableCell>
                                <TableRow>
                                <TableCell style={tableCell}>Itemname</TableCell>
                                <TableCell style={tableCell}>Qt.</TableCell>
                                <TableCell style={tableCell}>Wt.</TableCell>
                                <TableCell style={tableCell}>Rate</TableCell>
                                <TableCell style={tableCell}>Mak.Cha</TableCell>
                                <TableCell style={tableCell}>Polish Cha</TableCell>
                                <TableCell style={tableCell}>Dis.</TableCell>
                                {data.state === 'MADHYA PRADESH' ?
                                <TableCell style={tableCell}>Sgst<br></br>Cgst</TableCell>   
                                :<TableCell style={tableCell}>Igst</TableCell>}
                                <TableCell style={tableCell}>Total</TableCell>
                                </TableRow>
                                {data.details.map((det) =>
                                <TableRow>
                                <TableCell style={tableCell}>{det.itemname}</TableCell>
                                <TableCell style={tableCell}>{det.qty}</TableCell>
                                <TableCell style={tableCell}>{det.wt}</TableCell>
                                <TableCell style={tableCell}>{det.rate}</TableCell>
                                <TableCell style={tableCell}>{
                                det.unit === '1' ? <span>{Number((det.makingcha)).toFixed(2)}</span>
                                : det.unit === '2' ? <span>{((det.wt || 0) * (det.makingcha || 0)).toFixed(2)}</span>
                                : det.unit === '3' ? <span>{((det.makingcha || 0) * (det.qty || 0)).toFixed(2)}</span>
                                : det.unit === '4' ? <span>{Number((det.makingcha || 0)).toFixed(2)}</span> : ''}
                                </TableCell>
                                <TableCell style={tableCell}>{det.polishcha}(%)<br></br>
                                    {(((det.wt * det.rate) * det.polishcha) / 100).toFixed(2)}</TableCell>
                                <TableCell style={tableCell}>{det.disamt}</TableCell>
                                {data.state === 'MADHYA PRADESH' ?
                                <TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{data.sgst}</span>&nbsp;
                                {
                                det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                                ) * Number(data.sgst) / 100).toFixed(2)}</span>
                                : det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) 
                                + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.sgst) / 100).toFixed(2)}</span>
                                : det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.makingcha || 0) * (det.qty || 0)) 
                                + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.sgst) / 100).toFixed(2)}</span>
                                : det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) 
                                + Number(((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.sgst) / 100).toFixed(2)}</span> : ''
                                }
                                <br></br>
                                <span style={{ backgroundColor: "red" }}>{data.cgst}</span>&nbsp;
                                {
                                det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                                ) * Number(data.cgst) / 100).toFixed(2)}</span>
                                : det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) 
                                + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.cgst) / 100).toFixed(2)}</span>
                                : det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.makingcha || 0) * (det.qty || 0)) 
                                + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.cgst) / 100).toFixed(2)}</span>
                                : det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) 
                                + Number(((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.cgst) / 100).toFixed(2)}</span> : ''
                                }
                                </TableCell>   
                                :<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{data.igst}</span>&nbsp;
                                {
                                det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                                ) * Number(data.igst) / 100).toFixed(2)}</span>
                                : det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) 
                                + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.igst) / 100).toFixed(2)}</span>
                                : det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.makingcha || 0) * (det.qty || 0)) 
                                + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.igst) / 100).toFixed(2)}</span>
                                : det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) 
                                + Number(((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.igst) / 100).toFixed(2)}</span> : ''
                                }
                                </TableCell>}
{data.state === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{
det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) + Number(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(Number(data.sgst) + Number(data.cgst)) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) + (((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(Number(data.sgst) + Number(data.cgst)) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) + (((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(Number(data.sgst) + Number(data.cgst)) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) + Number(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(Number(data.sgst) + Number(data.cgst)) / 100)- (data.disamt || 0)).toFixed(2)}</span> : ''
}
</TableCell>:
<TableCell style={tableCell}>{
det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) + Number(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(data.igst) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) + (((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(data.igst) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) + (((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(data.igst) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) + Number(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(data.igst) / 100)- (data.disamt || 0)).toFixed(2)}</span> : ''
}
</TableCell>}
                </TableRow>
                )}
                <TableCell style={tableCell}>
                                 <b>{numberFormat(data.total)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                             <b>{data.mode}</b><br></br>
                                 <b>{numberFormat(data.amount)}</b><br></br>
                                 <b>{data.mode1}</b><br></br>
                                 <b>{numberFormat(data.amt1)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                                 <b>{numberFormat(data.due)}</b>
                            </TableCell>
                </TableRow>   
                ):null
                }
        </TableBody>
        {
            this.state.gettotalgst ?
            this.state.gettotalgst.map((data,i) =>
             <TableRow>
                 <TableCell><b>Subtotal</b></TableCell>
                 <TableCell></TableCell>
                 <TableCell></TableCell>
                 <TableCell></TableCell>
                 <TableCell style={tableCell}>{data.total === null ?
                                 <b>No data</b>:
                                 <b>{numberFormat(data.total)}</b>}
                            </TableCell>
                             <TableCell style={tableCell}>{data.amt === null ?
                             <b>No data</b>:
                             <b>{numberFormat(Number(data.amt) + Number(data.amt1))}</b>}
                            </TableCell>
                             <TableCell style={tableCell}>{data.due === null ?
                                 <b>No data</b>:
                                 <b>{numberFormat(data.due)}</b>}
                            </TableCell>
             </TableRow>
             ):null
            }
                    </Table>
                </TableContainer>:''}
                {this.state.show === 'yes' && this.state.visibleeditgst ?
                <div><SaleEdit data={{editid:this.state.visibleeditgst,SubmitGst:this.SubmitGst.bind(this),
                CancelGst:this.CancelGst.bind(this),
                customername:this.state.cname,amount:this.state.paidamt,vat:this.state.vat,c_id:this.state.c_id,
                remark:this.state.remark,address:this.state.address,state:this.state.state,cdate:this.state.cdate,
                commitdate:this.state.commitdate,paytype:this.state.paytype,due:this.state.due,total:this.state.total,
                cgst:this.state.cgst,sgst:this.state.sgst,igst:this.state.igst,cgstno:this.state.cgstno,
                amt1:this.state.amt1,premarkcash:this.state.premarkcash,
                premarkonline:this.state.premarkonline,mode:this.state.mode,mode1:this.state.mode1}}/>
                </div>:''}
                {this.state.show === 'yes' && this.state.searchdatagst ?
                <TableContainer component={Paper}>
                <Table aria-label="spanning table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Bill No.</TableCell>
                            <TableCell>Customers Name</TableCell>
                            <TableCell>Date</TableCell>
                            <TableCell align='center'>Item Details</TableCell>
                            <TableCell>Sub Total</TableCell>
                                    <TableCell>Paid Amt.</TableCell>
                                    <TableCell>Due. Amt.</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                            {
                            this.state.searchdatagst ?
                            this.state.searchdatagst.map((data,i) =>
                            <TableRow>
                            <TableCell style={tableCell}>{data.id}</TableCell>
                            <TableCell style={tableCell}>
                            <b><Button onClick={() => this.editgst(data)}>{data.name}</Button></b><br></br>
                                        <span>{data.address}</span><br></br>
                                        <span>{data.state}</span><br></br>
                            </TableCell>
                            <TableCell style={tableCell}>{data.date}</TableCell>
                            <TableRow>
                            <TableCell style={tableCell}>Itemname</TableCell>
                            <TableCell style={tableCell}>Qt.</TableCell>
                            <TableCell style={tableCell}>Wt.</TableCell>
                            <TableCell style={tableCell}>Rate</TableCell>
                            <TableCell style={tableCell}>Mak.Cha</TableCell>
                            <TableCell style={tableCell}>Polish Cha</TableCell>
                            <TableCell style={tableCell}>Dis.</TableCell>
                            {data.state === 'MADHYA PRADESH' ?
                            <TableCell style={tableCell}>Sgst<br></br>Cgst</TableCell>   
                            :<TableCell style={tableCell}>Igst</TableCell>}
                            <TableCell style={tableCell}>Total</TableCell>
                            </TableRow>
                            {data.details.map((det) =>
                            <TableRow>
                            <TableCell style={tableCell}>{det.itemname}</TableCell>
                            <TableCell style={tableCell}>{det.qty}</TableCell>
                            <TableCell style={tableCell}>{det.wt}</TableCell>
                            <TableCell style={tableCell}>{det.rate}</TableCell>
                            <TableCell style={tableCell}>{
                            det.unit === '1' ? <span>{Number((det.makingcha)).toFixed(2)}</span>
                            : det.unit === '2' ? <span>{((det.wt || 0) * (det.makingcha || 0)).toFixed(2)}</span>
                            : det.unit === '3' ? <span>{((det.makingcha || 0) * (det.qty || 0)).toFixed(2)}</span>
                            : det.unit === '4' ? <span>{Number((det.makingcha || 0)).toFixed(2)}</span> : ''}
                            </TableCell>
                            <TableCell style={tableCell}>{det.polishcha}(%)<br></br>
                                    {(((det.wt * det.rate) * det.polishcha) / 100).toFixed(2)}</TableCell>
                            <TableCell style={tableCell}>{det.disamt}</TableCell>
                            {data.state === 'MADHYA PRADESH' ?
                            <TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{data.sgst}</span>&nbsp;
                            {
                            det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                            ) * Number(data.sgst) / 100).toFixed(2)}</span>
                            : det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) 
                            + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.sgst) / 100).toFixed(2)}</span>
                            : det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.makingcha || 0) * (det.qty || 0)) 
                            + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.sgst) / 100).toFixed(2)}</span>
                            : det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) 
                            + Number(((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.sgst) / 100).toFixed(2)}</span> : ''
                            }
                            <br></br>
                            <span style={{ backgroundColor: "red" }}>{data.cgst}</span>&nbsp;
                            {
                            det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                            ) * Number(data.cgst) / 100).toFixed(2)}</span>
                            : det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) 
                            + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.cgst) / 100).toFixed(2)}</span>
                            : det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.makingcha || 0) * (det.qty || 0)) 
                            + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.cgst) / 100).toFixed(2)}</span>
                            : det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) 
                            + Number(((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.cgst) / 100).toFixed(2)}</span> : ''
                            }
                            </TableCell>   
                            :<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{data.igst}</span>&nbsp;
                            {
                            det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                            ) * Number(data.igst) / 100).toFixed(2)}</span>
                            : det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) 
                            + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.igst) / 100).toFixed(2)}</span>
                            : det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.makingcha || 0) * (det.qty || 0)) 
                            + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.igst) / 100).toFixed(2)}</span>
                            : det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) 
                            + Number(((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.igst) / 100).toFixed(2)}</span> : ''
                            }
                            </TableCell>}
{data.state === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{
det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) + Number(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(Number(data.sgst) + Number(data.cgst)) / 100)).toFixed(2)}</span>
: det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) + (((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(Number(data.sgst) + Number(data.cgst)) / 100)).toFixed(2)}</span>
: det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) + (((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(Number(data.sgst) + Number(data.cgst)) / 100)).toFixed(2)}</span>
: det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) + Number(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(Number(data.sgst) + Number(data.cgst)) / 100)).toFixed(2)}</span> : ''
}
</TableCell>:
<TableCell style={tableCell}>{
det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) + Number(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(data.igst) / 100)).toFixed(2)}</span>
: det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) + (((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(data.igst) / 100)).toFixed(2)}</span>
: det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) + (((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(data.igst) / 100)).toFixed(2)}</span>
: det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) + Number(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(data.igst) / 100)).toFixed(2)}</span> : ''
}
</TableCell>}
            </TableRow>
            )}
            <TableCell style={tableCell}>
                                 <b>{numberFormat(data.total)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                             <b>{data.mode}</b><br></br>
                                 <b>{numberFormat(data.amount)}</b><br></br>
                                 <b>{data.mode1}</b><br></br>
                                 <b>{numberFormat(data.amt1)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                                 <b>{numberFormat(data.due)}</b>
                            </TableCell>
            </TableRow>   
            ):null
            }
    </TableBody>
                </Table>
            </TableContainer>
                :this.state.show === 'yes' && this.state.daterangegst ?
                <TableContainer component={Paper}>
                    <Table aria-label="spanning table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Bill No.</TableCell>
                                <TableCell>Customers Name</TableCell>
                                <TableCell>Date</TableCell>
                                <TableCell align='center'>Item Details</TableCell>
                                <TableCell>Sub Total</TableCell>
                                    <TableCell>Paid Amt.</TableCell>
                                    <TableCell>Due. Amt.</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                                {
                                this.state.daterangegst ?
                                this.state.daterangegst.map((data,i) =>
                                <TableRow>
                                <TableCell style={tableCell}>{data.id}</TableCell>
                                <TableCell style={tableCell}>
                                <b><Button onClick={() => this.editgst(data)}>{data.name}</Button></b><br></br>
                                        <span>{data.address}</span><br></br>
                                        <span>{data.state}</span><br></br>
                                </TableCell>
                                <TableCell style={tableCell}>{data.date}</TableCell>
                                <TableRow>
                                <TableCell style={tableCell}>Itemname</TableCell>
                                <TableCell style={tableCell}>Qt.</TableCell>
                                <TableCell style={tableCell}>Wt.</TableCell>
                                <TableCell style={tableCell}>Rate</TableCell>
                                <TableCell style={tableCell}>Mak.Cha</TableCell>
                                <TableCell style={tableCell}>Polish Cha</TableCell>
                                <TableCell style={tableCell}>Dis.</TableCell>
                                {data.state === 'MADHYA PRADESH' ?
                                <TableCell style={tableCell}>Sgst<br></br>Cgst</TableCell>   
                                :<TableCell style={tableCell}>Igst</TableCell>}
                                <TableCell style={tableCell}>Total</TableCell>
                                </TableRow>
                                {data.details.map((det) =>
                                <TableRow>
                                <TableCell style={tableCell}>{det.itemname}</TableCell>
                                <TableCell style={tableCell}>{det.qty}</TableCell>
                                <TableCell style={tableCell}>{det.wt}</TableCell>
                                <TableCell style={tableCell}>{det.rate}</TableCell>
                                <TableCell style={tableCell}>{
                                det.unit === '1' ? <span>{Number((det.makingcha)).toFixed(2)}</span>
                                : det.unit === '2' ? <span>{((det.wt || 0) * (det.makingcha || 0)).toFixed(2)}</span>
                                : det.unit === '3' ? <span>{((det.makingcha || 0) * (det.qty || 0)).toFixed(2)}</span>
                                : det.unit === '4' ? <span>{Number((det.makingcha || 0)).toFixed(2)}</span> : ''}
                                </TableCell>
                                <TableCell style={tableCell}>{det.polishcha}(%)<br></br>
                                    {(((det.wt * det.rate) * det.polishcha) / 100).toFixed(2)}</TableCell>
                                <TableCell style={tableCell}>{det.disamt}</TableCell>
                                {data.state === 'MADHYA PRADESH' ?
                                <TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{data.sgst}</span>&nbsp;
                                {
                                det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                                ) * Number(data.sgst) / 100).toFixed(2)}</span>
                                : det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) 
                                + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.sgst) / 100).toFixed(2)}</span>
                                : det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.makingcha || 0) * (det.qty || 0)) 
                                + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.sgst) / 100).toFixed(2)}</span>
                                : det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) 
                                + Number(((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.sgst) / 100).toFixed(2)}</span> : ''
                                }
                                <br></br>
                                <span style={{ backgroundColor: "red" }}>{data.cgst}</span>&nbsp;
                                {
                                det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                                ) * Number(data.cgst) / 100).toFixed(2)}</span>
                                : det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) 
                                + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.cgst) / 100).toFixed(2)}</span>
                                : det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.makingcha || 0) * (det.qty || 0)) 
                                + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.cgst) / 100).toFixed(2)}</span>
                                : det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) 
                                + Number(((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.cgst) / 100).toFixed(2)}</span> : ''
                                }
                                </TableCell>   
                                :<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{data.igst}</span>&nbsp;
                                {
                                det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
                                ) * Number(data.igst) / 100).toFixed(2)}</span>
                                : det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) 
                                + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.igst) / 100).toFixed(2)}</span>
                                : det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.makingcha || 0) * (det.qty || 0)) 
                                + (((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.igst) / 100).toFixed(2)}</span>
                                : det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) 
                                + Number(((det.wt * det.rate) * det.polishcha) / 100)) ) * Number(data.igst) / 100).toFixed(2)}</span> : ''
                                }
                                </TableCell>}
{data.state === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{
det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) + Number(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(Number(data.sgst) + Number(data.cgst)) / 100)).toFixed(2)}</span>
: det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) + (((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(Number(data.sgst) + Number(data.cgst)) / 100)).toFixed(2)}</span>
: det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) + (((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(Number(data.sgst) + Number(data.cgst)) / 100)).toFixed(2)}</span>
: det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) + Number(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(Number(data.sgst) + Number(data.cgst)) / 100)).toFixed(2)}</span> : ''
}
</TableCell>:
<TableCell style={tableCell}>{
det.unit === '1' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) + Number(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(data.igst) / 100)).toFixed(2)}</span>
: det.unit === '2' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) + (((((det.wt || 0) * (det.rate || 0)) + ((det.wt || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(data.igst) / 100)).toFixed(2)}</span>
: det.unit === '3' ? <span>{(((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) + (((((det.wt || 0) * (det.rate || 0)) + ((det.qty || 0) * (det.makingcha || 0)) + (((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(data.igst) / 100)).toFixed(2)}</span>
: det.unit === '4' ? <span>{(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) + Number(((Number((det.wt || 0) * (det.rate || 0)) + Number(det.makingcha || 0) + Number(((det.wt * det.rate) * det.polishcha) / 100)) 
) * Number(data.igst) / 100)).toFixed(2)}</span> : ''
}
</TableCell>}
                </TableRow>
                )}
               <TableCell style={tableCell}>
                                 <b>{numberFormat(data.total)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                             <b>{data.mode}</b><br></br>
                                 <b>{numberFormat(data.amount)}</b><br></br>
                                 <b>{data.mode1}</b><br></br>
                                 <b>{numberFormat(data.amt1)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                                 <b>{numberFormat(data.due)}</b>
                            </TableCell>
                </TableRow>   
                ):null
                }
        </TableBody>
        </Table>
        </TableContainer>:''} 
        {this.state.customerid ?
        <Grid container spacing={1}>
        <Grid item xs={4}>
        <p>Customer Name :&nbsp;{this.state.name}</p>
        <p>Customer Id :&nbsp;{this.state.customerid}</p>
        </Grid>
        <Grid item xs={4}>
        <p>Address :&nbsp;{this.state.address}</p>
        <p>State :&nbsp;{this.state.citys}</p>
        </Grid>
         <TableContainer component={Paper}>
         <Table aria-label="spanning table">
             <TableHead>
                 <TableRow>
                     <TableCell>Gst Include</TableCell>
                     <TableCell>Bill No.</TableCell>
                     <TableCell>Date</TableCell>
                     <TableCell>Total</TableCell>
                     <TableCell>Paid Amt.</TableCell>
                     <TableCell>Due Amt.</TableCell>
                 </TableRow>
             </TableHead>
             <TableBody>
                     {
                     this.state.cusdata ?
                     this.state.cusdata.map((data,i) =>
                     <TableRow>
                     <TableCell style={tableCell}>{data.vat}</TableCell>
                     <TableCell style={tableCell}>{data.bill_no}</TableCell>
                     <TableCell style={tableCell}>{data.date_update}</TableCell>
                     <TableCell style={tableCell}>{data.total}</TableCell>
                     <TableCell style={tableCell}>{data.amt}</TableCell>
                     <TableCell style={tableCell}>{data.due}</TableCell>
                     </TableRow>   
                     ):null
                     }
                     {
                     this.state.totaldata ?
                     this.state.totaldata.map((data,i) =>
                     <TableRow>
                     <TableCell style={tableCell}><b>Sub Total</b></TableCell>
                     <TableCell style={tableCell}></TableCell>
                     <TableCell style={tableCell}></TableCell>
                     <TableCell style={tableCell}>
                                
                                 <b>{numberFormat(data.total)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                
                                 <b>{numberFormat(data.amt)}</b>
                            </TableCell>
                             <TableCell style={tableCell}>
                            
                                 <b>{numberFormat(data.total - data.amt)}</b>
                            </TableCell>
                     </TableRow>   
                     ):null
                     }
             </TableBody>
         </Table>
     </TableContainer>
     {/* <div className='repayment'>
     <div><strong>Repayment</strong></div>
     <div><TextField name="ramt" id="standard-basic" label="Enter Amount" value={this.state.ramt} 
     onChange={(e) => { this.setState({ ramt: e.target.value }) }}/></div> 
     <div><button type='button' value='Pay' name='ramt' className='paybtn' onClick={this.repay()}>Pay</button></div>
     </div> */}
     </Grid>:''
        } <BlockUI blocking={this.state.blocking} />
        </div>
        
        )
    }
}
export default SalesReports;

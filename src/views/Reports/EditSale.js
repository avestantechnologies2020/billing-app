import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Remove';
import Select from 'react-select'
import axios from 'axios'
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import PrintSale from "views/Sale/PrintSale.js";

class Forms extends React.Component {

constructor(props) {
super(props)
var today = new Date(),
date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
var today = new Date(),
time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
var today1 = new Date(),
date1 = today1.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() ;
var igst = this.props.data.igst;
var sgst = this.props.data.sgst;
var cgst = this.props.data.cgst;
var gst = Number(cgst) + Number(sgst);
var amount = this.props.data.amount;
var tax = this.props.data.vat;
var c_id = this.props.data.c_id;
var remark = this.props.data.remark;
var address = this.props.data.address;
var state = this.props.data.state;
var cdate = this.props.data.cdate;
var commitdate = this.props.data.commitdate;
var paytype  = this.props.data.paytype;
var billno = this.props.data.editid;
var cname = this.props.data.customername;
var due = this.props.data.due;
var total = this.props.data.total;
var cgstno = this.props.data.cgstno;
this.state = {
billno: billno, customerid: c_id, customername: "", remark: remark, address: address, refno: "", date: cdate,amtnew:'',
commitdate: commitdate, vat: tax, paytype: paytype, itemlist: "", rowData: [], itemid: "", id: 0, selectOptions: [], id: "",
name: cname, seen: false, seeni: false, price: '', print: false, billdata: "", totalamt:total, amt: amount, due: due,
currentdate : date1, currenttime : time, nameerror:'', citys:state, statelist:'', gst:gst, sgst:sgst, cgst:cgst, igst:igst,
date1:date1,maxid:'',editid:this.props.data.editid,cgstno:cgstno,amt1:this.props.data.amt1,
premarkcash:this.props.data.premarkcash,premarkonline:this.props.data.premarkonline,mode:this.props.data.mode,
mode1:this.props.data.mode1,cgstno:this.props.data.cgstno
};
this.getdata();
this.handleQuantiteChange = this.handleQuantiteChange.bind(this);
this.handlePrixChange = this.handlePrixChange.bind(this);
this.handleselectprdtChange = this.handleselectprdtChange.bind(this);
this.handleRowDelete = this.handleRowDelete.bind(this);
this.handleRowAdd = this.handleRowAdd.bind(this);
this.handleMakingchaChange = this.handleMakingchaChange.bind(this);
this.getTotal = this.getTotal.bind(this);
}

togglePrint(){
if(this.valid()){
const total1 = this.getTotalwithout() - 0;
const total1new = total1.toFixed(2);
const total =  this.getTotal() - 0;
const totalnew = total.toFixed(2);
const totali =  this.getTotaligst() - 0;
const totalnewi = totali.toFixed(2);
if(this.state.vat === 'yes' && this.state.citys === "MADHYA PRADESH"){
    this.setState({print: !this.state.print,totalamt: totalnew});
}else if(this.state.vat === 'yes'){
    this.setState({print: !this.state.print,totalamt: totalnewi}); 
}else{
    this.setState({print: !this.state.print,totalamt: total1new}); 
}
}
};

async getdata() {
const res = await axios.get('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getcus')
const data = res.data

const options = data.map(d => ({
"value": d.id,
"label": d.customername,
"remark": d.remark,
"address": d.address,
"citys": d.c_state,
"cgstno": d.gstno
}))

this.setState({ selectOptions: options })

}

handleChange(e) {
this.setState({ customerid: e.value, name: e.label, remark: e.remark, address: e.address, nameerror:'',
citys: e.citys,cgstno: e.cgstno })
}

handleQuantiteChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { qty: parseInt(value, 10) });
this.setState({rowData: rowDataCopy});
}
handleselectprdtChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { itemname: value });
this.setState({
rowData: rowDataCopy
});
}
handlePrixChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { rate: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleWtChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { wt: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleUnitChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { unit: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handlePolishChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { polishcha: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleMakingchaChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { makingcha: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleDisChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { disamt: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}

valid(){
if(this.state.rowData.length===0){
    this.setState({nameerror:'Customer Name And Category Are Required'})
}else if (this.state.name.length===0) {
    this.setState({nameerror:'Customer Name And Category Are Required'})
}else{
    return true
}
}

componentDidMount() {
if(this.state.vat === 'yes'){
    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getsaledetailgst/' + this.props.data.editid).then((resp) => {
    resp.json().then((result) => {
        this.setState({ rowData: result })
    })
})
}else{
    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getsaledetail/' + this.props.data.editid).then((resp) => {
        resp.json().then((result) => {
            this.setState({ rowData: result })
        })
    }) 
}

fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getitem').then((resp) => {
resp.json().then((result) => {
this.setState({ itemlist: result })
})
})
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getstate').then((resp) => {
resp.json().then((result) => {
this.setState({ statelist: result })
})
})

if (this.state.vat === 'yes') {
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getidgst').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result })
})
})
} else {
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getid').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result })
})
})
}
this.getdata();
}
Print(){
    if(this.valid()){
        if(this.state.vat === 'yes'){
            this.props.data.SubmitGst(this.state)
        }else{
            this.props.data.Submit(this.state)
        }}
    this.setState({ print: false});
    window.print()
    this.componentDidMount();
}
  
render() {
const total1 = this.getTotalwithout() - (Number(this.state.amt) + Number(this.state.amt1));
const total1new = total1.toFixed(2);
const newtotal = total1new - this.state.amtnew;
const total =  this.getTotal() - (Number(this.state.amt) + Number(this.state.amt1));
const totalnew = total.toFixed(2);
const ntotal = totalnew - this.state.amtnew;
const totali =  this.getTotaligst() - (Number(this.state.amt) + Number(this.state.amt1));
const totalnewi = totali.toFixed(2);
const ntotali = totalnewi - this.state.amtnew;
const gettotal = this.getTotalwithout() - 0;
const getTotal1 = gettotal.toFixed(2);
const gettotal1 = this.getTotal() -0;
const getTotal2 = gettotal1.toFixed(2);
const getsgst = this.getsgst() -0;
const sgst1 = getsgst.toFixed(2);
const getcgst = this.getcgst() -0;
const cgst1 = getcgst.toFixed(2);
const getigst = this.getigst() -0;
const igst = getigst.toFixed(2);
const gettotaligst = this.getTotaligst() - 0;
const totaligst = gettotaligst.toFixed(2);
const getwt = this.getwt() - 0;
const totalwt = getwt.toFixed(2);

const mystyle = {color: "white",backgroundColor: "#0ABE95",padding: "10px",fontFamily: "Arial"};
const mystyle1 = {padding: "10px"};
const mystyle2 = {padding: "10px"};
const mystyle3 = {padding: "10px"};
const mystyle4 = {padding: "10px"};
const mystyle5 = {padding: "10px"};
const mystyle6 = {padding: "10px", width: "182px"};
const mystyle7 = {padding: "10px", width: "182px"};
const mystyle8 = {padding: "10px", width: "182px"};
const cstate = {padding: "5px", width: "182px"};
const mystyleitem = {padding: "10px", width: "80px"};
const btnstyle = {padding: "10px",};
const table = {minWidth: 340,};
const mystyleq = {padding: "10px", width: "30px"};
const wt = {padding: "10px", width: "40px"};
const rate = {padding: "10px", width: "40px"};
const discount = {padding: "10px", width: "40px"};
const mystyle80 = {width: "40px"};
const makingchange = {padding: "10px", width: "60px"};
const button = {width: 13, height: 13,padding: 0};
const removebutton = {width: 13, height: 13,padding: 0};
const mystypaytype = {padding: 0, width: "195px"}
const customStyles = {container: provided => ({...provided,width: "225px",paddingtop: "5px"})};
const addcus = {padding: "5px",}
const model = {position: 'fixed',width: '100 px',height: '100 px',};
const content = {backgroundColor: "DodgerBlue",position: 'absolute',top: '0px',left: '30px',width: '500px',padding: '20px',
};
const model1 = {position: 'fixed',width: '100 px',height: '100 px',};
const content1 = {backgroundColor: "DodgerBlue",position: 'absolute',top: '0px',left: '30px',width: '500px',padding: '20px',
};
const modelprint = {position: 'absoulute',};
const contentprint = {backgroundColor: "white",position: 'absolute',top: '60px',left: '10px',width: '970px',padding: '20px',
};
const tableCell = {paddingRight: 10,paddingLeft: 10,minWidth:5}
const tableCell1 = {paddingRight: 10,paddingLeft: 10,minWidth:5,border: '1px solid black'}

return (
<div>
<div>
{this.state.vat === 'yes' && this.state.print === false ?
<span class="no-printme"><Button onClick={() =>{ this.togglePrint()}} color="primary">Edit & Print</Button>
<Button onClick={()=>{this.props.data.CancelGst()}} color='secondary'>Cancel</Button></span>:
this.state.print === false ?
<span class="no-printme"><Button onClick={() =>{this.togglePrint()}} color="primary">Edit & Print</Button>
<Button onClick={()=>this.props.data.Cancel()} color='secondary'>Cancel</Button></span>:null}
</div>
{this.state.print ?
<PrintSale data={{Print:this.Print.bind(this),name:this.state.name,invoiceno:this.state.maxid,address:this.state.
address,contact:this.state.remark,vat:this.state.vat,rowData:this.state.rowData,amt:this.state.amt,totalamt:
this.state.totalamt,citys:this.state.citys,sgst:this.state.sgst,cgst:this.state.cgst,igst:this.state.igst,
gst:this.state.gst,maxid:this.state.maxid,editid:this.state.editid,cgstno:this.state.cgstno,amt1:this.state.amt1,
mode:this.state.mode,mode1:this.state.mode1}}/>
: <Grid item xs={12}><form autoComplete="off">
<div style={mystyle}>

<span>Bill No. :&nbsp;{this.props.data.editid} </span>
<Select styles={customStyles} options={this.state.selectOptions} onChange={this.handleChange.bind(this)}
placeholder="Select Customers..." defaultInputValue={this.props.data.customername}
theme={theme => ({
...theme,
borderRadius: 0,
colors: {
...theme.colors,
primary25: 'green',
neutral0: '#c8c8c8',
neutral90: 'white'
},
})} /><p style={{color:'red',fontSize:16,fontStyle:"oblique"}}>{this.state.nameerror}</p>
<TextField style={mystyle1} name="customerid" value={this.state.customerid} id="standard-basic" label="Customer Id"
onChange={(e) => { this.setState({ customerid: e.target.value }) }}/>
<TextField style={mystyle3} name="remark" value={this.state.remark} id="standard-basic" label="Remark"
onChange={(e) => { this.setState({ remark: e.target.value }) }} />
<TextField style={mystyle4} name="address" value={this.state.address} id="standard-basic" label="Address"
onChange={(e) => { this.setState({ address: e.target.value }) }} />
<TextField style={mystyle5} name="citys" id="standard-basic" label="State" value={this.state.citys}
onChange={(e) => { this.setState({ citys: e.target.value }) }} />
<TextField style={mystyle6}
id="datetime-local"
name="date"
label="Date"
type="date"

InputLabelProps={{
shrink: true,
}}
onChange={(e) => { this.setState({ date: e.target.value }) }}
/>
<TextField style={mystyle7}
id="date"
name="commitdate"
label="Commitment Dt."
type="date"
InputLabelProps={{
shrink: true,
}}
onChange={(e) => { this.setState({ commitdate: e.target.value }) }}
/>
<TextField style={mystyle8} name="vat" id="select" label="Gst Include" select
onChange={(e) => { this.setState({ vat: e.target.value }) }} defaultValue={this.state.vat}>
<MenuItem value="yes">Yes</MenuItem>
<MenuItem value="no">No</MenuItem>
</TextField>
<TextField style={mystyle8} name="paytype" id="select" label="Payment Type" select
value={this.state.paytype}
onChange={(e) => { this.setState({ paytype: e.target.value }) }}>
<MenuItem value="1">Cash</MenuItem>
<MenuItem value="2">Online</MenuItem>
</TextField>

<TableContainer component={Paper}>
<Table style={table} aria-label="spanning table">
<TableHead>

<TableRow>
<TableCell style={tableCell}><IconButton style={button} color="primary" variant="contained"
onClick={this.handleRowAdd}><AddIcon /></IconButton></TableCell>
<TableCell style={tableCell}>Item</TableCell>
<TableCell style={tableCell}>Qty.</TableCell>
<TableCell style={tableCell}>Wt.(gm.)</TableCell>
<TableCell style={tableCell}>Rate<br></br>(Per gm.)</TableCell>
<TableCell style={tableCell}>Making Charge</TableCell>
<TableCell style={tableCell}>Polish Cha(%)</TableCell>
<TableCell style={tableCell}>Amount</TableCell>
<TableCell style={tableCell}>Dis.Amt</TableCell>
{
this.state.vat === 'yes' && this.state.citys === "MADHYA PRADESH" ?
<TableCell style={tableCell}>SGST<br></br>CGST</TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}>IGST</TableCell>:''}
<TableCell style={tableCell}>Total</TableCell>
</TableRow>
</TableHead>

<TableBody>


{this.state.rowData.map((data, index) =>



<TableRow key={index} id={index}>
<TableCell style={tableCell}><IconButton style={removebutton} color="secondary" variant="contained"

onClick={(e) => this.handleRowDelete(index)} active><RemoveIcon /></IconButton></TableCell>
<TableCell style={tableCell}>

<TextField style={mystyleitem} name="itemname" id="select" label="Itemname" select
value={this.state.rowData.itemname} defaultValue={data.itemname}
onChange={(e) => this.handleselectprdtChange(index, e.target.value)}>
{
this.state.itemlist ?
    this.state.itemlist.map((item, i) =>
        <MenuItem key={item.id} value={item.itemname}>{item.itemname}</MenuItem>
    )
    :
    null
}
</TextField>
</TableCell>
<TableCell style={tableCell}><TextField style={mystyleq} name="qty" id="standard-basic" label="Qty." defaultValue={data.qty}
value={this.state.rowData.qty} onChange={(e) => this.handleQuantiteChange(index, e.target.value)} /></TableCell>
<TableCell style={tableCell}><TextField style={wt} name="wt" id="standard-basic" label="Weight" defaultValue={data.wt}
value={this.state.rowData.wt} onChange={(e) => this.handleWtChange(index, e.target.value)} /></TableCell>
<TableCell style={tableCell}><TextField style={rate} name="rate" id="standard-basic" label="Rate" defaultValue={data.rate}
value={this.state.rowData.rate} onChange={(e) => this.handlePrixChange(index, e.target.value)} /></TableCell>
<TableCell style={tableCell}>
<TableRow><TableCell style={tableCell}>
<TextField style={mystyle80} name="unit" id="select" select
value={this.state.rowData.unit} defaultValue={data.unit}
onChange={(e) => this.handleUnitChange(index, e.target.value)}>
<MenuItem value="1">Fix</MenuItem>
<MenuItem value="2">Per gm.</MenuItem>
<MenuItem value="3">Per pcs.</MenuItem>
<MenuItem value="4">None</MenuItem>
</TextField></TableCell>

<TableCell style={tableCell}>{
data.unit === '1' || data.unit === 1 ? <span>{(Number(data.makingcha)).toFixed(2)}</span>
: data.unit === '2' || data.unit === 2 ? <span>{(Number(data.wt || 0) * Number(data.makingcha || 0)).toFixed(2)}</span>
: data.unit === '3' || data.unit === 3 ? <span>{(Number(data.makingcha || 0) * Number(data.qty || 0)).toFixed(2)}</span>
: data.unit === '4' || data.unit === 4 ? <span>{(Number(data.makingcha || 0)).toFixed(2)}</span> : ''
}</TableCell>

</TableRow><TextField style={makingchange} name="makingcha" id="standard-basic" label="Making cha."
value={this.state.rowData.makingcha} defaultValue={data.makingcha}
onChange={(e) => this.handleMakingchaChange(index, e.target.value)} />
</TableCell>
<TableCell style={tableCell}><TextField style={mystyleitem} name="polishcha" id="select" select
value={this.state.rowData.polishcha} defaultValue={data.polishcha}
onChange={(e) => this.handlePolishChange(index, e.target.value)}>
<MenuItem value="5">5&nbsp;%</MenuItem>
<MenuItem value="10">10&nbsp;%</MenuItem>
<MenuItem value='0'>None</MenuItem>
</TextField></TableCell>
{data.polishcha ?
<TableCell style={tableCell}>{
    data.unit === '1' || data.unit === 1 ? <span>{(Number((Number((data.wt || 0)) * Number((data.rate || 0))) + Number((data.makingcha || 0))) 
    + Number((Number((data.wt * data.rate)) * Number(data.polishcha)) / 100)).toFixed(2)}</span>
    : data.unit === '2' || data.unit === 2 ? <span>{((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0))) 
    + (((data.wt * data.rate) * data.polishcha) / 100)).toFixed(2)}</span>
    : data.unit === '3' || data.unit === 3 ? <span>{((((data.wt || 0) * (data.rate || 0)) + ((data.makingcha || 0) * (data.qty || 0))) 
    + (((data.wt * data.rate) * data.polishcha) / 100)).toFixed(2)}</span>
    : data.unit === '4' || data.unit === 4 ? <span>{(Number((Number((data.wt || 0)) * Number((data.rate || 0))) + Number((data.makingcha || 0))) 
    + Number((Number((data.wt * data.rate)) * Number(data.polishcha)) / 100)).toFixed(2)}</span> : ''
}
</TableCell> :''}
<TableCell style={tableCell}><TextField style={discount} name="disamt" id="standard-basic" label="Dis."
value={this.state.rowData.disamt} defaultValue={data.disamt}
onChange={(e) => this.handleDisChange(index, e.target.value)} /></TableCell>

{this.state.vat === "yes" && this.state.citys === "MADHYA PRADESH" ?
<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{this.state.sgst}</span>&nbsp;
{
data.unit === '1' || data.unit === 1 ? <span>{(((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) 
+ Number(((data.wt * data.rate) * data.polishcha) / 100)) ) * Number(this.state.sgst) / 100).toFixed(2)}</span>
: data.unit === '2' || data.unit === 2 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) 
+ (((data.wt * data.rate) * data.polishcha) / 100))) * Number(this.state.sgst) / 100).toFixed(2)}</span>
: data.unit === '3' || data.unit === 3 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.makingcha || 0) * (data.qty || 0)) 
+ (((data.wt * data.rate) * data.polishcha) / 100))) * Number(this.state.sgst) / 100).toFixed(2)}</span>
: data.unit === '4' || data.unit === 4 ? <span>{(((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) 
+ Number(((data.wt * data.rate) * data.polishcha) / 100))) * Number(this.state.sgst) / 100).toFixed(2)}</span> : ''
}
<br></br>
<span style={{ backgroundColor: "red" }}>{this.state.cgst}</span>&nbsp;
{
data.unit === '1' || data.unit === 1 ? <span>{(((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) + Number(((data.wt * data.rate) * data.polishcha) / 100)) ) * Number(this.state.cgst) / 100).toFixed(2)}</span>
: data.unit === '2' || data.unit === 2 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) 
+ (((data.wt * data.rate) * data.polishcha) / 100))) * Number(this.state.cgst) / 100).toFixed(2)}</span>
: data.unit === '3' || data.unit === 3 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.makingcha || 0) * (data.qty || 0)) 
+ (((data.wt * data.rate) * data.polishcha) / 100))) * Number(this.state.cgst) / 100).toFixed(2)}</span>
: data.unit === '4' || data.unit === 4 ? <span>{(((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) 
+ Number(((data.wt * data.rate) * data.polishcha) / 100))) * Number(this.state.cgst) / 100).toFixed(2)}</span> : ''
}
</TableCell>:this.state.vat === 'yes' ?
<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{this.state.igst}</span>&nbsp;
{
data.unit === '1' || data.unit === 1 ? <span>{(((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) + Number(((data.wt * data.rate) * data.polishcha) / 100)) ) * Number(this.state.igst) / 100).toFixed(2)}</span>
: data.unit === '2' || data.unit === 2 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) 
+ (((data.wt * data.rate) * data.polishcha) / 100))) * Number(this.state.igst) / 100).toFixed(2)}</span>
: data.unit === '3' || data.unit === 3 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.makingcha || 0) * (data.qty || 0)) 
+ (((data.wt * data.rate) * data.polishcha) / 100))) * Number(this.state.igst) / 100).toFixed(2)}</span>
: data.unit === '4' || data.unit === 4 ? <span>{(((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) 
+ Number(((data.wt * data.rate) * data.polishcha) / 100))) * Number(this.state.igst) / 100).toFixed(2)}</span> : ''
}
</TableCell>:''}
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{
data.unit === '1' || data.unit === 1 ? <span>{(((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) + Number(((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) + Number(((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.gst) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: data.unit === '2' || data.unit === 2 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.gst) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: data.unit === '3' || data.unit === 3 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.qty || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((((data.wt || 0) * (data.rate || 0)) + ((data.qty || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.gst) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: data.unit === '4' || data.unit === 4 ? <span>{(((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) + Number(((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) + Number(((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.gst) / 100)- (data.disamt || 0)).toFixed(2)}</span> : ''
}
</TableCell>:this.state.vat === 'yes' ?
<TableCell style={tableCell}>{
data.unit === '1' || data.unit === 1 ? <span>{(((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) + Number(((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) + Number(((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.igst) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: data.unit === '2' || data.unit === 2 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.igst) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: data.unit === '3' || data.unit === 3 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.qty || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((((data.wt || 0) * (data.rate || 0)) + ((data.qty || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.igst) / 100)- (data.disamt || 0)).toFixed(2)}</span>
: data.unit === '4' || data.unit === 4 ? <span>{(((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) + Number(((data.wt * data.rate) * data.polishcha) / 100)) 
) + (((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) + Number(((data.wt * data.rate) * data.polishcha) / 100)) 
) * Number(this.state.igst) / 100)- (data.disamt || 0)).toFixed(2)}</span> : ''
}
</TableCell>:''}
{this.state.vat === 'no' ?
<TableCell style={tableCell}>{
data.unit === '1' || data.unit === 1 ? <span>{(((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) + Number(((data.wt * data.rate) * data.polishcha) / 100)) 
- (data.disamt || 0))).toFixed(2)}</span>
: data.unit === '2' || data.unit === 2? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.wt || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
- (data.disamt || 0))).toFixed(2)}</span>
: data.unit === '3' || data.unit === 3 ? <span>{(((((data.wt || 0) * (data.rate || 0)) + ((data.qty || 0) * (data.makingcha || 0)) + (((data.wt * data.rate) * data.polishcha) / 100)) 
- (data.disamt || 0))).toFixed(2)}</span>
: data.unit === '4' || data.unit === 4 ? <span>{(((Number((data.wt || 0) * (data.rate || 0)) + Number(data.makingcha || 0) + Number(((data.wt * data.rate) * data.polishcha) / 100)) 
- (data.disamt || 0))).toFixed(2)}</span> : ''
}
</TableCell>
: ''
}
</TableRow>)}
<TableRow>
<TableCell rowSpan={12} />
<TableCell style={tableCell}>Subtotal</TableCell>
<TableCell style={tableCell}>{this.getqty()}</TableCell>
<TableCell style={tableCell}>{totalwt}</TableCell>
<TableCell></TableCell>
<TableCell style={tableCell}>{this.state.vat === 'yes' ?
<TextField style={mystyleitem} name="due" id="standard-basic" label="Due"
     value={ntotal} />:
     <TextField style={mystyleitem} name="due" id="standard-basic" label="Due"
     value={newtotal} />}
</TableCell>{
this.state.vat == 'yes' ?
<TableCell style={tableCell}><TextField style={mystyleitem} name="amt" id="standard-basic" label="Paid."
value={this.state.amt} onChange={this.myChangeHandler} /><br></br>
<TextField style={mystyleitem} name="amt1" id="standard-basic" label="Paid."
value={this.state.amt1} onChange={this.myChangeHandler} /></TableCell> :''
}
<TableCell><TextField style={mystyleitem} name="premark" id="standard-basic" label="Remark"
value={this.state.premarkcash} onChange={this.myChangeHandler} /><br></br>
<TextField style={mystyleitem} name="premark1" id="standard-basic" label="Remark"
value={this.state.premarkonline} onChange={this.myChangeHandler} /></TableCell>
<TableCell><TextField name="mode" label='Mode' value={this.state.mode}
 style={mystyleitem}>
</TextField><br></br>
<TextField name="mode1" label='Mode' value={this.state.mode1}
 style={mystyleitem}>
</TextField></TableCell>
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{sgst1}<br></br>{cgst1}</TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}>{igst}</TableCell>
:<TableCell style={tableCell}><TextField style={mystyleitem} name="amt" id="standard-basic" label="Paid."
value={this.state.amt} onChange={this.myChangeHandler} /><br></br>
<TextField style={mystyleitem} name="amt1" id="standard-basic" label="Paid."
value={this.state.amt1} onChange={this.myChangeHandler} /></TableCell>}
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TableCell style={tableCell}><span>{getTotal2}</span></TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}><span>{totaligst}</span></TableCell>
:<TableCell style={tableCell}><span>{getTotal1}</span></TableCell>}
</TableRow>
</TableBody>
</Table>
</TableContainer>
</div>
</form></Grid>}
</div >
)

}

getTotal() {
let grandTotal = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === '1' || row.unit === 1 ? (Number((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    - (row.disamt || 0)) + Number(((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    - (row.disamt || 0)) * (this.state.gst) / 100))
    : row.unit === '2' || row.unit === 2 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    - (row.disamt || 0)) + (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    - (row.disamt || 0)) * (this.state.gst)  / 100))
    : row.unit === '3' || row.unit === 3 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.qty || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    - (row.disamt || 0)) + (((((row.wt || 0) * (row.rate || 0)) + ((row.qty || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    - (row.disamt || 0)) * (this.state.gst)  / 100))
    : row.unit === '4' || row.unit === 4 ? (Number((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    - (row.disamt || 0)) + Number(((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    - (row.disamt || 0)) * (this.state.gst)  / 100)) : ''
}
);
if (rowTotals.length > 0) {
grandTotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandTotal;
}

getTotaligst() {
let grandTotal = 0;
const rowTotals = this.state.rowData.map(
row => {
    return row.unit === '1' || row.unit === 1 ? (Number((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    ) + Number(((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.igst) / 100)- (row.disamt || 0))
    : row.unit === '2' || row.unit === 2 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) + (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.igst)  / 100)- (row.disamt || 0))
    : row.unit === '3' || row.unit === 3 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.qty || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) + (((((row.wt || 0) * (row.rate || 0)) + ((row.qty || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.igst)  / 100)- (row.disamt || 0))
    : row.unit === '4' || row.unit === 4 ? (Number((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    ) + Number(((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.igst)  / 100)- (row.disamt || 0)) : ''
}
);
if (rowTotals.length > 0) {
grandTotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandTotal;
}

getTotalwithout() {
let grandTotal = 0;
const rowTotals = this.state.rowData.map(


row => {
return row.unit === '1' || row.unit === 1 ? (((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    - (row.disamt || 0)))
    : row.unit === '2' || row.unit === 2 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    - (row.disamt || 0)))
    : row.unit === '3' || row.unit === 3 ? (((((row.wt || 0) * (row.rate || 0)) + ((row.qty || 0) * (row.makingcha || 0)) + (((row.wt * row.rate) * row.polishcha) / 100)) 
    - (row.disamt || 0)))
    : row.unit === '4' || row.unit === 4 ? (((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    - (row.disamt || 0))) : ''
}
);

if (rowTotals.length > 0) {
grandTotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandTotal;
}

getsgst() {
let grandsgst = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === '1' ? (((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * Number(this.state.sgst) / 100)
    : row.unit === '2' ? (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * Number(this.state.sgst) / 100)
    : row.unit === '3' ? (((((row.wt || 0) * (row.rate || 0)) + ((row.makingcha || 0) * (row.qty || 0)) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * Number(this.state.sgst) / 100)
    : row.unit === '4' ? (((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) 
    + Number(((row.wt * row.rate) * row.polishcha) / 100)) ) * Number(this.state.sgst) / 100) : ''
}
);
if (rowTotals.length > 0) {
grandsgst = rowTotals.reduce((acc, val) => acc + val);
}
return grandsgst;
}

getcgst() {
let grandcgst = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === '1' ? (((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * Number(this.state.cgst) / 100)
    : row.unit === '2' ? (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * Number(this.state.cgst) / 100)
    : row.unit === '3' ? (((((row.wt || 0) * (row.rate || 0)) + ((row.makingcha || 0) * (row.qty || 0)) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * Number(this.state.cgst) / 100)
    : row.unit === '4' ? (((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) 
    + Number(((row.wt * row.rate) * row.polishcha) / 100)) ) * Number(this.state.cgst) / 100) : ''
}
);
if (rowTotals.length > 0) {
grandcgst = rowTotals.reduce((acc, val) => acc + val);
}
return grandcgst;
}

getigst() {
let grandigst = 0;
const rowTotals = this.state.rowData.map(
row => {
return row.unit === '1' ? (((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) + Number(((row.wt * row.rate) * row.polishcha) / 100)) 
    ) * (this.state.igst) / 100)
    : row.unit === '2' ? (((((row.wt || 0) * (row.rate || 0)) + ((row.wt || 0) * (row.makingcha || 0)) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * (this.state.igst) / 100)
    : row.unit === '3' ? (((((row.wt || 0) * (row.rate || 0)) + ((row.makingcha || 0) * (row.qty || 0)) 
    + (((row.wt * row.rate) * row.polishcha) / 100)) ) * (this.state.igst) / 100)
    : row.unit === '4' ? (((Number((row.wt || 0) * (row.rate || 0)) + Number(row.makingcha || 0) 
    + Number(((row.wt * row.rate) * row.polishcha) / 100)) ) * (this.state.igst) / 100) : ''
}
);
if (rowTotals.length > 0) {
grandigst = rowTotals.reduce((acc, val) => acc + val);
}
return grandigst;
}

getqty() {
let grandqty = 0;
const rowTotals = this.state.rowData.map(
row =>
Number(row.qty) || 0

);

if (rowTotals.length > 0) {
grandqty = rowTotals.reduce((acc, val) => acc + val);
}
return grandqty;
}

getwt() {
let grandwt = 0;
const rowTotals = this.state.rowData.map(
row =>
Number(row.wt) || 0

);

if (rowTotals.length > 0) {
grandwt = rowTotals.reduce((acc, val) => acc + val);
}
return grandwt;
}



handleRowDelete(row) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy.splice(row, 1);
this.setState({
rowData: rowDataCopy
});
}
handleRowAdd() {
let id = this.state.id;
id = id++;
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy.push({
itemname: "",
qty: 0,
wt: 0,
rate: 0,
unit: "",
polishcha: 0,
makingcha: 0,
disamt: 0

});
this.setState({
rowData: rowDataCopy,
id: id
});
}
}
export default withRouter(Forms);

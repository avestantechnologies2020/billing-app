import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Remove';
import Select from 'react-select'
import axios from 'axios'
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import InvoicePrint from "views/Invoice/InvoicePrint";

class Forms extends React.Component {

constructor(props) {
super(props)
var today = new Date(),
date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() ;
var today = new Date(),
time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
var igst = this.props.data.igst;
var sgst = this.props.data.sgst;
var cgst = this.props.data.cgst;
var gst = Number(cgst) + Number(sgst);
var amount = this.props.data.amount;
var tax = this.props.data.vat;
var c_id = this.props.data.c_id;
var remark = this.props.data.remark;
var address = this.props.data.address;
var state = this.props.data.state;
var cdate = this.props.data.cdate;
var commitdate = this.props.data.commitdate;
var paytype  = this.props.data.paytype;
var billno = this.props.data.editid;
var cname = this.props.data.customername;
var due = this.props.data.due;
var total = this.props.data.total;
this.state = {
billno: billno, customerid: c_id, customername: "", remark: remark, address: address, refno: "", date: cdate,
commitdate: commitdate, vat: tax, paytype: paytype, itemlist: "", rowData: [], itemid: "", id: 0, selectOptions: [], id: "",
name: cname, seen: false, seeni: false, price: '', print: false, billdata: "", totalamt:total, amt: amount, due: due,
currentdate : date, currenttime : time, nameerror:'', citys:state, statelist:'', gst:gst, sgst:sgst, cgst:cgst, igst:igst,
editid:this.props.data.editid,cgstno:this.props.data.cgstno,
};
this.getdata();
this.handleQuantiteChange = this.handleQuantiteChange.bind(this);
this.handlePrixChange = this.handlePrixChange.bind(this);
this.handleselectprdtChange = this.handleselectprdtChange.bind(this);
this.handleRowDelete = this.handleRowDelete.bind(this);
this.handleRowAdd = this.handleRowAdd.bind(this);
this.handleMakingchaChange = this.handleMakingchaChange.bind(this);
this.getTotal = this.getTotal.bind(this);
}

togglePrint(){
if(this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH'){
    const total =  this.getTotal() - 0;
    const totalnew = total.toFixed(2);
    const totalgram = this.getTotalGram()-0;
    const gm = totalgram.toFixed(2);
    this.setState({totalamt: totalnew});
    this.setState({gm:gm});
    }else if(this.state.vat === 'yes'){
    const total =  this.getTotaligst() - 0;
    const totalnew = total.toFixed(2);
    const totalgram = this.getTotalGram()-0;
    const gm = totalgram.toFixed(2);
    this.setState({totalamt: totalnew});
    this.setState({gm:gm});
    }else{
    const total1 = this.getTotalwithout() - 0;
    const total1new = total1.toFixed(2);
    const totalgram = this.getTotalGram()-0;
    const gm = totalgram.toFixed(2);
    this.setState({totalamt: total1new});
    this.setState({gm:gm});
    }
this.setState({
print: !this.state.print
});
}

async getdata() {
    const res = await axios.get('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getsup')
    const data = res.data
    const options = data.map(d => ({
    "value": d.id,
    "label": d.supplier_name,
    "contact": d.contact_no,
    "address": d.address,
    "citys": d.c_state,
    "cgstno": d.gstno
    }))
    this.setState({ selectOptions: options })
    }


handleChange(e) {
this.setState({ customerid: e.value, name: e.label, remark: e.contact, address: e.address, nameerror:'',
citys: e.citys,cgstno:e.cgstno })
}

handleQuantiteChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { qty: parseInt(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleselectprdtChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { itemname: value });
this.setState({
rowData: rowDataCopy
});
}
handlePrixChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { rate: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleWtChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { wt: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleUnitChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { unit: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handlePolishChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { polishcha: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleMakingchaChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { makingcha: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleDisChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { disamt: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}

valid(){
if(this.state.name.length === 0){
    this.setState({nameerror:'Supplier Name required'})
}else{
    return true
}
}

componentDidMount() {

    fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getinvdetails/' + this.props.data.editid).then((resp) => {
        resp.json().then((result) => {
            this.setState({ rowData: result })
        })
    }) 


fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getitem').then((resp) => {
resp.json().then((result) => {
this.setState({ itemlist: result })
})
})
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getstate').then((resp) => {
resp.json().then((result) => {
this.setState({ statelist: result })
})
})
this.getdata();
}

Print(){
    if(this.valid()){
        if(this.state.vat === 'yes'){
            this.props.data.SubmitGst(this.state)
        }else{
            this.props.data.Submit(this.state)
        }}
    this.setState({rowData: [],supplierid:'',suppliername:'',contact:'',
    address:'',paytype:'',amt: '',citys:'',vat:'',print: false,});
    window.print()
    this.componentDidMount();
}
  
render() {
    const total1 = this.getTotalwithout() - this.state.amt;
    const total1new = total1.toFixed(2);
    const total =  this.getTotal() - this.state.amt;
    const totalnew = total.toFixed(2);
    const totalamt = this.getTotalwithout()-0;
    const amt = totalamt.toFixed(2);
    const totalgram = this.getTotalGram()-0;
    const gm = totalgram.toFixed(2);
    const weight = this.getwt()-0;
    const wt1 = weight.toFixed(2);
    const gettotal1 = this.getTotal() -0;
    const getTotal2 = gettotal1.toFixed(2);
    const getsgst = this.getsgst() -0;
    const sgst1 = getsgst.toFixed(2);
    const getcgst = this.getcgst() -0;
    const cgst1 = getcgst.toFixed(2);
    const getigst = this.getigst() -0;
    const igst = getigst.toFixed(2);
    const gettotaligst = this.getTotaligst() - 0;
    const totaligst = gettotaligst.toFixed(2);
    
    const mystyle = {color: "white",backgroundColor: "#0ABE95",padding: "10px",fontFamily: "Arial"};
    const mystyle1 = {padding: "10px"};
    const mystyle2 = {padding: "10px"};
    const mystyle3 = {padding: "10px"};
    const mystyle4 = {padding: "10px"};
    const mystyle5 = {padding: "10px"};
    const mystyle6 = {padding: "10px", width: "182px"};
    const mystyle7 = {padding: "10px", width: "182px"};
    const mystyle8 = {padding: "10px", width: "182px"};
    const mystyleitem = {padding: "10px", width: "60px"};
    const btnstyle = {padding: "10px",};
    const table = {minWidth: 700};
    const mystyleq = {padding: "10px", width: "30px"};
    const wt = {padding: "10px", width: "40px"};
    const rate = {padding: "10px", width: "40px"};
    const discount = {padding: "10px", width: "40px"};
    const mystyle80 = {width: "40px"};
    const makingchange = {padding: "10px", width: "60px"};
    const button = {width: 13, height: 13,padding: 0};
    const removebutton = {width: 13, height: 13,padding: 0};
    const mystypaytype = {padding: 0, width: "195px"}
    const customStyles = {
    container: provided => ({...provided,width: "225px",paddingtop: "5px"})};
    const addcus = {padding: "5px",}
    const model = {position: 'fixed',width: '100 px',height: '100 px',};
    const content = {backgroundColor: "DodgerBlue",position: 'absolute',top: '0px',left: '30px',width: '500px',
    padding: '20px',};
    const model1 = {position: 'fixed',width: '100 px',height: '100 px'};
    const content1 = {backgroundColor: "DodgerBlue",position: 'absolute',top: '0px',left: '30px',width: '500px',
    padding: '20px',};
    const modelprint = {position: 'absolute',};
    const contentprint = {backgroundColor: "white",position: 'absolute',top: '-30px',left: '-5px',width: '970px',padding: '20px',
};
    const cstate = { padding: "5px", width: "182px"};
    const tableCell = {paddingRight: 10,paddingLeft: 10,minWidth:5}

return (
<div>
<div>
{this.state.vat === 'yes' && this.state.print === false ?
<span><Button onClick={() =>{ this.togglePrint()}} color="primary">Edit & Print</Button>
<Button onClick={()=>{this.props.data.CancelGst()}} color='secondary'>Cancel</Button></span>:
this.state.print === false ?
<span><Button onClick={() =>{this.togglePrint()}} color="primary">Edit & Print</Button>
<Button onClick={()=>this.props.data.Cancel()} color='secondary'>Cancel</Button></span>:null}
</div>
{this.state.print ?

<InvoicePrint data={{Print:this.Print.bind(this),name:this.state.name,invoiceno:this.state.maxid,address:this.state.
address,contact:this.state.remark,vat:this.state.vat,rowData:this.state.rowData,totalamt:this.state.totalamt,
citys:this.state.citys,sgst:this.state.sgst,cgst:this.state.cgst,igst:this.state.igst,gst:this.state.gst,
gm:this.state.gm,editid:this.state.editid,cgstno:this.state.cgstno}}/>

: <Grid item xs={12}><form autoComplete="off">
<div style={mystyle}>

<span>Bill No. :&nbsp;{this.props.data.editid} </span>
<Select styles={customStyles} options={this.state.selectOptions} onChange={this.handleChange.bind(this)}
placeholder="Select Supplier..." defaultInputValue={this.props.data.customername}
theme={theme => ({...theme,borderRadius: 0,colors: {...theme.colors,primary25: 'green',neutral0: '#c8c8c8',neutral90: 'white'
},
})} /><p style={{color:'red',fontSize:16,fontStyle:"oblique"}}>{this.state.nameerror}</p>
<TextField style={mystyle1} name="customerid" value={this.state.customerid} id="standard-basic" label="Supplier Id"
onChange={(e) => { this.setState({ customerid: e.target.value }) }}/>
<TextField style={mystyle3} name="remark" value={this.state.remark} id="standard-basic" label="Remark"
onChange={(e) => { this.setState({ remark: e.target.value }) }} />
<TextField style={mystyle4} name="address" value={this.state.address} id="standard-basic" label="Address"
onChange={(e) => { this.setState({ address: e.target.value }) }} />
<TextField style={mystyle5} name="citys" id="standard-basic" label="State" value={this.state.citys}
onChange={(e) => { this.setState({ citys: e.target.value }) }} />
<TextField style={mystyle6}
id="datetime-local"
name="date"
label="Date"
type="date"

InputLabelProps={{
shrink: true,
}}
onChange={(e) => { this.setState({ date: e.target.value }) }}
/>
<TextField style={mystyle7}
id="date"
name="commitdate"
label="Commitment Dt."
type="date"
InputLabelProps={{
shrink: true,
}}
onChange={(e) => { this.setState({ commitdate: e.target.value }) }}
/>
<TextField style={mystyle8} name="paytype" id="select" label="Payment Type" select
value={this.state.paytype}
onChange={(e) => { this.setState({ paytype: e.target.value }) }}>
<MenuItem value="1">Cash</MenuItem>
<MenuItem value="2">Online</MenuItem>
</TextField>

<TableContainer component={Paper}>
<Table style={table} aria-label="spanning table">
<TableHead>

<TableRow>
<TableCell style={tableCell}><IconButton style={button} color="primary" variant="contained"
onClick={this.handleRowAdd}><AddIcon /></IconButton></TableCell>
<TableCell style={tableCell}>Item</TableCell>
<TableCell style={tableCell}>Qty.</TableCell>
<TableCell style={tableCell}>Wt.(gm.)</TableCell>
<TableCell style={tableCell}>Purity</TableCell>
<TableCell style={tableCell}>Waste</TableCell>
<TableCell style={tableCell}>Rate<br></br>(Per gm.)</TableCell>
<TableCell style={tableCell}>Making Charge</TableCell>
{
this.state.vat === 'yes' && this.state.citys === "MADHYA PRADESH" ?
<TableCell style={tableCell}>SGST<br></br>CGST</TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}>IGST</TableCell>:''}
<TableCell style={tableCell}>Amount/Fine</TableCell>
</TableRow>
</TableHead>

<TableBody>


{this.state.rowData.map((data, index) =>



<TableRow key={index} id={index}>
<TableCell style={tableCell}><IconButton style={removebutton} color="secondary" variant="contained"

onClick={(e) => this.handleRowDelete(index)} active><RemoveIcon /></IconButton></TableCell>
<TableCell style={tableCell}>

<TextField style={mystyleitem} name="itemname" id="select" label="Itemname" select
value={this.state.rowData.itemname} defaultValue={data.itemname}
onChange={(e) => this.handleselectprdtChange(index, e.target.value)}>
{
this.state.itemlist ?
    this.state.itemlist.map((item, i) =>
        <MenuItem key={item.id} value={item.itemname}>{item.itemname}</MenuItem>
    )
    :
    null
}
</TextField>
</TableCell>
<TableCell style={tableCell}><TextField style={mystyleq} name="qty" id="standard-basic" label="Qty." defaultValue={data.qty}
value={this.state.rowData.qty} onChange={(e) => this.handleQuantiteChange(index, e.target.value)} /></TableCell>
<TableCell style={tableCell}><TextField style={wt} name="wt" id="standard-basic" label="Weight" defaultValue={data.wt}
value={this.state.rowData.wt} onChange={(e) => this.handleWtChange(index, e.target.value)} /></TableCell>
<TableCell><TextField style={wt} name="purity" id="standard-basic" label="Purity" defaultValue={data.purity}
value={this.state.rowData.purity} onChange={(e) => this.handlePurityChange(index, e.target.value)} /></TableCell>
<TableCell><TextField style={wt} name="waste" id="standard-basic" label="Waste" defaultValue={data.waste}
value={this.state.rowData.waste} onChange={(e) => this.handleWasteChange(index, e.target.value)} /></TableCell>
<TableCell style={tableCell}><TextField style={rate} name="rate" id="standard-basic" label="Rate" defaultValue={data.rate}
value={this.state.rowData.rate} onChange={(e) => this.handlePrixChange(index, e.target.value)} /></TableCell>
<TableCell style={tableCell}>
<TableRow><TableCell style={tableCell}>
<TextField style={mystyle80} name="unit" id="select" select
value={this.state.rowData.unit} defaultValue={data.unit}
onChange={(e) => this.handleUnitChange(index, e.target.value)}>
<MenuItem value="1">Fix</MenuItem>
<MenuItem value="2">Per gm.</MenuItem>
<MenuItem value="3">Per pcs.</MenuItem>
<MenuItem value="4">None</MenuItem>
</TextField></TableCell>

<TableCell style={tableCell}>{
data.unit === '1' || data.unit === 1 ? <span>{(Number(data.makingcha)).toFixed(2)}</span>
: data.unit === '2' || data.unit === 2 ? <span>{(Number(data.wt || 0) * Number(data.makingcha || 0)).toFixed(2)}</span>
: data.unit === '3' || data.unit === 3 ? <span>{(Number(data.makingcha || 0) * Number(data.qty || 0)).toFixed(2)}</span>
: data.unit === '4' || data.unit === 4 ? <span>{(Number(data.makingcha || 0)).toFixed(2)}</span> : ''
}</TableCell>

</TableRow><TextField style={makingchange} name="makingcha" id="standard-basic" label="Making cha."
value={this.state.rowData.makingcha} defaultValue={data.makingcha}
onChange={(e) => this.handleMakingchaChange(index, e.target.value)} />
</TableCell>

{this.state.vat === "yes" && this.state.citys === "MADHYA PRADESH" ?
<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{this.state.sgst}</span>&nbsp;
{
data.unit === 1|| data.unit === '1' ? <span>{((((Number((data.wt * data.purity) / 100) + Number((data.wt * data.waste) / 100)) * (data.rate))
    +Number(data.makingcha)) * Number(this.state.sgst) / 100).toFixed(2)}</span>
    :data.unit === 2|| data.unit === '2' ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) *(data.rate))
    +(data.wt * data.makingcha)) * Number(this.state.sgst) / 100).toFixed(2)}</span>:
    data.unit === 3|| data.unit === '3' ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))* (data.rate))
    +(data.qty * data.makingcha)) * Number(this.state.sgst) / 100).toFixed(2)}</span>:
    data.unit === 4|| data.unit === '4' ? <span>{((((Number((data.wt * data.purity) / 100) + Number((data.wt * data.waste) / 100)) * (data.rate))
    +Number(data.makingcha)) * Number(this.state.sgst) / 100).toFixed(2)}</span>:''
}
<br></br>
<span style={{ backgroundColor: "red" }}>{this.state.cgst}</span>&nbsp;
{
data.unit === 1|| data.unit === '1' ? <span>{((((Number((data.wt * data.purity) / 100) + Number((data.wt * data.waste) / 100)) * (data.rate))
    +Number(data.makingcha)) * Number(this.state.cgst) / 100).toFixed(2)}</span>
    :data.unit === 2|| data.unit === '2' ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) *(data.rate))
    +(data.wt * data.makingcha)) * Number(this.state.cgst) / 100).toFixed(2)}</span>:
    data.unit === 3|| data.unit === '3' ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))* (data.rate))
    +(data.qty * data.makingcha)) * Number(this.state.cgst) / 100).toFixed(2)}</span>:
    data.unit === 4|| data.unit === '4' ? <span>{((((Number((data.wt * data.purity) / 100) + Number((data.wt * data.waste) / 100)) * (data.rate))
    +Number(data.makingcha)) * Number(this.state.cgst) / 100).toFixed(2)}</span>:''
}
</TableCell>:this.state.vat === 'yes' ?
<TableCell style={tableCell}><span style={{ backgroundColor: "red" }}>{this.state.igst}</span>&nbsp;
{
data.unit === 1 || data.unit === '1' ? <span>{((((Number((data.wt * data.purity) / 100) + Number((data.wt * data.waste) / 100)) * (data.rate))
    +Number(data.makingcha)) * Number(this.state.igst) / 100).toFixed(2)}</span>
    :data.unit === 2 || data.unit === '2' ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) *(data.rate))
    +(data.wt * data.makingcha)) * Number(this.state.igst) / 100).toFixed(2)}</span>:
    data.unit === 3 || data.unit === '3' ? <span>{((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))* (data.rate))
    +(data.qty * data.makingcha)) * Number(this.state.igst) / 100).toFixed(2)}</span>:
    data.unit === 4 || data.unit === '4' ? <span>{((((Number((data.wt * data.purity) / 100) + Number((data.wt * data.waste) / 100)) * (data.rate))
    +Number(data.makingcha)) * Number(this.state.igst) / 100).toFixed(2)}</span>:''
}
</TableCell>:''}
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{
data.rate === '0' ?
<span>{(((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:data.unit === 1 || data.unit === '1' ? <span>{(((((Number((data.wt * data.purity) / 100) + Number((data.wt * data.waste) / 100)) * (data.rate))
+Number(data.makingcha)) * Number(this.state.gst) / 100) + Number(((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100)) * (data.rate)) + (data.makingcha))).toFixed(2)}</span>
:data.unit === 2 || data.unit === '2' ? <span>{(((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) *(data.rate))
+(data.wt * data.makingcha)) * Number(this.state.gst) / 100)+(((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100)) *(data.rate)) + (data.wt * data.makingcha))).toFixed(2)}</span>:
data.unit === 3 || data.unit === '3' ? <span>{(((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))* (data.rate))
+(data.qty * data.makingcha))* Number(this.state.gst) /100) + (((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100))* (data.rate)) + (data.qty * data.makingcha))).toFixed(2)}</span>:
data.unit === 4 || data.unit === '4' ? <span>{(((((Number((data.wt * data.purity) / 100) + Number((data.wt * data.waste) / 100)) * (data.rate))
+Number(data.makingcha))* Number(this.state.gst) / 100) + Number(((((data.wt * data.purity) / 100) 
+ Number((data.wt * data.waste) / 100)) * (data.rate)) + Number(data.makingcha))).toFixed(2)}</span>:''
}
</TableCell>:this.state.vat === 'yes' ?
<TableCell style={tableCell}>{
data.rate === '0' ?
<span>{(((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:data.unit === 1 || data.unit === '1' ? <span>{(((((Number((data.wt * data.purity) / 100) + Number((data.wt * data.waste) / 100)) * (data.rate))
+Number(data.makingcha)) * Number(this.state.igst) / 100) + Number(((((data.wt * data.purity) / 100) 
+ Number((data.wt * data.waste) / 100)) * (data.rate)) + Number(data.makingcha))).toFixed(2)}</span>
:data.unit === 2 || data.unit === '2' ? <span>{(((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) *(data.rate))
+(data.wt * data.makingcha)) * Number(this.state.igst) / 100)+(((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100)) *(data.rate)) + (data.wt * data.makingcha))).toFixed(2)}</span>:
data.unit === 3 || data.unit === '3' ? <span>{(((((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))* (data.rate))
+(data.qty * data.makingcha))* Number(this.state.igst) /100) + (((((data.wt * data.purity) / 100) 
+ ((data.wt * data.waste) / 100))* (data.rate)) + (data.qty * data.makingcha))).toFixed(2)}</span>:
data.unit === 4 || data.unit === '4' ? <span>{(((((Number((data.wt * data.purity) / 100) + Number((data.wt * data.waste) / 100)) * (data.rate))
+Number(data.makingcha))* Number(this.state.igst) / 100) + Number(((((data.wt * data.purity) / 100) 
+ Number((data.wt * data.waste) / 100)) * (data.rate)) + Number(data.makingcha))).toFixed(2)}</span>:''
}
</TableCell>:''}
{this.state.vat === 'no' ?
<TableCell style={tableCell}>{
data.rate === '0' ?
<span>{(((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:data.unit === '1' || data.unit === 1 ? <span>{(((Number((data.wt * data.purity) / 100) + Number((data.wt * data.waste) / 100)) * (data.rate))
+Number(data.makingcha)).toFixed(2)}</span>
:data.unit === '2' || data.unit === 2 ? <span>{(((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)) *(data.rate))
+(data.wt * data.makingcha)).toFixed(2)}</span>:
data.unit === '3' || data.unit === 3 ? <span>{(((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))* (data.rate))
+(data.qty * data.makingcha)).toFixed(2)}</span>:
data.unit === '4' || data.unit === 4 ? <span>{(((Number((data.wt * data.purity) / 100) + Number((data.wt * data.waste) / 100)) * (data.rate))
+Number(data.makingcha)).toFixed(2)}</span>:''
}
</TableCell>
: ''
}
</TableRow>)}
<TableRow>
<TableCell rowSpan={12} />
<TableCell>Subtotal</TableCell>
<TableCell>{this.getqty()}</TableCell>
<TableCell>{wt1}</TableCell>
<TableCell></TableCell>
<TableCell></TableCell>
<TableCell style={tableCell}>
</TableCell>{
this.state.vat == 'yes' ?
<TableCell style={tableCell}></TableCell> :''
}
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TableCell style={tableCell}>{sgst1}<br></br>{cgst1}</TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}>{igst}</TableCell>
:<TableCell style={tableCell}></TableCell>}
{this.state.vat === 'yes' && this.state.citys === 'MADHYA PRADESH' ?
<TableCell style={tableCell}><span>{getTotal2}</span></TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}><span>{totaligst}</span></TableCell>
:<TableCell><span>{amt}<br></br>{gm}&nbsp;gm</span></TableCell>}
</TableRow>
</TableBody>
</Table>
</TableContainer>
</div>
</form></Grid>}
</div >
)

}

getTotal() {
    let grandTotal = 0;
    const rowTotals = this.state.rowData.map(
    row => {
    return row.unit === 1 || row.unit === '1' ? (((((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100)) * (row.rate))
        +Number(row.makingcha)) * Number(this.state.gst) / 100) + Number(((((row.wt * row.purity) / 100) 
        + Number((row.wt * row.waste) / 100)) * (row.rate)) + Number(row.makingcha)))
        :row.unit === 2 || row.unit === '2' ? (((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) *(row.rate))
        +(row.wt * row.makingcha)) * Number(this.state.gst) / 100)+(((((row.wt * row.purity) / 100) 
        + ((row.wt * row.waste) / 100)) *(row.rate)) + (row.wt * row.makingcha))):
        row.unit === 3 || row.unit === '3' ? (((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100))* (row.rate))
        +(row.qty * row.makingcha))* Number(this.state.gst) /100) + (((((row.wt * row.purity) / 100) 
        + ((row.wt * row.waste) / 100))* (row.rate)) + (row.qty * row.makingcha))):
        row.unit === 4 || row.unit === '4' ? (((((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100)) * (row.rate))
        +Number(row.makingcha))* Number(this.state.gst) / 100) + Number(((((row.wt * row.purity) / 100) 
        + Number((row.wt * row.waste) / 100)) * (row.rate)) + Number(row.makingcha))):''
    });
    if (rowTotals.length > 0) {
    grandTotal = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandTotal;
    }
        
    getTotaligst() {
    let grandTotal = 0;
    const rowTotals = this.state.rowData.map(
    row => {
    return row.unit === 1 || row.unit === '1' ? (((((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100)) * (row.rate))
        +Number(row.makingcha)) * Number(this.state.igst) / 100) + Number(((((row.wt * row.purity) / 100) 
        + Number((row.wt * row.waste) / 100)) * (row.rate)) + Number(row.makingcha)))
        :row.unit === 2 || row.unit === '2' ? (((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) *(row.rate))
        +(row.wt * row.makingcha)) * Number(this.state.igst) / 100)+(((((row.wt * row.purity) / 100) 
        + ((row.wt * row.waste) / 100)) *(row.rate)) + (row.wt * row.makingcha))):
        row.unit === 3 || row.unit === '3' ? (((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100))* (row.rate))
        +(row.qty * row.makingcha))* Number(this.state.igst) /100) + (((((row.wt * row.purity) / 100) 
        + ((row.wt * row.waste) / 100))* (row.rate)) + (row.qty * row.makingcha))):
        row.unit === 4 || row.unit === '4' ? (((((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100)) * (row.rate))
        +Number(row.makingcha))* Number(this.state.igst) / 100) + Number(((((row.wt * row.purity) / 100) 
        + Number((row.wt * row.waste) / 100)) * (row.rate)) + Number(row.makingcha))):''
    });
    if (rowTotals.length > 0) {
    grandTotal = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandTotal;
    }
    getTotalwithout() {
    let grandTotal = 0;
    const rowTotals = this.state.rowData.map(
    row => {
    return row.unit === '1'|| row.unit === 1 ? (((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100)) * (row.rate))
        +Number(row.makingcha))
        :row.unit === '2'|| row.unit === 2 ? (((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) *(row.rate))
        +(row.wt * row.makingcha)):
        row.unit === '3'|| row.unit === 3 ? (((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100))* (row.rate))
        +(row.qty * row.makingcha)):
        row.unit === '4'|| row.unit === 4 ? (((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100)) * (row.rate))
        +Number(row.makingcha)):''
    });
    if (rowTotals.length > 0) {
    grandTotal = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandTotal;
    }
    getsgst() {
    let grandsgst = 0;
    const rowTotals = this.state.rowData.map(
    row => {
    return row.unit === 1 || row.unit === '1' ? ((((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100)) * (row.rate))
        +Number(row.makingcha)) * Number(this.state.sgst) / 100)
        :row.unit === 2 || row.unit === '2' ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) *(row.rate))
        +(row.wt * row.makingcha)) * Number(this.state.sgst) / 100):
        row.unit === 3 || row.unit === '3' ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100))* (row.rate))
        +(row.qty * row.makingcha)) * Number(this.state.sgst) / 100):
        row.unit === 4 || row.unit === '4' ? ((((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100)) * (row.rate))
        +Number(row.makingcha)) * Number(this.state.sgst) / 100):''
    });
    if (rowTotals.length > 0) {
    grandsgst = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandsgst;
    }
    
    getcgst() {
    let grandcgst = 0;
    const rowTotals = this.state.rowData.map(
    row => {
    return row.unit === 1 || row.unit === '1' ? ((((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100)) * (row.rate))
        +Number(row.makingcha)) * Number(this.state.cgst) / 100)
        :row.unit === 2 || row.unit === '2' ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) *(row.rate))
        +(row.wt * row.makingcha)) * Number(this.state.cgst) / 100):
        row.unit === 3 || row.unit === '3' ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100))* (row.rate))
        +(row.qty * row.makingcha)) * Number(this.state.cgst) / 100):
        row.unit === 4 || row.unit === '4' ? ((((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100)) * (row.rate))
        +Number(row.makingcha)) * Number(this.state.cgst) / 100):''
    });
    if (rowTotals.length > 0) {
    grandcgst = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandcgst;
    }
    
    getigst() {
    let grandigst = 0;
    const rowTotals = this.state.rowData.map(
    row => {
    return row.unit === 1 || row.unit === '1' ? ((((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100)) * (row.rate))
        +Number(row.makingcha)) * Number(this.state.igst) / 100)
        :row.unit === 2 || row.unit === '2' ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)) *(row.rate))
        +(row.wt * row.makingcha)) * Number(this.state.igst) / 100):
        row.unit === 3 || row.unit === '3' ? ((((((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100))* (row.rate))
        +(row.qty * row.makingcha)) * Number(this.state.igst) / 100):
        row.unit === 4 || row.unit === '4' ? ((((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100)) * (row.rate))
        +Number(row.makingcha)) * Number(this.state.igst) / 100):''
    }
    );
    if (rowTotals.length > 0) {
    grandigst = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandigst;
    }
    getTotalGram() {
        let grandTotal = 0;
        const rowTotals = this.state.rowData.map(
        row => {
        return ((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)
        }
        );
        if (rowTotals.length > 0) {
        grandTotal = rowTotals.reduce((acc, val) => acc + val);
        }
        return grandTotal;
        }
    
    getqty() {
    let grandqty = 0;
    const rowTotals = this.state.rowData.map(
    row =>
    Number(row.qty) || 0
    
    );
    
    if (rowTotals.length > 0) {
    grandqty = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandqty;
    }
    getwt() {
    let grandwt = 0;
    const rowTotals = this.state.rowData.map(
    row =>
    Number(row.wt) || 0
    
    );
    
    if (rowTotals.length > 0) {
    grandwt = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandwt;
    }



handleRowDelete(row) {
    const rowDataCopy = this.state.rowData.slice(0);
    rowDataCopy.splice(row, 1);
    this.setState({
    rowData: rowDataCopy
    });
    }
    handleRowAdd() {
    let id = this.state.id;
    id = id++;
    const rowDataCopy = this.state.rowData.slice(0);
    rowDataCopy.push({
    itemname: "",
    qty: 0,
    wt: 0,
    purity:'',
    waste:'',
    rate: 0,
    unit: "",
    makingcha: 0,
    });
    this.setState({
    rowData: rowDataCopy,
    id: id
    });
    }
    }
export default withRouter(Forms);

import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Remove';
import Select from 'react-select'
import axios from 'axios'
import { withRouter,NavLink } from 'react-router-dom';
import InvoicePrint from './InvoicePrint';
import BlockUI from "../../BlockUI/BlockUI";

class Invoice extends React.Component {

constructor() {
super()
var today = new Date(),
date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear() ;
var today = new Date(),
time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
this.state = {
maxid: "", supplierid: "", suppliername: "", contact: "", address: "", date: '',citys:'',
commitdate: "", vat: "", paytype: "", itemlist: "", rowData: [], itemid: "", id: 0, selectOptions: [], id: "",
name: '', seen: false, seeni: false, price: '', print: false, billdata: "", currentdate : date,
currenttime:time, errorsuppliername:'',totalamt:'',totalgram:'',lastdata: [],sum: 0,suminrate: 0,
lastdataG:[],sumG: 0,suminrateG: 0,cgstno:''
};
this.getdata();
this.handleQuantiteChange = this.handleQuantiteChange.bind(this);
this.handlePrixChange = this.handlePrixChange.bind(this);
this.handleselectprdtChange = this.handleselectprdtChange.bind(this);
this.handleRowDelete = this.handleRowDelete.bind(this);
this.handleRowAdd = this.handleRowAdd.bind(this);
this.getTotalwithout = this.getTotalwithout.bind(this);
this.getTotalGram = this.getTotalGram.bind(this);
}
state = {
  blocking: false,
  user: {}
};
togglePop = () => {
this.setState({
seen: !this.state.seen
});
};

togglePrint = () => {
const totalamt = this.getTotalwithout()-0;
const amt = totalamt.toFixed(2);
const totalgram = this.getTotalGram()-0;
const gm = totalgram.toFixed(2);

this.setState({
print: !this.state.print,totalamt:totalamt,totalgram:totalgram
});
};


async getdata() {
  this.setState({blocking:true})
const res = await axios.get('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getsup')
const data = res.data

const options = data.map(d => ({
"value": d.id,
"label": d.supplier_name,
"contact": d.contact_no,
"address": d.address,
'citys' : d.c_state,
'cgstno': d.gstno,
}))
this.setState({ selectOptions: options,blocking:false })
}

handleChange(e) {
  this.setState({blocking:true})
this.setState({ supplierid: e.value, suppliername: e.label, contact: e.contact, address: e.address, 
errorsuppliername:'', citys: e.citys, cgstno: e.cgstno })
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getlastdata/' + e.value).then((resp) => {
        resp.json().then((result) => {
        this.setState({ lastdata: result })
        })
        })
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getlastdataG/' + e.value).then((resp) => {
  resp.json().then((result) => {
  this.setState({ lastdataG: result ,blocking:false})
  })
  })
}

handleQuantiteChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { qty: parseInt(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleselectprdtChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { itemname: value });
this.setState({
rowData: rowDataCopy
});
}
handlePrixChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { rate: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handlePurityChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { purity: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleWasteChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { waste: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleWtChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { wt: parseFloat(value, 10) });
this.setState({
rowData: rowDataCopy
});
}
handleremarkChange(index, value) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { itemremark: value });
this.setState({
rowData: rowDataCopy
});
}

valid(){
    if(this.state.suppliername.length === 0){
        this.setState({errorsuppliername:'Supplier Name required'})
    }else{
        return true
    }
}

submit() {
  this.setState({blocking:true})
  if(this.valid()){
    let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/invoice_bill";
    let data = this.state;
    fetch(url, {
    method: 'POST',
    headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
    },
    body: JSON.stringify(data)
    }).then((result) => {
    result.json().then((resp) => {
      this.setState({blocking:false})
    this.componentDidMount();
    this.togglePrint()
    })
    })
  }
}
save() {this.setState({blocking:true})
  if(this.valid()){
    let url = "http://ydbjewellersserver.jkgroupmanpower.in/public/api/invoice_bill";
    let data = this.state;
    data.rowData.forEach((e,i)=>{
        
      if(i == 0){
        debugger
let dum = (((parseFloat(e['wt']) * parseFloat((e['purity']))) / 100)
+ ((parseFloat(e['wt']) * (parseFloat(e['waste']))) / 100));

let du=(((parseFloat(e['wt']) * parseFloat((e['purity']))) / 100)
+ ((parseFloat(e['wt']) * (parseFloat(e['waste']))) / 100))*(parseFloat(e['rate']));
        

data.rowData[i]["sum"] = (e['itemname'] === 'Silver' ? parseFloat((data.sum) - 
        dum) :
        (e['itemname']) === 'Gold' ?
        parseFloat(data.sumG - 
        dum) : '');

        data.rowData[i]["sum_inrate"] = (e['itemname'] === 'Silver' ? parseFloat((data.suminrate) - 
        du) :
        (e['itemname']) === 'Gold' ?
        parseFloat(data.suminrateG - 
        du) : '');
       
       }
       else{
        let dum = (((parseFloat(e['wt']) * parseFloat((e['purity']))) / 100)
        + ((parseFloat(e['wt']) * (parseFloat(e['waste']))) / 100));

        
let du=(((parseFloat(e['wt']) * parseFloat((e['purity']))) / 100)
+ ((parseFloat(e['wt']) * (parseFloat(e['waste']))) / 100))*(parseFloat(e['rate']));

        
                data.rowData[i]["sum"] = (e['itemname'] === 'Silver' ? parseFloat((data.rowData[i-1].sum) - 
                dum) :
                (e['itemname']) === 'Gold' ?
                parseFloat(data.rowData[i-1].sum - 
                dum) : '');

                data.rowData[i]["sum_inrate"] =   
                (e['itemname'] === 'Silver' ? 
               ((parseFloat((data.rowData[i-1].sum_inrate)) -du )) :

                (e['itemname']) === 'Gold' ?
                (parseFloat(data.rowData[i-1].sum_inrate)) -du : ''); 
         
      }
    })
    fetch(url, {
    method: 'POST',
    headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
    },
    body: JSON.stringify(data)
    }).then((result) => {
    result.json().then((resp) => {
    this.componentDidMount();
    this.setState({rowData: [],supplierid:'',suppliername:'',contact:'',
     address:'',paytype:'',amt: '',citys:'',vat:'',blocking:false});
    })
    })
  }
}
handlecategoryChange(index, value) {
  const rowDataCopy = this.state.rowData.slice(0);
  rowDataCopy[index] = Object.assign({}, rowDataCopy[index], { category: value });
  this.setState({
  rowData: rowDataCopy
  });
  }
componentDidMount() {
fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getidinv').then((resp) => {
resp.json().then((result) => {
this.setState({ maxid: result })
})
})
this.getdata();
}

Print(){
  this.setState({rowData: [],supplierid:'',suppliername:'',contact:'',
  address:'',paytype:'',amt: '',citys:'',vat:'',print: false,});
  window.print()
  this.componentDidMount();
}
changevat(e){this.setState({blocking:true})
  this.setState({ vat: e.target.value }) 
  if(e.target.value==='yes'){
  fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getidpurgst').then((resp) => {
  resp.json().then((result) => {
  this.setState({ maxid: result,blocking:false })
  })
  })
  }else{
  fetch('http://ydbjewellersserver.jkgroupmanpower.in/public/api/getidpur').then((resp) => {
  resp.json().then((result) => {
  this.setState({ maxid: result,blocking:false })
  })
  })
  }
  if(e.target.value==='yes' && this.state.citys === "MADHYA PRADESH"){
  this.setState({ gstnew: this.state.gst })
  }else{
  this.setState({ gstnew: this.state.igst })
  }
  }
  

render() {

const totalamt = this.getTotalwithout()-0;
const amt = totalamt.toFixed(2);
const totalgram = this.getTotalGram()-0;
const gm = totalgram.toFixed(2);
const weight = this.getwt()-0;
const wt1 = weight.toFixed(2);

const mystyle = {color: "white",background: "#0ABE95",padding: "10px",fontFamily: "Arial"};
const mystyle1 = {padding: "10px"};
const mystyle2 = {padding: "10px"};
const mystyle3 = {padding: "10px"};
const mystyle4 = {padding: "10px"};
const mystyle5 = {padding: "10px"};
const mystyle6 = {padding: "10px", width: "182px"};
const mystyle7 = {padding: "10px", width: "182px"};
const mystyle8 = {padding: "10px", width: "182px"};
const mystyleitem = {padding: "10px", width: "60px"};
const btnstyle = {padding: "10px",};
const table = {minWidth: 700};
const mystyleq = {padding: "10px", width: "30px"};
const wt = {padding: "10px", width: "40px"};
const rate = {padding: "10px", width: "40px"};
const discount = {padding: "10px", width: "40px"};
const mystyle80 = {width: "40px"};
const makingchange = {padding: "10px", width: "60px"};
const button = {width: 13, height: 13,padding: 0};
const removebutton = {width: 13, height: 13,padding: 0};
const mystypaytype = {padding: 0, width: "195px"}
const customStyles = {
container: provided => ({...provided,width: "225px",paddingtop: "5px"})};
const addcus = {padding: "5px",}
const model = {position: 'fixed',width: '100 px',height: '100 px',};
const content = {backgroundColor: "#0ABE95",position: 'absolute',top: '0px',left: '30px',width: '500px',padding: '20px',
};
const model1 = {position: 'fixed',width: '100 px',height: '100 px',};
const content1 = {backgroundColor: "#0ABE95",position: 'absolute',top: '0px',left: '30px',width: '500px',padding: '20px',
};
const modelprint = {position: 'absolute',};
const contentprint = {backgroundColor: "white",position: 'absolute',top: '-30px',left: '-5px',width: '970px',padding: '20px',
};
const tableCell = {paddingRight: 10,paddingLeft: 10,minWidth:5}

return (

<div>
  {this.state.print === false ?
    <div>
    <span><Button onClick={() => this.save()} color="primary">Save</Button></span>
    <span><Button onClick={() => this.submit()} color="primary">Print & Pdf</Button></span>
    <span><NavLink to="./Report" exact>
<Button color="primary">Report</Button>
</NavLink>
{this.props.children}</span>
    </div>:null
  }
{this.state.print ?

<InvoicePrint data={{Print:this.Print.bind(this),name:this.state.suppliername,invoiceno:this.state.maxid,address:this.state.
address,contact:this.state.contact,vat:this.state.vat,rowData:this.state.rowData,totalamt:this.state.totalamt,
citys:this.state.citys,gm:this.state.totalgram,cgstno:this.state.cgstno}}/>

:<form autoComplete="off">
<div style={mystyle}>

{
this.state.maxid ?
this.state.maxid.map((item, i) =>
<span>Bill No. :&nbsp;{item.id + 1} </span>
)
:
null
}
<Select styles={customStyles} options={this.state.selectOptions} onChange={this.handleChange.bind(this)}
placeholder="Select Supplier..." theme={theme => ({...theme, borderRadius: 0,
colors: {
...theme.colors,
primary25: 'green',
neutral0: '#c8c8c8',
neutral90: 'white'
},
})} /><p style={{color:'red',fontSize:16,fontStyle:"oblique"}}>{this.state.errorsuppliername}</p>
<TextField style={mystyle1} name="supplierid" value={this.state.supplierid} id="standard-basic" label="Supplier Id"
onChange={(e) => { this.setState({ supplierid: e.target.value }) }} />
<TextField style={mystyle3} name="contact" value={this.state.contact} id="standard-basic" label="Contact No."
onChange={(e) => { this.setState({contact: e.target.value }) }} />
<TextField style={mystyle4} name="address" value={this.state.address} id="standard-basic" label="Address"
onChange={(e) => { this.setState({ address: e.target.value }) }} />
<TextField style={mystyle6}
name='date' label="Date" value={this.state.currentdate}
onChange={(e) => { this.setState({ date: e.target.value }) }}
/>
<TextField style={mystyle7}
id="date"
name="commitdate"
label="Commitment Dt."
type="date"
InputLabelProps={{
shrink: true,
}}
onChange={(e) => { this.setState({ commitdate: e.target.value }) }}
/>
{/* <TextField style={mystyle8} name="vat" id="select" label="Gst Include" select
onChange={(e) => {this.changevat(e) }}>
<MenuItem value="yes">Yes</MenuItem>
<MenuItem value="no">No</MenuItem>
</TextField> */}
<TextField style={mystyle7} name="paytype" id="select" label="Payment Type" select
value={this.state.paytype}
onChange={(e) => { this.setState({ paytype: e.target.value }) }}>
<MenuItem value="1">Cash</MenuItem>
<MenuItem value="2">Online</MenuItem>
</TextField>

<TableContainer component={Paper}>
<Table style={table} aria-label="spanning table">
<TableHead>

<TableRow>
<TableCell><IconButton style={button} color="primary" variant="contained"
onClick={this.handleRowAdd}><AddIcon /></IconButton></TableCell>
<TableCell>Item</TableCell>
<TableCell>Qty.</TableCell>
<TableCell>Wt.(gm)</TableCell>
<TableCell>Purity(%)</TableCell>
<TableCell>Waste(%)</TableCell>
<TableCell>Rate<br></br>(Per gm)</TableCell>
{this.state.vat === 'yes' && this.state.citys === "MADHYA PRADESH" ?
<TableCell style={tableCell}>SGST<br></br>CGST</TableCell>
:this.state.vat === 'yes' ?
<TableCell style={tableCell}>IGST</TableCell>:''}
<TableCell>Amount/Fine</TableCell>
</TableRow>
</TableHead>

<TableBody>
{this.state.rowData.map((data, index) =>
<TableRow key={index} id={index}>
<TableCell><IconButton style={removebutton} color="secondary" variant="contained"
onClick={(e) => this.handleRowDelete(index)} active><RemoveIcon /></IconButton></TableCell>
<TableCell>
<TextField style={mystyleitem} name="itemname" id="select" label="Itemname" select
value={this.state.rowData.itemname}
onChange={(e) => this.handleselectprdtChange(index, e.target.value)}>
<MenuItem value="Gold">Gold</MenuItem>
<MenuItem value="Silver">Silver</MenuItem>
</TextField><hr></hr><br></br>
<TextField name="category" id="select" select label='Cat.'
value={this.state.rowData.category}
onChange={(e) => this.handlecategoryChange(index, e.target.value)}>
<MenuItem value="Gold">Gold</MenuItem>
<MenuItem value="Silver">Silver</MenuItem>
</TextField>&nbsp;&nbsp;
<TextField name="item_remark" id="standard-basic" label="remark." 
value={this.state.rowData.itemremark} onChange={(e) => this.handleremarkChange(index, e.target.value)} />
</TableCell>
<TableCell><TextField style={mystyleq} name="qty" id="standard-basic" label="Qty."
value={this.state.rowData.qty} onChange={(e) => this.handleQuantiteChange(index, e.target.value)} /></TableCell>
<TableCell><TextField style={wt} name="wt" id="standard-basic" label="Weight"
value={this.state.rowData.wt} onChange={(e) => this.handleWtChange(index, e.target.value)} /></TableCell>
<TableCell><TextField style={wt} name="purity" id="standard-basic" label="Purity"
value={this.state.rowData.purity} onChange={(e) => this.handlePurityChange(index, e.target.value)} /></TableCell>
<TableCell><TextField style={wt} name="waste" id="standard-basic" label="Waste"
value={this.state.rowData.waste} onChange={(e) => this.handleWasteChange(index, e.target.value)} /></TableCell>
<TableCell><TextField style={rate} name="rate" id="standard-basic" label="Rate"
value={this.state.rowData.rate} onChange={(e) => this.handlePrixChange(index, e.target.value)} /></TableCell>
<TableCell>{
data.rate === 0 ?
<span>{(((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100)).toFixed(2)}&nbsp;Gm</span>
:<span>{((((data.wt * data.purity) / 100) + ((data.wt * data.waste) / 100))*(data.rate)).toFixed(2)}</span>
}</TableCell>
</TableRow>)}
<TableRow>
<TableCell rowSpan={12} />
<TableCell>Subtotal</TableCell>
<TableCell>{this.getqty()}</TableCell>
<TableCell>{wt1}</TableCell>
<TableCell></TableCell>
<TableCell></TableCell>
<TableCell></TableCell>
<TableCell><span>{amt}<br></br>{gm}&nbsp;gm</span></TableCell>
</TableRow>
</TableBody>
</Table>
</TableContainer>
</div>
</form>} <BlockUI blocking={this.state.blocking} />
</div>)}

getTotalwithout() {
let grandTotal = 0;
const rowTotals = this.state.rowData.map(
row => {
return ((Number((row.wt * row.purity) / 100) + Number((row.wt * row.waste) / 100))*(row.rate))
}
);
if (rowTotals.length > 0) {
grandTotal = rowTotals.reduce((acc, val) => acc + val);
}
return grandTotal;
}

getTotalGram() {
    let grandTotal = 0;
    const rowTotals = this.state.rowData.map(
    row => {
    return ((row.wt * row.purity) / 100) + ((row.wt * row.waste) / 100)
    }
    );
    if (rowTotals.length > 0) {
    grandTotal = rowTotals.reduce((acc, val) => acc + val);
    }
    return grandTotal;
    }

getqty() {
let grandqty = 0;
const rowTotals = this.state.rowData.map(
row =>
(row.qty) || 0

);

if (rowTotals.length > 0) {
grandqty = rowTotals.reduce((acc, val) => acc + val);
}
return grandqty;
}
getwt() {
let grandwt = 0;
const rowTotals = this.state.rowData.map(
row =>
(row.wt) || 0

);

if (rowTotals.length > 0) {
grandwt = rowTotals.reduce((acc, val) => acc + val);
}
return grandwt;
}



handleRowDelete(row) {
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy.splice(row, 1);
this.setState({
rowData: rowDataCopy
});
}
handleRowAdd() {
this.setState({sum:this.getsum(),suminrate:this.getsuminrate(),sumG:this.getsumG(),suminrateG:this.getsuminrateG()})
let id = this.state.id;
id = id++;
const rowDataCopy = this.state.rowData.slice(0);
rowDataCopy.push({
itemname: "",
qty: 0,
wt: 0,
purity:'',
waste:'',
rate: 0,
unit: "",
makingcha: 0,
itemremark: '',
});
this.setState({
rowData: rowDataCopy,
id: id
});
}
getsum() {
  let grandwt = 0;
  const rowTotals = this.state.lastdata.map(
  row =>
  (row.sum) || 0
  
  );
  
  if (rowTotals.length > 0) {
  grandwt = rowTotals.reduce((acc, val) => acc + val);
  }
  return grandwt;
  }
getsuminrate() {
  let grandwt = 0;
  const rowTotals = this.state.lastdata.map(
  row =>
  (row.sum_inrate) || 0
  
  );
  
  if (rowTotals.length > 0) {
  grandwt = rowTotals.reduce((acc, val) => acc + val);
  }
  return grandwt;
  }
getsumG() {
  let grandwt = 0;
  const rowTotals = this.state.lastdataG.map(
  row =>
  (row.sum) || 0
  
  );
  
  if (rowTotals.length > 0) {
  grandwt = rowTotals.reduce((acc, val) => acc + val);
  }
  return grandwt;
  }
getsuminrateG() {
  let grandwt = 0;
  const rowTotals = this.state.lastdataG.map(
  row =>
  (row.sum_inrate) || 0
  
  );
  
  if (rowTotals.length > 0) {
  grandwt = rowTotals.reduce((acc, val) => acc + val);
  }
  return grandwt;
  }
}
export default withRouter(Invoice);
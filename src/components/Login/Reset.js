import React, { useState } from 'react';
import axios from 'axios';
// import { setUserSession } from './Utils/Common';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
// import Checkbox from '@material-ui/core/Checkbox';
// import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

 
function Reset(props) {
  const [loading, setLoading] = useState(false);
  const username = useFormInput('');
  const password = useFormInput('');
  const password1 = useFormInput('');
  const [error, setError] = useState(null);
 
  const handleLogin = () => {
    setError(null);
    setLoading(true);
    axios.put('http://ydbjewellersserver.jkgroupmanpower.in/public/api/reset/1', { username: username.value, password1: password1.value,password: password.value }).then(response => {
      setLoading(false);
      console.log(response)
      // setUserSession(response.data.token, response.data.user);
      props.history.push('/Login');
    }).catch(error => {
      setLoading(false);
      if (error.response.status === 201) setError(error.response.data.message);
      else setError("Invalid Old Password.....");
    });
  }
  function Copyright() {
    return (
      <Typography variant="body2" color="textSecondary" align="center">
        {'Avestan Technologies'}
        {' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
  }
  const useStyles = makeStyles((theme) => ({
    root: {
      height: '100vh',
    },
    image: {
      backgroundImage: 'url(https://source.unsplash.com/random)',
      backgroundRepeat: 'no-repeat',
      backgroundColor:
        theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    paper: {
      margin: theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));  
  const classes = useStyles();
  return (
    <Grid container component="main" className={classes.root}>
    <CssBaseline />
    <Grid item xs={false} sm={4} md={7} className={classes.image} />
    <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Password Reset
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
          {...username}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Username"
            name="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
          {...password1}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="New-Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />

<TextField
          {...password}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Old-Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <p style={{color:'red',fontSize:'12px52'}}>{error}</p>
          <Button
          value={loading ? 'Loading...' : 'Login'} 
          onClick={handleLogin} disabled={loading}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Update
          </Button>
          <Box mt={5}>
            <Copyright />
          </Box>
        </form>
      </div>
    </Grid>
  </Grid>
  );
}
 
const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);
 
  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}
 
export default Reset;
/*!

=========================================================
* Material Dashboard React - v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import ShopIcon from '@material-ui/icons/Shop';
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import AssessmentIcon from '@material-ui/icons/Assessment';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import FiberSmartRecordIcon from '@material-ui/icons/FiberSmartRecord';
// core components/views for Admin layout
// import DashboardPage from "views/Dashboard/Dashboard.js";
import Purchase from "views/Purchase/Purchase.js";
import Sale from "views/Sale/Sale.js";
import Ledger from "views/Sale/Ledger.js";
import Invoice from "views/Invoice/Invoice.js";
import SalesReports from "views/Reports/SalesReports.js";
import PurchaseReports from "views/Reports/PurchaseReports.js";
import InvoiceReports from "views/Reports/InvoiceReports.js";
import Setting from "views/Setting/GstSetting.js";
import Report from "./views/Purchase/Report";


const dashboardRoutes = [
  // {
  //   path: "/dashboard",
  //   name: "Dashboard",
  //   icon: Dashboard,
  //   component: DashboardPage,
  //   layout: "/admin"
  // },
  // {
  //   path: "/login",
  //   name: "Login",
  //   icon: MonetizationOnIcon,
  //   component: Login,
  //   layout: "/admin"
  // },
  {
    path: "/sale",
    name: "Sale",
    icon: MonetizationOnIcon,
    component: Sale,
    layout: "/admin"
  },
  {
    path: "/purchase",
    name: "Purchase",
    icon: ShopIcon,
    component: Purchase,
    layout: "/admin"
  },
  
  {
    path: "/invoice-fine",
    name: "Invoice",
    rtlName: "طباعة",
    icon: LibraryBooks,
    component: Invoice,
    layout: "/admin"
  },
  {
    path: "/reports-sale",
    name: "Sale Report",
    rtlName: "الرموز",
    icon: AssessmentIcon,
    component: SalesReports,
    layout: "/admin"
  },
  {
    path: "/reports-purchase",
    name: "Purchase Rep.",
    rtlName: "خرائط",
    icon: FiberManualRecordIcon,
    component: PurchaseReports,
    layout: "/admin"
  },
  {
    path: "/invoice-ledger",
    name: "Invoice Rep.",
    rtlName: "إخطارات",
    icon: FiberSmartRecordIcon,
    component: InvoiceReports,
    layout: "/admin"
  },
  {
    path: "/Ledger",
    name: "Ledger",
    rtlName: "إخطارات",
    icon: FiberSmartRecordIcon,
    component: Ledger,
    layout: "/admin"
  },
  {
    path: "/Report",
    name: "Report",
    rtlName: "إخطارات",
    icon: FiberSmartRecordIcon,
    component: Report,
    layout: "/admin"
  },
  {
    path: "/Setting",
    name: "Setting",
    rtlName: "إخطارات",
    icon: FiberSmartRecordIcon,
    component: Setting,
    layout: "/admin"
  },
  
];

export default dashboardRoutes;
